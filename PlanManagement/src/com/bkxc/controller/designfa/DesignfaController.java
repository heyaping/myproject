package com.bkxc.controller.designfa;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bkxc.controller.AbstractController;
import com.bkxc.pojo.Designfa;
import com.bkxc.pojo.Designxh;
import com.bkxc.pojo.Productbom;
import com.bkxc.pojo.TreeJson;
import com.bkxc.service.designfa.DesignfaService;
/**
 * 设计方案管理
 * @author luo
 *
 */
@Controller
@RequestMapping("/designfaManager")
public class DesignfaController extends AbstractController {
	@Autowired
	private DesignfaService designfaService;
	/**
	 * 设计方案界面跳转
	 * @return 设计方案主界面
	 */
	@RequestMapping(method = RequestMethod.GET)
	public String designfaInlet(){
		return "/designProjectManagement/index";
	}
	/**
	 * 异步请求设计型号
	 * @return 所有设计型号信息
	 */
	@ResponseBody
	@RequestMapping(value = "/getOnModel",method = RequestMethod.POST)
	public List<Designxh> getOnDesignxh(){
		return designfaService.getOnXh();
	}
	/**
	 * 异步请求根据设计型号得到设计方案
	 * @param designxh
	 * @return 该设计型号下所有设计方案
	 */
	@ResponseBody
	@RequestMapping(value = "/getPlanByModel",method = RequestMethod.POST)
	public List<Designfa> getFaByXh(Designxh designxh){
		return designfaService.getFaByXh(designxh);
	}
	/**
	 * 添加或者修改设计方案
	 * @param designfa
	 * @return 是否成功或已经存在
	 */
	@ResponseBody
	@RequestMapping(value = "/updatePlan",method = RequestMethod.POST)
	public String updatePlan(Designfa designfa){
		if(designfa.getTaskID() != null){
			try {
				designfa.setModifier(getUserName());
				String msg = designfaService.modifyDesignfa(designfa);
				if(msg.equals("exist")){
					return "exist";
				}
				return "update";
			} catch (Exception e) {
				return "false";
			}
		}else{
			try {
				designfa.setCreator(getUserName());
				String msg = designfaService.addDesignfa(designfa);
				if(msg.equals("exist")){
					return "exist";
				}
				return "add";
			} catch (Exception e) {
				return "false";
			}
		}
	}
	/**
	 * 批量删除设计方案
	 * @param taskIDs
	 * @return 是否删除成功
	 */
	@ResponseBody
	@RequestMapping(value = "/removePlan",method = RequestMethod.POST)
	public String removePlan(String taskIDs){
		try {
			designfaService.removeDesignfa(taskIDs);
		} catch (Exception e) {
			return "false";
		}
		return "true";
	}
	/**
	 * 启动设计方案
	 * @param taskID
	 * @return 是否启动成功
	 */
	@ResponseBody
	@RequestMapping(value = "/statusOn",method = RequestMethod.POST)
	public String statusOn(String taskID){
		try {
			designfaService.modifyStatusOn(taskID);
		} catch (Exception e) {
			return "false";
		}
		return "true";
	}
	/**
	 * 停用设计方案
	 * @param taskID
	 * @return 是否成功
	 */
	@ResponseBody
	@RequestMapping(value = "/statusOff",method = RequestMethod.POST)
	public String statusOff(String taskID){
		try {
			designfaService.modifyStatusOff(taskID);
		} catch (Exception e) {
			return "false";
		}
		return "true";
	}
	/**
	 * 根据设计方案得到指标树信息
	 * @param productbom
	 * @return 该方案对应的指标树结构
	 */
	@ResponseBody
	@RequestMapping(value = "/proTree",method = RequestMethod.POST)
	public String proTree(Productbom productbom){
		List<TreeJson> list = designfaService.getAllPro(productbom);
		//集合转换成json
		JSONArray json = JSONArray.fromObject(list);
		return json.toString();
	}
	/**
	 * 添加或者修改指标节点
	 * @param productbom
	 * @return 是否成功
	 */
	@ResponseBody
	@RequestMapping(value = "/addPro",method = RequestMethod.POST)
	public String addPro(Productbom productbom){
		if(productbom.getProductID()!=null){
			try {
				designfaService.modifyPro(productbom);
			} catch (Exception e) {
				return "false";
			}
		}else{
			try {
				if(productbom.getParentProductID()==null||productbom.getParentProductID().equals("")){
					productbom.setParentProductID(0);
				}
				String msg = designfaService.addPro(productbom);
				if(msg.equals("exist")){
					return "exist";
				}
			} catch (Exception e) {
				return "false";
			}
		}
		return "true";
	}
	/**
	 * 删除指标节点
	 * @param productID
	 * @return 是否成功
	 */
	@ResponseBody
	@RequestMapping(value = "/removePro",method = RequestMethod.POST)
	public String removePro(String productID){
		try {
			designfaService.removePro(productID);
		} catch (Exception e) {
			return "false";
		}
		return "true";
	}
}
