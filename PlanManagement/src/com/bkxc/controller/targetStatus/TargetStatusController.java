package com.bkxc.controller.targetStatus;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.bkxc.controller.AbstractController;
@Controller
@RequestMapping("/targetStatus")
public class TargetStatusController extends AbstractController {
	@RequestMapping(method = RequestMethod.GET)
	public String index(){
		return "indicatorStatusMonitoring/conditionMonitoring";
	}
}
