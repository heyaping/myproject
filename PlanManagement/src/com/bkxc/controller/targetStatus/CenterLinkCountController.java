package com.bkxc.controller.targetStatus;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.bkxc.controller.AbstractController;
/**
 * centerLink计算
 * @author luo
 *
 */
@Controller
@RequestMapping("/centerLinkCount")
public class CenterLinkCountController extends AbstractController {
	/**
	 * centerLink计算界面跳转
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET)
	public String index(){
		return "indexDrivenCalculation/centerLink";
	}
}
