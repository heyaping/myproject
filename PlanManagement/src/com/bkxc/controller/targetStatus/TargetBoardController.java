package com.bkxc.controller.targetStatus;

import java.io.UnsupportedEncodingException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bkxc.controller.AbstractController;
import com.bkxc.pojo.Designfa;
import com.bkxc.pojo.Designxh;
import com.bkxc.pojo.Downboard;
import com.bkxc.pojo.Upboard;
import com.bkxc.service.targetStatus.TargetBoardService;
/**
 * 指标看板管理
 * @author luo
 *
 */
@Controller
@RequestMapping("/targetBoard")
public class TargetBoardController extends AbstractController {
	@Autowired
	private TargetBoardService boardService;
	/**
	 * 指标看板界面跳转
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET)
	public String index(){
		return "indicatorStatusMonitoring/kanbanConfiguration";
	}
	/**
	 * 得到所有设计型号
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/modelInfo",method = RequestMethod.POST)
	public List<Designxh> modelInfo(){
		
		return boardService.getAllDesignxh();
	}
	/**
	 * 根据设计型号得到设计方案
	 * @param designfa
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/projectInfo",method = RequestMethod.POST)
	public List<Designfa> projectInfo(Designfa designfa){
		
		return boardService.getFaByXh(designfa);
	}
	/**
	 * 根据设计方案得到设计指标树
	 * @param taskID
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/targetTree",method = RequestMethod.POST)
	public String targetTree(int taskID){
		return boardService.getTargetTreeJson(taskID);
	}
	/**
	 * 得到一级看板信息
	 * @param taskID
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/searchOneBoard",method = RequestMethod.POST)
	public List<Upboard> searchOneBoard(int taskID){
		return boardService.getUpboardByFa(taskID);
	}
	/**
	 * 添加或者修改一级看板信息
	 * @param upboard
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/addOneBoard",method = RequestMethod.POST)
	public String addOneBoard(Upboard upboard){
		String targetName = upboard.getTargetName();
		try {
			targetName = new String(targetName.getBytes("ISO8859-1"),"UTF-8");
		} catch (UnsupportedEncodingException e1) {
		}
		upboard.setTargetName(targetName);
		upboard.setUserID(getUserinfoBySession().getUserID());
		try {
			boardService.addUpboard(upboard);
		} catch (Exception e) {
			return "false";
		}
		return "true";
	}
	/**
	 * 得到二级看板信息
	 * @param upKanbanID
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/searchTwoBoard",method = RequestMethod.POST)
	public List<Downboard> searchTwoBoard(int upKanbanID){
		return boardService.getDownboard(upKanbanID);
	}
	/**
	 * 添加或者修改二级看板信息
	 * @param downboard
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "addTwoBoard",method = RequestMethod.POST)
	public String addTwoBoard(Downboard downboard){
		String targetName = downboard.getTargetName();
		try {
			targetName = new String(targetName.getBytes("ISO8859-1"),"UTF-8");
		} catch (UnsupportedEncodingException e) {
		}
		downboard.setTargetName(targetName);
		downboard.setUserID(getUserinfoBySession().getUserID());
		try {
			boardService.addDownboard(downboard);
		} catch (Exception e) {
			return "false";
		}
		return "true";
	}
	/**
	 * 删除二级看板信息
	 * @param lowKanbanID
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/removeDownboard",method = RequestMethod.POST)
	public String removeDownboard(int lowKanbanID){
		try {
			boardService.removeDownboard(lowKanbanID);
		} catch (Exception e) {
			return "false";
		}
		return "true";
	}
	/**
	 * 删除一级看板信息
	 * @param upKanbanID
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/removeUpboard",method = RequestMethod.POST)
	public String removeUpboard(int upKanbanID){
		try {
			boardService.removeUpboard(upKanbanID);
		} catch (Exception e) {
			return "false";
		}
		return "true";
	}
}
