package com.bkxc.controller.designxh;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bkxc.controller.AbstractController;
import com.bkxc.pojo.Designxh;
import com.bkxc.pojo.Xhattr;
import com.bkxc.service.designxh.DesignxhService;
/**
 * 设计型号管理
 * @author luo
 *
 */
@Controller
@RequestMapping("/designxhManager")
public class DesignxhController extends AbstractController {
	@Autowired
	private DesignxhService designxhService;
	/**
	 * 设计型号界面跳转
	 * @return 设计型号界面
	 */
	@RequestMapping(method = RequestMethod.GET)
	public String designxhIndex(){
		return "/designModelManagement/index";
	}
	/**
	 * 得到设计型号信息
	 * @return 设计型号信息
	 */
	@ResponseBody
	@RequestMapping(value = "/searchDesignxh",method = RequestMethod.POST)
	public List<Designxh> searchDesignxh(){
		return designxhService.getDesignxh();
	}
	/**
	 * 添加或修改设计型号
	 * @param designxh
	 * @return 是否成功
	 */
	@ResponseBody
	@RequestMapping(value = "/addDesignxh",method = RequestMethod.POST)
	public String addDesignxh(Designxh designxh){
		if(designxh.getModelID()!=null){
			try {
				designxh.setModifier(getUserName());
				String msg = designxhService.modifyDesignxh(designxh);
				if(msg.equals("exist")){
					return "exist";
				}
				return "update";
			} catch (Exception e) {
				return "false";
			}
		}else{
			try {
				designxh.setCreator(getUserName());
				String msg = designxhService.addDesignxh(designxh);
				if(msg.equals("exist")){
					return "exist";
				}
				return "add";
			} catch (Exception e) {
				return "false";
			}
		}
	}
	/**
	 * 根据设计型号得到战技指标信息
	 * @param designxh
	 * @return 该设计型号的战技指标
	 */
	@ResponseBody
	@RequestMapping(value = "/searchXhattr",method = RequestMethod.POST)
	public List<Xhattr> searchXhattr(Designxh designxh){
		return designxhService.getXhattr(designxh);
	}
	/**
	 * 添加或者修改战技指标
	 * @param xhattr
	 * @return 是否成功
	 */
	@ResponseBody
	@RequestMapping(value = "/addXhattr",method = RequestMethod.POST)
	public String addXhattr(Xhattr xhattr){
		if(xhattr.getAttrID()!=null){
			try {
				String msg = designxhService.modifyXhattr(xhattr);
				if(msg.equals("exist")){
					return "exist";
				}
			} catch (Exception e) {
				return "false";
			}
		}else{
			try {
				String msg = designxhService.addXhattr(xhattr);
				if(msg.equals("exist")){
					return "exist";
				}
			} catch (Exception e) {
				return "false";
			}
		}
		return "true";
	}
	/**
	 * 批量删除战技指标
	 * @param attrIDs
	 * @return 是否成功
	 */
	@ResponseBody
	@RequestMapping(value = "/removeAttr",method = RequestMethod.POST)
	public String removeAttr(String attrIDs){
		try {
			designxhService.removeXhattr(attrIDs);
		} catch (Exception e) {
			return "false";
		}
		return "true";
	}
	/**
	 * 批量删除设计模型
	 * @param modelIDs
	 * @return 是否成功
	 */
	@ResponseBody
	@RequestMapping(value = "/removeModel",method = RequestMethod.POST)
	public String removeModel(String modelIDs){
		try {
			designxhService.removeModel(modelIDs);
		} catch (Exception e) {
			return "false";
		}
		return "true";
	}
	/**
	 * 启用设计型号
	 * @param modelID
	 * @return 是否成功
	 */
	@ResponseBody
	@RequestMapping(value = "/qyDesignxh",method = RequestMethod.POST)
	public String qyDesignxh(String modelID){
		try {
			designxhService.modifyModelOn(modelID);
		} catch (Exception e) {
			e.printStackTrace();
			return "false";
		}
		return "true";
	}
	/**
	 * 停用设计型号
	 * @param modelID
	 * @return 是否成功
	 */
	@ResponseBody
	@RequestMapping(value = "/tyDesignxh",method = RequestMethod.POST)
	public String tyDesignxh(String modelID){
		try {
			designxhService.modifyModelOff(modelID);
		} catch (Exception e) {
			return "false";
		}
		return "true";
	}
}
