package com.bkxc.controller.targetbom;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bkxc.controller.AbstractController;
import com.bkxc.pojo.Designfa;
import com.bkxc.pojo.Designxh;
import com.bkxc.pojo.Parameter;
import com.bkxc.pojo.Targetbom;
import com.bkxc.service.targetbom.TargetbomService;
import com.bkxc.util.DateUtil;

/**
 * 指标管理控制器
 * @author 李
 *
 */
@Controller
@RequestMapping("/TargetbomManager")
public class TargetbomController extends AbstractController {
	@Autowired
	private TargetbomService targetbomService;
	@ResponseBody
	@RequestMapping(value = "/getDesignxh",method = RequestMethod.POST)
	public List<Designxh> getDesignxh(){
		return targetbomService.getDesignxh();
	}
	@ResponseBody
	@RequestMapping(value = "/getDesignfa",method = RequestMethod.POST)
	public List<Designfa> getDesignfa(Integer modelID){
		return targetbomService.getDesignfa(modelID);
	}
	@ResponseBody
	@RequestMapping(value = "/getTargetBomJson",method = RequestMethod.POST)
	public List<Targetbom> getTargetBomJson(Integer taskID){
		
		return targetbomService.selectParentTarget(taskID);
	}
	@ResponseBody
	@RequestMapping(value = "/selectChildNode",method = RequestMethod.POST)
	public List<Targetbom> selectChildNode(Integer parentTargetID){
		return targetbomService.selectChildNode(parentTargetID);
		
	}
	@ResponseBody
	@RequestMapping(value = "/getjson",method = RequestMethod.POST ,produces = "text/html;charset=UTF-8")
	public String treeJson(Integer taskID){
		return targetbomService.selectAllTarget(taskID);
	}
	@ResponseBody
	@RequestMapping(value = "/getTargetType",method = RequestMethod.POST )
	public  List<Parameter> selectZbType(){
		return targetbomService.selectTargaType();
	}
	@ResponseBody
	@RequestMapping(value = "/selectDateType",method = RequestMethod.POST )
	public  List<Parameter> selectDateType(){
		return targetbomService.selectDateType();
	}
	@ResponseBody
	@RequestMapping(value = "/selectUnit",method = RequestMethod.POST )
	public  List<Parameter> selectUnit(){
		return targetbomService.selectUnit();
	}
	@ResponseBody
	@RequestMapping(value = "/getPrductID",method = RequestMethod.POST)
	public int getPrductID(Integer taskID){
		return targetbomService.selectProductID(taskID);
	}
	@ResponseBody
	@RequestMapping(value = "/addTargetbom",method = RequestMethod.POST)
	public String addTargetbom(Targetbom targetbom,int exnull,Designfa designfa){
		int targetID=0;
		if(targetbom.getTargetID()!=null){
			try {
				targetbom.setModifier(getUserName());
				targetbom.setModifyDT(DateUtil.getNowDateFormat());
				//targetbomService.modifyDesignxh(targetbom);
				targetbomService.updateTargetbom(targetbom);
				return "update";
			} catch (Exception e) {
				return "false";
			}
		}else{
			try {
				targetbom.setCreator(getUserName());
				targetbom.setCreateDT(DateUtil.getNowDateFormat());
				if(exnull==1){
					targetID =targetbomService.addTargetbom(targetbom,null);
				}else{
					targetbom.setParentTargetID(0);
					targetID = targetbomService.addTargetbom(targetbom,designfa);
				}
				return targetID+"";
			} catch (Exception e) {
				e.printStackTrace();
				return "false";
			}
		}
	}
	@ResponseBody
	@RequestMapping(value = "/selectTargetbom",method = RequestMethod.POST)
	public List<Targetbom> selectTargetbom(Integer TargetbomID){
		return targetbomService.selectTargetbom(TargetbomID);
	}
	@ResponseBody
	@RequestMapping(value = "/deleteTargetbom",method = RequestMethod.POST)
	public String deleteTargetbom(Integer TargetbomID){
		try {
			targetbomService.deleteTargetbom(TargetbomID);
		} catch (Exception e) {
			return "false";
		}
		return "true";
	}
	
	@ResponseBody
	@RequestMapping(value = "/selectProduct_tagert",method = RequestMethod.POST)
	public List<Targetbom> selectProduct_target(Integer productID){
		return targetbomService.selectProducts(productID);
	}
	@ResponseBody
	@RequestMapping(value = "/Search",method = RequestMethod.POST)
	public List<Targetbom> Search(Integer modelID,Integer taskID,String targetType,Integer director){
		return targetbomService.selectSearch(modelID,taskID,targetType,director);
	}
	@ResponseBody
	@RequestMapping(value = "/getKbjson",method = RequestMethod.POST ,produces = "text/html;charset=UTF-8")
	public String kanbanJson(Integer taskID){
		return targetbomService.kanbanJson(taskID);
	}

}
