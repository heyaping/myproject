package com.bkxc.controller.targetDrive;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.Vector;

import javax.servlet.http.HttpSession;
import javax.xml.namespace.QName;
import javax.xml.rpc.ParameterMode;
import javax.xml.rpc.ServiceException;

import org.apache.axis.client.Call;
import org.apache.axis.client.Service;
import org.apache.axis.encoding.XMLType;
import org.apache.log4j.Logger;
import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartFile;

import com.bkxc.controller.AbstractController;
import com.bkxc.pojo.TargetInfo;
import com.bkxc.pojo.Targetbom;
import com.bkxc.service.driveCalculation.DriveService;
import com.bkxc.service.targetbom.TargetbomService;
import com.bkxc.util.WebServiceImplUtil;
/**
 * 驱动计算管理
 * @author luo
 *
 */
@Controller
@RequestMapping("/driveCount")
public class DriveCountController extends AbstractController {
	@Autowired private DriveService driveService;
	@Autowired private TargetbomService targetbomService;
	/**
	 * 驱动计算界面跳转
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET)
	public String index(){
		return "indexDrivenCalculation/driveCalculation";
	}
	/**
	 * 根据设计方案得到设计指标树
	 * @param taskID
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/returnTargetTree",method = RequestMethod.POST)
	public String targetTree(int taskID){
		return driveService.getTargetTree(taskID);
	}
	/**
	 * 根据指标ID得到指标节点信息
	 * @param targetID
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/searchTarget",method = RequestMethod.POST)
	public List<TargetInfo> searchTarget(int targetID){
		return driveService.getTargetboms(targetID);
	}
	/**
	 * 得到模型树
	 * @param session
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/modelTree",method = RequestMethod.POST)
	public String modelTargetTree(HttpSession session){
		WebServiceImplUtil webService = new WebServiceImplUtil();
		return webService.webServiceImpl((String)session.getAttribute("PxcTempPath"));
	}
	/**
	 * 上传pxc模型文件
	 * @param pxcFile
	 * @param session
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/upload",method = RequestMethod.POST)
	public String upload(MultipartFile pxcFile,HttpSession session){
		String path = "e:/temp/"+getUserinfoBySession().getUserID()+"/";
		File f = new File(path);
		f.delete();
		if(!f.exists()){
		  f.mkdirs();
		} 
		String fileName = pxcFile.getOriginalFilename();
		File destFile = new File(path,fileName);
		try {
			pxcFile.transferTo(destFile);
			session.setAttribute("PxcTempPath", path+fileName);
		} catch (IllegalStateException e) {
			e.printStackTrace();
			return "false";
		} catch (IOException e) {
			e.printStackTrace();
			return "false";
		}
		return "true";
	}
	/*@ResponseBody
	@RequestMapping(value = "/searchModel",method = RequestMethod.POST)
	public List<TargetInfo> searchTarget(String targetName){
		if(targetName.equals("-1")){
			return null;
		}
		WebServiceImplUtil webService = new WebServiceImplUtil();
		return webService.getTargetInfo(targetName);
	}*/
	
	/**
	 * 设计指标与模型指标映射
	 * @param targetID
	 * @param state
	 * @param type
	 * @param fullName
	 * @param currentValue
	 * @param lowerBound
	 * @param upperBound
	 * @param optVarType
	 * @param session
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/addMapping",method = RequestMethod.POST)
	public String addMapping(String targetID,String state,String type,String fullName,String currentValue,String lowerBound,String upperBound,String optVarType,HttpSession session){
		String pxcpath = (String)session.getAttribute("PxcTempPath");
		String xmlpath = pxcpath.replace("pxc", "xml");
		WebServiceImplUtil ser = new WebServiceImplUtil();
		ser.writeTargetXml(targetID, state, type, fullName, currentValue, lowerBound, upperBound, optVarType, xmlpath);
		Vector<Object> vec = ser.runMapping(pxcpath, xmlpath);
		if(!vec.get(0).equals("true")){
			return vec.get(1).toString();
		}else{
			String result = (String)vec.get(1);
			try {
				Document doc = DocumentHelper.parseText(result);
				Element root = doc.getRootElement();
				List<Element> els = root.elements();
				for (Element element : els) {
					String tid = "";
					String tcurrentValue = "";
					if(element.attributeValue("targetID")!=null){
						tid = element.attributeValue("targetID");
					}
					if(element.attributeValue("currentValue")!=null){
						tcurrentValue = element.attributeValue("currentValue");
					}
					if(!tid.equals("")&&!tcurrentValue.equals("")){
						Targetbom target = new Targetbom();
						target.setTargetID(Integer.parseInt(tid));
						target.setTargeFactVal(tcurrentValue);
						try {
							driveService.updateTargetValue(target);
						} catch (Exception e) {
							return "databaseError";
						}
					}
				}
			} catch (DocumentException e) {
				return e.getMessage();
			}
		}
		return "true";
	}
}
