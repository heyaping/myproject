package com.bkxc.controller.system;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.WeakReferenceMonitor.ReleaseListener;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bkxc.controller.AbstractController;
import com.bkxc.pojo.Jobs;
import com.bkxc.pojo.Roles;
import com.bkxc.pojo.SexPojo;
import com.bkxc.pojo.Users;
import com.bkxc.service.role.RoleService;
import com.bkxc.service.system.UserService;
/**
 * 用户管理
 * @author luo
 *
 */
@Controller
@RequestMapping("/userManager")
public class UserManagerController extends AbstractController {
	@Autowired
	private RoleService roleService;
	@Autowired 
	private UserService userService;
	/**
	 * 用户管理界面跳转
	 * @return 用户管理界面
	 */
	@RequestMapping(method = RequestMethod.GET)
	public String index(){
		return "/userManagement/index";
	}
	/**
	 * 得到所有用户信息
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/search",method = RequestMethod.POST)
	public List search(){
		return userService.getUserAll();
	}
	@ResponseBody
	@RequestMapping(value = "/roleSearch",method = RequestMethod.POST)
	public List<Users> selectRoleUsers(){
		return userService.selectRoleUsers();
	}
	/**
	 * 得到性别角色职位
	 * @param type
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/query",method = RequestMethod.POST)
	public Vector query(String type){
		Vector ve = new Vector();
		if(type.equals("sex")){
			SexPojo sexOne = new SexPojo();
			sexOne.setName("请选择");
			sexOne.setValue(0);
			
			SexPojo sexTwo = new SexPojo();
			sexTwo.setName("男");
			sexTwo.setValue(1);
			
			SexPojo sexThree = new SexPojo();
			sexThree.setName("女");
			sexThree.setValue(2);
			ve.add(sexOne);
			ve.add(sexTwo);
			ve.add(sexThree);
		}else if(type.equals("role")){
			Roles role = new Roles();
			role.setRoleID(0);
			role.setRoleName("请选择");
			ve.add(role);
			List<Roles> roles = roleService.getRoleAll();
			for (Roles roles2 : roles) {
				ve.add(roles2);
			}
		}else if(type.equals("job")){
			Jobs job = new Jobs();
			job.setJobID(0);
			job.setJobName("请选择");
			ve.add(job);
			List<Jobs> jobs = userService.getJob();
			for (Jobs job2 : jobs) {
				ve.add(job2);
			}
		}
		return ve;
	}
	/**
	 * 添加用户
	 * @param user
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/add",method = RequestMethod.POST)
	public String addUser(Users user){
		user.setCreator(getUserName());
		try {
			userService.addUser(user);
		} catch (Exception e) {
			return "false";
		}
		return "true";
	}
	/**
	 * 修改用户信息
	 * @param user
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/update",method = RequestMethod.POST)
	public String updateUser(Users user){
		user.setModifier(getUserName());
		try {
			userService.modifyUser(user);
		} catch (Exception e) {
			return "false";
		}
		return "true";
	}
	/**
	 * 删除用户
	 * @param userIDs
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/remove",method = RequestMethod.POST)
	public String removeUser(String userIDs){
		try {
			userService.removeUser(userIDs);
		} catch (Exception e) {
			return "false";
		}
		return "true";
	}
}
