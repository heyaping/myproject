package com.bkxc.controller.system;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bkxc.controller.AbstractController;
import com.bkxc.pojo.Users;
import com.bkxc.pojo.Uservsdept;
import com.bkxc.service.system.UserToDeptService;
/**
 * 用户对应部门管理
 * @author luo
 *
 */
@Controller
@RequestMapping("/userToDeptManager")
public class UserToDepartManagerController extends AbstractController {
	@Autowired
	private UserToDeptService userToDeptService;
	/**
	 * 用户对应部门界面跳转
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET)
	public String index(){
		return "departmentCorrespondentUser/index";
	}
	/**
	 * 根据部门得到用户信息
	 * @param deptID
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/searchByDept",method = RequestMethod.POST)
	public List<Users> searchByDept(String deptID){
		return userToDeptService.getUserByDept(deptID);
	}
	/**
	 * 得到没有分配部门的用户
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/searchNotDept",method = RequestMethod.POST)
	public List<Users> searchNotDept(){
		return userToDeptService.getUserNotDept();
	}
	/**
	 * 添加用户至部门
	 * @param uservsdept
	 * @param userIDs
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/add",method = RequestMethod.POST)
	public String add(Uservsdept uservsdept,String userIDs){
		try {
			uservsdept.setCreator(getUserName());
			userToDeptService.addUserToDept(uservsdept,userIDs);
		} catch (Exception e) {
			return "false";
		}
		return "true";
	}
	/**
	 * 在部门中移除用户
	 * @param userIDs
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/remove",method = RequestMethod.POST)
	public String remove(String userIDs){
		try {
			userToDeptService.removeUserToDept(userIDs);
		} catch (Exception e) {
			return "false";
		}
		return "true";
	}
}
