package com.bkxc.controller.system;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bkxc.controller.AbstractController;
import com.bkxc.pojo.Depts;
import com.bkxc.service.system.DeptService;
/**
 * 部门管理控制器
 * @author luo
 *
 */
@Controller
@RequestMapping("/deptManager")
public class DepartManagerController extends AbstractController {
	@Autowired
	private DeptService deptService;
	/**
	 * 跳转到部门管理主界面
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET)
	public String index(){
		return "departmentManagement/index";
	}
	/**
	 * 异步请求部门查询
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/search",method = RequestMethod.POST)
	public List<Depts> search(){
		return deptService.getDept();
	}
	/**
	 * 异步请求部门添加
	 * @param depts
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/add",method = RequestMethod.POST)
	public String add(Depts depts){
		try {
			depts.setCreator(getUserName());
			deptService.addDept(depts);
		} catch (Exception e) {
			return "false";
		}
		return "true";
	}
	/**
	 * 异步请求部门修改
	 * @param depts
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/modify",method = RequestMethod.POST)
	public String modify(Depts depts){
		try {
			depts.setModifier(getUserName());
			deptService.modifyDept(depts);
		} catch (Exception e) {
			return "false";
		}
		return "true";
	}
	/**
	 * 异步请求部门删除
	 * @param deptIDs
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/remove",method = RequestMethod.POST)
	public String remove(String deptIDs){
		try {
			deptService.removeDept(deptIDs);
		} catch (Exception e) {
			return "false";
		}
		return "true";
	}
	
	/**
	 * 异步请求用户所在所有群组
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/searchMajorGroups",method = RequestMethod.POST)
	public List<Depts> searchMajorGroups(Integer userID){
		return deptService.selectUserDept(userID);
	}
}
