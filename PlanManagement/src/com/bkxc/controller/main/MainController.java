package com.bkxc.controller.main;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.bkxc.controller.AbstractController;
import com.bkxc.pojo.Users;
import com.bkxc.service.main.MainService;
/**
 * 主界面
 * @author luo
 *
 */
@Controller
@RequestMapping("/main")
public class MainController extends AbstractController {
	@Autowired
	private MainService mainService;
	/**
	 * 用户登录跳转
	 * @param model
	 * @param user
	 * @return 
	 */
	@RequestMapping(method=RequestMethod.POST)
	public String loginMain(Model model,Users user){
		Users userInfo = mainService.getUser(user);
		if(userInfo!=null){
			//用户信息存入会话作用域
			getSession().setAttribute("USER_INFO",userInfo);
			//菜单信息存入会话作用域
			getSession().setAttribute("MAIN_MENU", mainService.getMainMenuAll());
			return "/main/main";
		}
		return "redirect:/login.action";
	}
	@RequestMapping(method = RequestMethod.GET)
	public String login(){
		if(getSession().getAttribute("USER_INFO") != null){
			return "/main/main";
		}
		return "redirect:/login.action";
	}
}