package com.bkxc.controller.targetInfo;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.bkxc.controller.AbstractController;

@Controller
@RequestMapping("/targetManager")
public class TargetManager extends AbstractController {
	@RequestMapping(method = RequestMethod.GET)
	public String index(){
		return "indexManagement/index";
	}
}
