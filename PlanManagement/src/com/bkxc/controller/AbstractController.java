package com.bkxc.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.bkxc.pojo.Users;

public class AbstractController {
    /**
     * 获取登陆的对象
     * @return
     * Userinfo
     */
	protected Users getUserinfoBySession(){
		HttpSession session=getSession();
		if(session!=null){
			return (Users)session.getAttribute("USER_INFO");
		}
	
	return null;
	}
	/**
	 * 登陆用户的名字
	 * @return
	 * String
	 */
	protected String getUserName()
	{
		Users userinfo=getUserinfoBySession();
		if(userinfo!=null){
			return userinfo.getUserName();
		}
		return null;
	}
	/**
	 * request
	 * @return
	 * HttpServletRequest
	 */
	protected HttpServletRequest getRequest()
	{
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder
				.getRequestAttributes()).getRequest();
		return request;
	}
	/**
	 * session
	 * @return
	 * HttpSession
	 */
	protected HttpSession getSession(){
		HttpServletRequest req=getRequest();
		if(req!=null){
			return req.getSession();
		}
		return null;
	}
}
