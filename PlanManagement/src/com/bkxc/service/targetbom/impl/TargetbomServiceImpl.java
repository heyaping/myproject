package com.bkxc.service.targetbom.impl;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.bkxc.mapper.targetbom.TargetbomMapper;
import com.bkxc.pojo.Designfa;
import com.bkxc.pojo.Designxh;
import com.bkxc.pojo.Parameter;
import com.bkxc.pojo.Targetbom;
import com.bkxc.service.targetbom.TargetbomService;

@Service
public class TargetbomServiceImpl implements TargetbomService{
	@Autowired
	private TargetbomMapper targetbomMapper;
	
	public List<Designxh> getDesignxh(){
		return targetbomMapper.selectDesignxh();
	}
	public List<Designfa> getDesignfa(int modelID) {
		return targetbomMapper.selectDesignfa(modelID);
	}
	public List<Targetbom> selectParentTarget(int taskID) {
		return targetbomMapper.selectParentTarget(taskID);
	}
	public List<Targetbom> selectChildNode(int parentTargetID) {
		return targetbomMapper.selectChildNode(parentTargetID);
	}
	private StringBuffer Treejson;
	String color="";
	public String selectAllTarget(int taskID) {
		Treejson = new StringBuffer();
		List<Targetbom> parent = targetbomMapper.selectParentTarget(taskID);
		if(parent.size()>0){
			try{
		for (Targetbom targetbom : parent) {
			if(targetbom.getDataType().equals("5")){
			 if(Integer.parseInt(targetbom.getTargeFactVal())<Integer.parseInt(targetbom.getTargeLowLimit())){
                 color="#8F1D21";
                 }
                 if(Integer.parseInt(targetbom.getTargeFactVal())>Integer.parseInt(targetbom.getTargeUpperLimit())){
                	 //��ɫ
                 color=" #F7CA18";
                 }
                 if(Integer.parseInt(targetbom.getTargeFactVal())<=Integer.parseInt(targetbom.getTargeUpperLimit())&&Integer.parseInt(targetbom.getTargeFactVal())>=Integer.parseInt(targetbom.getTargeLowLimit())){
                 color="#407A52";
                 }
			}else{
				color="#1F4963";
			}
			System.out.println(targetbom.getTargeFactVal());
			if(targetbom.getTargeFactVal()!=null&&targetbom.getTargeFactVal()!=""){
			Treejson.append("[{\"key\":\""+targetbom.getTargetName()+"\",\"index\":\""+targetbom.getTargeFactVal()+"\",\"name\":\""+targetbom.getTargetName()+"\", \"color\":\""+color+"\" ,\"id\":\""+targetbom.getTargetID()+"\"}");
			addChileJson(targetbom);
			}else{
				Treejson.append("[{\"key\":\""+targetbom.getTargetName()+"\",\"name\":\""+targetbom.getTargetName()+"\", \"color\":\""+color+"\" ,\"id\":\""+targetbom.getTargetID()+"\"}");
				addChileJson(targetbom);
			}
			Treejson.append("]");
		}
			}catch (NumberFormatException nFE) {
				System.out.println("Not an Integer");
			}
		}
		return Treejson.toString();
	}
	public void addChileJson(Targetbom parent){
		List<Targetbom> child = targetbomMapper.selectChildNode(parent.getTargetID());
		if(child.size()>0){
			try{
			for (Targetbom targetbom : child) {
				if(targetbom.getDataType().equals("5")){
					 if(Integer.parseInt(targetbom.getTargeFactVal())<Integer.parseInt(targetbom.getTargeLowLimit())){
		                 color="#8F1D21";
		                 }
		                 if(Integer.parseInt(targetbom.getTargeFactVal())>Integer.parseInt(targetbom.getTargeUpperLimit())){
		                 color=" #F7CA18";
		                 }
		                 if(Integer.parseInt(targetbom.getTargeFactVal())<=Integer.parseInt(targetbom.getTargeUpperLimit())&&Integer.parseInt(targetbom.getTargeFactVal())>=Integer.parseInt(targetbom.getTargeLowLimit())){
		                 color="#407A52";
		                 }
					}else{
						color="#1F4963";
					}
				if(targetbom.getTargeFactVal()!=null&&targetbom.getTargeFactVal()!=""){
				Treejson.append(",{\"key\":\""+targetbom.getTargetName()+"\",\"index\":\""+targetbom.getTargeFactVal()+"\",\"name\":\""+targetbom.getTargetName()+"\",\"parent\":\""+parent.getTargetName()+"\" ,\"color\":\""+color+"\" , \"id\":\""+targetbom.getTargetID()+"\"}");
				addChileJson(targetbom);
				}else{
					Treejson.append(",{\"key\":\""+targetbom.getTargetName()+"\",\"name\":\""+targetbom.getTargetName()+"\",\"parent\":\""+parent.getTargetName()+"\" ,\"color\":\""+color+"\" , \"id\":\""+targetbom.getTargetID()+"\"}");
					addChileJson(targetbom);
				}
			}
			}catch (NumberFormatException nFE) {
				System.out.println("Not an Integer");
			}
		}
	}
	public List<Parameter> selectTargaType() {
		// TODO Auto-generated method stub
		return targetbomMapper.selectTargaType();
	}
	public List<Parameter> selectDateType() {
		// TODO Auto-generated method stub
		return targetbomMapper.selectDateType();
	}
	public List<Parameter> selectUnit() {
		// TODO Auto-generated method stub
		return targetbomMapper.selectUnit();
	}
	public int selectProductID(int taskID) {
		return targetbomMapper.selectProductID(taskID);
	}
	public int addTargetbom(Targetbom targetbom,Designfa designfa) throws Exception {
		targetbomMapper.addTargetbom(targetbom);
		int targetID = targetbom.getTargetID();
		if(designfa != null){
			designfa.setTargetID(targetID);
			targetbomMapper.updateFaTarget(designfa);
		}
		return targetID;
	}
	public List<Targetbom> selectTargetbom(int TargetbomID) {
		// TODO Auto-generated method stub
		return targetbomMapper.selectTargetbom(TargetbomID);
	}
	public void deleteTargetbom(int TargetbomID) {
		// TODO Auto-generated method stub
		targetbomMapper.deleteTargetbom(TargetbomID);
	}
	public void updateTargetbom(Targetbom targetbom) {
		targetbomMapper.updateTargetbom(targetbom);
		
	}
	public List<Targetbom> selectProducts(int product) {
		return targetbomMapper.selectProducts(product);
	}
	public List<Targetbom> selectSearch(Integer modelID,Integer taskID,String targetType,Integer director) {
		// TODO Auto-generated method stub
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("modelID", modelID);
		map.put("taskID", taskID);
		map.put("targetType", targetType);
		map.put("director", director);
		return targetbomMapper.selectSearch(map);
	}
	public List<Targetbom> selectUpboard(int taskID) {
		
		return targetbomMapper.selectUpboard(taskID);
	}
	public List<Targetbom> selectDownboard(int upkanbanID) {
		// TODO Auto-generated method stub
		return targetbomMapper.selectDownboard(upkanbanID);
	}
	private StringBuffer kanbanJson;
	String kbcolor="";
	int ex=0;
	public String kanbanJson(int taskID){
		ex=0;
		kanbanJson = new StringBuffer();
		List<Targetbom> parent = targetbomMapper.selectUpboard(taskID);
		kanbanJson.append("{\"class\":\"go.GraphLinksModel\",\"nodeDataArray\": [");
		try{
		for (Targetbom targetbom : parent) {
			if(targetbom.getDataType().equals("5")){
				 if(Integer.parseInt(targetbom.getTargeFactVal())<Integer.parseInt(targetbom.getTargeLowLimit())){
					 kbcolor="#8F1D21";
	                 }
	                 if(Integer.parseInt(targetbom.getTargeFactVal())>Integer.parseInt(targetbom.getTargeUpperLimit())){
	                	 kbcolor="#F7CA18";
	                 }
	                 if(Integer.parseInt(targetbom.getTargeFactVal())<=Integer.parseInt(targetbom.getTargeUpperLimit())&&Integer.parseInt(targetbom.getTargeFactVal())>=Integer.parseInt(targetbom.getTargeLowLimit())){
	                	 kbcolor="#407A52";
	                 }
				}else{
					kbcolor="#407A52";
				}
			System.out.println(targetbom.getTargeFactVal());
			
			if(targetbom.getTargeFactVal()!=null&&targetbom.getTargeFactVal()!=""){
				if(ex>=1){kanbanJson.append(",{\"key\":\""+targetbom.getTargetName()+"\",\"text\":\""+targetbom.getTargetName()+"\",\"isGroup\":true, \"color\":\""+kbcolor+"\" ,\"loc\":\"0 23.52284749830794\"}");
				kanbansJson(targetbom.getUpKanbanID(),targetbom.getTargetName());
				}else{
					
					kanbanJson.append("{\"key\":\""+targetbom.getTargetName()+"\",\"text\":\""+targetbom.getTargetName()+"\",\"isGroup\":true, \"color\":\""+kbcolor+"\" ,\"loc\":\"0 23.52284749830794\"}");
					kanbansJson(targetbom.getUpKanbanID(),targetbom.getTargetName());
				}
			}else{
				if(ex>=1){
				kanbanJson.append(",{\"key\":\""+targetbom.getTargetName()+"\",\"text\":\""+targetbom.getTargetName()+"\",\"isGroup\":true, \"color\":\"#407A52\" ,\"loc\":\"0 23.52284749830794\"}");
				kanbansJson(targetbom.getUpKanbanID(),targetbom.getTargetName());
				}else{
					
					kanbanJson.append("{\"key\":\""+targetbom.getTargetName()+"\",\"text\":\""+targetbom.getTargetName()+"\",\"isGroup\":true, \"color\":\"#407A52\" ,\"loc\":\"0 23.52284749830794\"}");
					kanbansJson(targetbom.getUpKanbanID(),targetbom.getTargetName());
				}
			}
			ex=ex+1;
	}
		}catch (NumberFormatException nFE) {
			System.out.println("Not an Integer");
		}
		kanbanJson.append("],\"linkDataArray\": []}");
		return kanbanJson.toString();
	}
	
	public void kanbansJson( int upkanbanID,String name){
		List<Targetbom> child = targetbomMapper.selectDownboard(upkanbanID);
		if(child.size()>0){
				try{
			for (Targetbom targetbom : child) {
				if(targetbom.getDataType().equals("5")){
					 if(Integer.parseInt(targetbom.getTargeFactVal())<Integer.parseInt(targetbom.getTargeLowLimit())){
						 kbcolor="#8F1D21";
		                 }
		                 if(Integer.parseInt(targetbom.getTargeFactVal())>Integer.parseInt(targetbom.getTargeUpperLimit())){
		                	 kbcolor="#F7CA18";
		                 }
		                 if(Integer.parseInt(targetbom.getTargeFactVal())<=Integer.parseInt(targetbom.getTargeUpperLimit())&&Integer.parseInt(targetbom.getTargeFactVal())>=Integer.parseInt(targetbom.getTargeLowLimit())){
		                	 kbcolor="#407A52";
		                 }
					}else{
						kbcolor="#407A52";
					}
				if(targetbom.getTargeFactVal()!=null&&targetbom.getTargeFactVal()!=""){
					kanbanJson.append(",{\"key\":\""+targetbom.getTargetName()+"\",\"text\":\""+targetbom.getTargetName()+"\",\"group\":\""+name+"\", \"color\":\""+kbcolor+"\" ,\"loc\":\"12 65.52284749830794\"}");
				}else{
					kanbanJson.append(",{\"key\":\""+targetbom.getTargetName()+"\",\"text\":\""+targetbom.getTargetName()+"\",\"group\":\""+name+"\", \"color\":\"#407A52\" ,\"loc\":\"12 65.52284749830794\"}");
				}
			}
				}catch (NumberFormatException nFE) {
					System.out.println("Not an Integer");
				}
							
	
		}
				}
}
