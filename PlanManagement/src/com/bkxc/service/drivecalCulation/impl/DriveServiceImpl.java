package com.bkxc.service.driveCalculation.impl;

import java.util.ArrayList;
import java.util.List;

import net.sf.json.JSONArray;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bkxc.mapper.driveCalculation.DriveMapper;
import com.bkxc.mapper.targetStatus.TargetBoardMapper;
import com.bkxc.pojo.TargetInfo;
import com.bkxc.pojo.Targetbom;
import com.bkxc.pojo.TreeGridJson;
import com.bkxc.pojo.TreeJson;
import com.bkxc.service.driveCalculation.DriveService;
@Service
public class DriveServiceImpl implements DriveService {
	@Autowired
	private DriveMapper driveMapper;
	@Autowired
	private TargetBoardMapper boardMapper;
	public List<TargetInfo> getTargetboms(int targetID) {
		return driveMapper.selectTargetByID(targetID);
	}
	public String getTargetTree(int taskID) {
		Integer targetID = boardMapper.selectTargetIDByDesignFa(taskID);
		List<Targetbom> list = boardMapper.selectTargetInfoByID(targetID);
		List<TreeGridJson> tree = new ArrayList<TreeGridJson>();
		for (Targetbom target : list) {
			TreeGridJson t = new TreeGridJson();
			t.setId(target.getTargetID());
			t.setName(target.getTargetName());
			t.setDataType(target.getDataTypeName());
			t.setTargeFactVal(target.getTargeFactVal());
			t.setTargeLowLimit(target.getTargeLowLimit());
			t.setTargetType(target.getTargetTypeName());
			t.setTargeUpperLimit(target.getTargeUpperLimit());
			t.setChildren(getChildTarget(target.getTargetID()));
			tree.add(t);
		}
		JSONArray json = JSONArray.fromObject(tree);
		return json.toString();
	}
	//递归组合多层树结构
	public List<TreeGridJson> getChildTarget(Integer parentID){
		List<Targetbom> list = boardMapper.selectTargetChildInfo(parentID);
		List<TreeGridJson> tree = new ArrayList<TreeGridJson>();
		if(list.size()>0){
			for (Targetbom target : list) {
				TreeGridJson t = new TreeGridJson();
				t.setId(target.getTargetID());
				t.setName(target.getTargetName());
				t.setDataType(target.getDataTypeName());
				t.setTargeFactVal(target.getTargeFactVal());
				t.setTargeLowLimit(target.getTargeLowLimit());
				t.setTargetType(target.getTargetTypeName());
				t.setTargeUpperLimit(target.getTargeUpperLimit());
				t.setChildren(getChildTarget(target.getTargetID()));
				tree.add(t);
			}
		}
		return tree;
	}
	public void updateTargetValue(Targetbom targetbom) throws Exception {
		driveMapper.updateTargetValue(targetbom);
	}
}
