package com.bkxc.service.driveCalculation;

import java.util.List;

import com.bkxc.pojo.TargetInfo;
import com.bkxc.pojo.Targetbom;

public interface DriveService {
	public List<TargetInfo> getTargetboms(int targetID);
	public String getTargetTree(int taskID);
	public void updateTargetValue(Targetbom targetbom)throws Exception;
}
