package com.bkxc.service.role.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bkxc.mapper.role.RoleMapper;
import com.bkxc.pojo.Roles;
import com.bkxc.service.role.RoleService;
@Service
public class RoleServiceImpl implements RoleService {
	@Autowired
	private RoleMapper roleMapper;
	public List<Roles> getRoleAll() {
		return roleMapper.selectRoleAll();
	}

}
