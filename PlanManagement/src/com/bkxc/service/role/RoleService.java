package com.bkxc.service.role;

import java.util.List;

import com.bkxc.pojo.Roles;

public interface RoleService {
	List<Roles> getRoleAll();
}
