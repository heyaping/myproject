package com.bkxc.service.system;

import java.util.List;

import org.springframework.aop.ThrowsAdvice;

import com.bkxc.pojo.Depts;

public interface DeptService {
	public List<Depts> getDept();
	public void addDept(Depts depts)throws Exception;
	public void modifyDept(Depts depts)throws Exception;
	public void removeDept(String deptIDs)throws Exception;
	public List<Depts> selectUserDept(Integer userID); 
}
