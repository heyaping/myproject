package com.bkxc.service.system;

import java.util.List;

import com.bkxc.pojo.Jobs;
import com.bkxc.pojo.Users;

public interface UserService {
	public List<Users> getUserAll();
	public void addUser(Users user)throws Exception;
	public void modifyUser(Users user)throws Exception;
	public void removeUser(String userIDs)throws Exception;
	public List<Jobs> getJob();
	List<Users> selectRoleUsers();
}
