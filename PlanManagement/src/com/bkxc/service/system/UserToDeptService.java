package com.bkxc.service.system;

import java.util.List;

import com.bkxc.pojo.Users;
import com.bkxc.pojo.Uservsdept;

public interface UserToDeptService {
	public List<Users> getUserByDept(String deptID);
	public void addUserToDept(Uservsdept uservsdept,String userIDs)throws Exception;
	public List<Users> getUserNotDept();
	public void removeUserToDept(String userIDs)throws Exception;
}
