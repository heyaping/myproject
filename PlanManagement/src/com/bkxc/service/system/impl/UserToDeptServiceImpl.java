package com.bkxc.service.system.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bkxc.mapper.system.UserToDeptMapper;
import com.bkxc.pojo.Users;
import com.bkxc.pojo.Uservsdept;
import com.bkxc.service.system.UserToDeptService;
import com.bkxc.util.DateUtil;
@Service
public class UserToDeptServiceImpl implements UserToDeptService {
	@Autowired
	private UserToDeptMapper deptMapper;

	public List<Users> getUserByDept(String deptID) {
		return deptMapper.selectUserByDept(deptID);
	}

	public void addUserToDept(Uservsdept uservsdept,String userIDs)throws Exception {
		uservsdept.setCreateDT(DateUtil.getNowDateFormat());
		String[] userID = userIDs.split(",");
		for (String userid : userID) {
			uservsdept.setUserID(Integer.parseInt(userid));
			deptMapper.insertUserToDept(uservsdept);
		}
	}
	public List<Users> getUserNotDept() {
		return deptMapper.selectUserNotDept();
	}
	public void removeUserToDept(String userIDs)throws Exception{
		String[] userID = userIDs.split(",");
		for (String string : userID) {
			deptMapper.deleteByUserID(string);
		}
	}
}
