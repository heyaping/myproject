package com.bkxc.service.system.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bkxc.mapper.system.DeptMapper;
import com.bkxc.mapper.system.UserMapper;
import com.bkxc.mapper.system.UserToDeptMapper;
import com.bkxc.pojo.Depts;
import com.bkxc.service.system.DeptService;
import com.bkxc.util.DateUtil;
@Service
public class DeptServiceImpl implements DeptService {
	@Autowired
	private DeptMapper deptMapper;
	@Autowired
	private UserToDeptMapper userToDeptMapper;
	public List<Depts> getDept() {
		return deptMapper.selectDept();
	}
	public void addDept(Depts depts)throws Exception {
		depts.setCreateDT(DateUtil.getNowDateFormat());
		deptMapper.insertDept(depts);
	}
	public void modifyDept(Depts depts)throws Exception {
		depts.setModifyDT(DateUtil.getNowDateFormat());
		deptMapper.updateDept(depts);
	}
	public void removeDept(String deptIDs)throws Exception {
		String[] deptID = deptIDs.split(",");
		for (String id : deptID) {
			deptMapper.deleteDept(id);
			userToDeptMapper.deleteByDeptID(id);
		}
	}
	public List<Depts> selectUserDept(Integer userID) {
		System.out.println("ddddddddddddddddddddddddddd");
		return deptMapper.selectUserDept(userID);
	}
}
