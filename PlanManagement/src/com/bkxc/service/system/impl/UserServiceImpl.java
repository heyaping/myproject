package com.bkxc.service.system.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bkxc.mapper.system.UserMapper;
import com.bkxc.mapper.system.UserToDeptMapper;
import com.bkxc.pojo.Jobs;
import com.bkxc.pojo.Users;
import com.bkxc.service.system.UserService;
@Service
public class UserServiceImpl implements UserService {
	@Autowired
	private UserMapper userMapper;
	@Autowired
	private UserToDeptMapper userToDeptMapper;
	public List<Users> getUserAll() {
		return userMapper.selectUserAll();
	}
	public void addUser(Users user) throws Exception {
		Date date = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String newDate = dateFormat.format(date);
		user.setCreateDT(newDate);
		userMapper.insertUser(user);
	}
	public void modifyUser(Users user) throws Exception {
		Date date = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String newDate = dateFormat.format(date);
		user.setModifyDT(newDate);
		userMapper.updateUser(user);
	}
	public void removeUser(String userIDs) throws Exception{
		String[] userID = userIDs.split(",");
		for (String string : userID) {
			userMapper.deleteUser(string);
			userToDeptMapper.deleteByUserID(string);
		}
	}
	public List<Jobs> getJob() {
		return userMapper.selectAllJob();
	}
	public List<Users> selectRoleUsers(){
		return userMapper.selectRoleUsers();
	}
}
