package com.bkxc.service.designxh;

import java.util.List;

import com.bkxc.pojo.Designxh;
import com.bkxc.pojo.Xhattr;

public interface DesignxhService {
	public String addDesignxh(Designxh designxh)throws Exception;
	public String addXhattr(Xhattr xhattr)throws Exception;
	public List<Xhattr> getXhattr(Designxh designxh);
	public List<Designxh> getDesignxh();
	public String modifyDesignxh(Designxh designxh)throws Exception;
	public String modifyXhattr(Xhattr xhattr)throws Exception;
	public void removeXhattr(String attrIDs)throws Exception;
	public void removeModel(String modelIDs)throws Exception;
	public void modifyModelOn(String modelID)throws Exception;
	public void modifyModelOff(String modelID)throws Exception;
}
