package com.bkxc.service.designxh.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bkxc.mapper.designxh.DesignxhMapper;
import com.bkxc.pojo.Designxh;
import com.bkxc.pojo.Xhattr;
import com.bkxc.service.designxh.DesignxhService;
import com.bkxc.util.DateUtil;
@Service
public class DesignxhServiceImpl implements DesignxhService {
	@Autowired
	private DesignxhMapper designxhMapper;
	public String addDesignxh(Designxh designxh)throws Exception {
		designxh.setCreateDT(DateUtil.getNowDateFormat());
		if(designxhMapper.selectXhByName(designxh).size()>0){
			return "exist";
		}
		designxhMapper.insertDesignXh(designxh);
		return "true";
	}
	public String addXhattr(Xhattr xhattr) throws Exception {
		if(designxhMapper.selectXhattrByModelAndName(xhattr).size()>0){
			return "exist";
		}
		designxhMapper.insertXhattr(xhattr);
		return "true";
	}
	public List<Xhattr> getXhattr(Designxh designxh) {
		return designxhMapper.selectXhattrByXh(designxh);
	}
	public List<Designxh> getDesignxh() {
		return designxhMapper.selectAll();
	}
	public String modifyDesignxh(Designxh designxh)throws Exception {
		designxh.setModifyDT(DateUtil.getNowDateFormat());
		if(designxhMapper.selectXhByName(designxh).size()>0){
			return "exist";
		}
		designxhMapper.updateDesignXh(designxh);
		return "true";
	}
	public String modifyXhattr(Xhattr xhattr) throws Exception {
		if(designxhMapper.selectXhattrByModelAndName(xhattr).size()>0){
			return "exist";
		}
		designxhMapper.updateXhattr(xhattr);
		return "true";
	}
	public void removeXhattr(String attrIDs) throws Exception {
		String[] attrID = attrIDs.split(",");
		for (String id : attrID) {
			designxhMapper.deleteXhattrByID(id);
		}
	}
	public void removeModel(String modelIDs) throws Exception {
		String[] modeID = modelIDs.split(",");
		for (String modelid : modeID) {
			designxhMapper.deleteDesignXh(modelid);
			designxhMapper.deleteXhattrByXh(modelid);
		}
	}
	public void modifyModelOn(String modelID) throws Exception {
		designxhMapper.onModel(modelID);
	}
	public void modifyModelOff(String modelID) throws Exception {
		designxhMapper.offModel(modelID);
	}
}
