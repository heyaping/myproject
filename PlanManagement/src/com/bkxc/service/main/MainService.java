package com.bkxc.service.main;

import java.util.List;
import java.util.Map;

import com.bkxc.pojo.MainMenu;
import com.bkxc.pojo.Users;

public interface MainService {
	Users getUser(Users user);
	List<MainMenu> getMainMenuAll();
}
