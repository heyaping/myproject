package com.bkxc.service.main.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bkxc.mapper.main.MainMapper;
import com.bkxc.pojo.MainMenu;
import com.bkxc.pojo.Users;
import com.bkxc.service.main.MainService;
@Service
public class MainServiceImpl implements MainService {
	@Autowired
	private MainMapper mainMapper;
	public Users getUser(Users user) {
		return mainMapper.selectUserByUser(user);
	}
	public List<MainMenu> getMainMenuAll() {
		return mainMapper.selectMainMenuAll();
	}
}
