package com.bkxc.service.targetStatus;

import java.util.List;

import com.bkxc.pojo.Designfa;
import com.bkxc.pojo.Designxh;
import com.bkxc.pojo.Downboard;
import com.bkxc.pojo.Upboard;

public interface TargetBoardService {
	public List<Designxh> getAllDesignxh();
	public List<Designfa> getFaByXh(Designfa designfa);
	public String getTargetTreeJson(int taskID);
	public List<Upboard> getUpboardByFa(int taskID);
	public void addUpboard(Upboard upboard)throws Exception;
	public List<Downboard> getDownboard(int upKanbanID);
	public void addDownboard(Downboard downboard)throws Exception;
	public void removeDownboard(int lowKanbanID)throws Exception;
	public void removeUpboard(int upKanbanID)throws Exception;
}
