package com.bkxc.service.targetStatus.impl;

import java.util.ArrayList;
import java.util.List;

import net.sf.json.JSONArray;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bkxc.mapper.targetStatus.TargetBoardMapper;
import com.bkxc.pojo.Designfa;
import com.bkxc.pojo.Designxh;
import com.bkxc.pojo.Downboard;
import com.bkxc.pojo.Targetbom;
import com.bkxc.pojo.TreeJson;
import com.bkxc.pojo.Upboard;
import com.bkxc.service.targetStatus.TargetBoardService;
@Service
public class TargetBoardServiceImpl implements TargetBoardService {
	@Autowired
	private TargetBoardMapper boardMapper;
	public List<Designxh> getAllDesignxh() {
		return boardMapper.selectAllDesignxh();
	}

	public List<Designfa> getFaByXh(Designfa designfa) {
		return boardMapper.selectFaByXh(designfa);
	}

	public String getTargetTreeJson(int taskID) {
		Integer targetID = boardMapper.selectTargetIDByDesignFa(taskID);
		List<Targetbom> list = boardMapper.selectTargetInfoByID(targetID);
		List<TreeJson> tree = new ArrayList<TreeJson>();
		for (Targetbom target : list) {
			TreeJson t = new TreeJson();
			t.setId(target.getTargetID());
			t.setText(target.getTargetName());
			t.setChildren(getChildTarget(target.getTargetID()));
			tree.add(t);
		}
		JSONArray json = JSONArray.fromObject(tree);
		return json.toString();
	}
	//递归组合多层树结构
	public List<TreeJson> getChildTarget(Integer parentID){
		List<Targetbom> list = boardMapper.selectTargetChildInfo(parentID);
		List<TreeJson> tree = new ArrayList<TreeJson>();
		if(list.size()>0){
			for (Targetbom target : list) {
				TreeJson t = new TreeJson();
				t.setId(target.getTargetID());
				t.setText(target.getTargetName());
				t.setChildren(getChildTarget(target.getTargetID()));
				tree.add(t);
			}
		}
		return tree;
	}

	public List<Upboard> getUpboardByFa(int taskID) {
		return boardMapper.selectUpboardByFA(taskID);
	}

	public void addUpboard(Upboard upboard)throws Exception {
		boardMapper.insertUpboard(upboard);
	}

	public List<Downboard> getDownboard(int upKanbanID) {
		return boardMapper.selectDownboard(upKanbanID);
	}

	public void addDownboard(Downboard downboard) throws Exception {
		boardMapper.insertDownBoard(downboard);
	}

	public void removeDownboard(int lowKanbanID) throws Exception {
		boardMapper.deleteDownBoard(lowKanbanID);
	}

	public void removeUpboard(int upKanbanID) throws Exception {
		boardMapper.deleteUpBoard(upKanbanID);
		boardMapper.deleteDownBoardByUpKanbanID(upKanbanID);
	}
}
