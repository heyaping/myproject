package com.bkxc.service.designfa.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sun.reflect.generics.tree.Tree;

import com.bkxc.mapper.designfa.DesignfaMapper;
import com.bkxc.pojo.Designfa;
import com.bkxc.pojo.Designxh;
import com.bkxc.pojo.Productbom;
import com.bkxc.pojo.TreeJson;
import com.bkxc.service.designfa.DesignfaService;
import com.bkxc.util.DateUtil;
@Service
public class DesignfaServiceImpl implements DesignfaService{
	@Autowired
	private DesignfaMapper designfaMapper;
	public List<Designxh> getOnXh() {
		return designfaMapper.selectOnXh();
	}
	public List<Designfa> getFaByXh(Designxh designxh) {
		return designfaMapper.selectByXh(designxh);
	}
	public String addDesignfa(Designfa designfa) throws Exception {
		if(designfaMapper.selectFaByName(designfa).size()>0){
			return "exist";
		}
		designfa.setCreateDT(DateUtil.getNowDateFormat());
		designfaMapper.insertDesignfa(designfa);
		return "true";
	}
	public String modifyDesignfa(Designfa designfa) throws Exception {
		if(designfaMapper.selectFaByName(designfa).size()>0){
			return "exist";
		}
		designfa.setModifyDT(DateUtil.getNowDateFormat());
		designfaMapper.updateDesignfa(designfa);
		return "true";
	}
	public void removeDesignfa(String taskIDs) throws Exception {
		String[] taskID = taskIDs.split(",");
		for (String id : taskID) {
			int productID = designfaMapper.selectFaProByID(Integer.parseInt(id));
			if(productID!=0){
				designfaMapper.deleteProByID(productID+"");
				deleteAllChild(productID);
			}
			designfaMapper.deleteDesignfa(id);
		}
	}
	public void modifyStatusOn(String taskID) throws Exception {
		designfaMapper.updateStatusOn(taskID);
	}
	public void modifyStatusOff(String taskID) throws Exception {
		designfaMapper.updateStatusOff(taskID);
	}
	public List<TreeJson> getPro(Productbom productbom) {
		List<Productbom> list = designfaMapper.selectPro(productbom);
		List<TreeJson> listr = new ArrayList<TreeJson>();
		for (Productbom pro : list) {
			TreeJson tree = new TreeJson();
			tree.setId(pro.getProductID());
			tree.setText(pro.getProductName());
			tree.setState("closed");
			listr.add(tree);
		}
		return listr;
	}
	public String addPro(Productbom productbom)throws Exception {
		if(productbom.getParentProductID()==0){
			if(designfaMapper.selectFaProByID(productbom.getTaskID())!=0){
				return "exist";
			}
			designfaMapper.insertPro(productbom);
			designfaMapper.updateDesignfaPro(productbom);
		}else{
			designfaMapper.insertPro(productbom);
		}
		return "ok";
	}
	public void modifyPro(Productbom productbom) throws Exception {
		designfaMapper.updatePro(productbom);
	}
	public void removePro(String productID)throws Exception{
		if(designfaMapper.selectProParentID(Integer.parseInt(productID))==0){
			designfaMapper.updateDesignProductID(Integer.parseInt(productID));
		}
		designfaMapper.deleteProByID(productID);
		deleteAllChild(Integer.parseInt(productID));
	}
	//递归删除子节点
	public void deleteAllChild(Integer parentID){
		List<Integer> child = designfaMapper.selectProByParentID(parentID);
		designfaMapper.deleteProByParentID(parentID);
		if(child.size()>0){
			for (Integer id : child) {
				deleteAllChild(id);
			}
		}
	}
	
	public List<TreeJson> getAllPro(Productbom productbom){
		List<Productbom> list = designfaMapper.selectPro(productbom);
		List<TreeJson> tree = new ArrayList<TreeJson>();
		for (Productbom pro : list) {
			TreeJson t = new TreeJson();
			t.setId(pro.getProductID());
			t.setText(pro.getProductName());
			t.setChildren(getAllProByParentID(pro.getProductID()));
			tree.add(t);
		}
		return tree;
	}
	public List<TreeJson> getAllProByParentID(Integer parentID){
		List<Productbom> list = designfaMapper.selectProInfoByParentID(parentID);
		List<TreeJson> tree = new ArrayList<TreeJson>();
		if(list.size()>0){
			for (Productbom pro : list) {
				TreeJson t = new TreeJson();
				t.setId(pro.getProductID());
				t.setText(pro.getProductName());
				t.setChildren(getAllProByParentID(pro.getProductID()));
				tree.add(t);
			}
		}
		return tree;
	}
}
