package com.bkxc.service.designfa;

import java.util.List;
import java.util.Map;

import com.bkxc.pojo.Designfa;
import com.bkxc.pojo.Designxh;
import com.bkxc.pojo.Productbom;
import com.bkxc.pojo.TreeJson;

public interface DesignfaService {
	public List<Designxh> getOnXh();
	public List<Designfa> getFaByXh(Designxh designxh);
	public String addDesignfa(Designfa designfa)throws Exception;
	public String modifyDesignfa(Designfa designfa)throws Exception;
	public void removeDesignfa(String taskIDs)throws Exception;
	public void modifyStatusOn(String taskID)throws Exception;
	public void modifyStatusOff(String taskID)throws Exception;
	public List<TreeJson> getPro(Productbom productbom);
	public String addPro(Productbom productbom)throws Exception;
	public void removePro(String productID)throws Exception;
	public void modifyPro(Productbom productbom)throws Exception;
	public List<TreeJson> getAllPro(Productbom productbom);
}
