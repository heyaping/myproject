package com.bkxc.pojo;
/**
 * 用户部门实体类
 * @author luo
 *
 */
public class Uservsdept {
	private Integer id;
	private Integer deptID;
	private Integer userID;
	private String createDT;
	private String creator;
	private String modifyDT;
	private String modifier;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getDeptID() {
		return deptID;
	}
	public void setDeptID(Integer deptID) {
		this.deptID = deptID;
	}
	public Integer getUserID() {
		return userID;
	}
	public void setUserID(Integer userID) {
		this.userID = userID;
	}
	public String getCreateDT() {
		return createDT;
	}
	public void setCreateDT(String createDT) {
		this.createDT = createDT;
	}
	public String getCreator() {
		return creator;
	}
	public void setCreator(String creator) {
		this.creator = creator;
	}
	public String getModifyDT() {
		return modifyDT;
	}
	public void setModifyDT(String modifyDT) {
		this.modifyDT = modifyDT;
	}
	public String getModifier() {
		return modifier;
	}
	public void setModifier(String modifier) {
		this.modifier = modifier;
	}
	
	
}
