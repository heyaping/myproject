package com.bkxc.pojo;
/**
 * 用户角色实体类
 * @author luo
 *
 */
public class Roles {
	private Integer roleID;
	private String roleName;
	private String CreateDT;
	private String creator;
	private String modifyDT;
	private String modifier;
	public Integer getRoleID() {
		return roleID;
	}
	public void setRoleID(Integer roleID) {
		this.roleID = roleID;
	}
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	public String getCreateDT() {
		return CreateDT;
	}
	public void setCreateDT(String createDT) {
		CreateDT = createDT;
	}
	public String getCreator() {
		return creator;
	}
	public void setCreator(String creator) {
		this.creator = creator;
	}
	public String getModifyDT() {
		return modifyDT;
	}
	public void setModifyDT(String modifyDT) {
		this.modifyDT = modifyDT;
	}
	public String getModifier() {
		return modifier;
	}
	public void setModifier(String modifier) {
		this.modifier = modifier;
	}
	
}
