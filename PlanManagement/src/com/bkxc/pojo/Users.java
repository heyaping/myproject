package com.bkxc.pojo;
/**
 * 用户实体类
 * @author luo
 *
 */
public class Users {
	private Integer userID;
	private String userCode;
	private String password;
	private String userName;
	private String tel;
	private String sex;
	private String email;
	private String postID;
	private Integer roleID;
	private String roleName;
	private String jobID;
	private String jobName;
	private String createDT;
	private String creator;
	private String modifyDT;
	private String modifier;
	private Integer majorGroups;
	

	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	public String getJobID() {
		return jobID;
	}
	public void setJobID(String jobID) {
		this.jobID = jobID;
	}
	public String getJobName() {
		return jobName;
	}
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	public Integer getUserID() {
		return userID;
	}
	public void setUserID(Integer userID) {
		this.userID = userID;
	}
	public String getUserCode() {
		return userCode;
	}
	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		if(sex.equals("1")){
			this.sex = "男";
		}else if(sex.equals("2")){
			this.sex = "女";
		}else{
			this.sex = sex;
		}
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPostID() {
		return postID;
	}
	public void setPostID(String postID) {
		this.postID = postID;
	}
	public Integer getRoleID() {
		return roleID;
	}
	public void setRoleID(Integer roleID) {
		this.roleID = roleID;
	}
	public String getCreateDT() {
		return createDT;
	}
	public void setCreateDT(String createDT) {
		this.createDT = createDT;
	}
	public String getCreator() {
		return creator;
	}
	public void setCreator(String creator) {
		this.creator = creator;
	}
	public String getModifyDT() {
		return modifyDT;
	}
	public void setModifyDT(String modifyDT) {
		this.modifyDT = modifyDT;
	}
	public String getModifier() {
		return modifier;
	}
	public void setModifier(String modifier) {
		this.modifier = modifier;
	}
	public Integer getMajorGroups() {
		return majorGroups;
	}
	public void setMajorGroups(Integer majorGroups) {
		this.majorGroups = majorGroups;
	}
	
}
