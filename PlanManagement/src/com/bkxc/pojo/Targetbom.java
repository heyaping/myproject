package com.bkxc.pojo;
/**
 * 指标信息实体类
 * @author luo
 *
 */
public class Targetbom {
	private Integer targetID;
	private Integer parentTargetID;
	private String nodePath;
	private String targetName;
	private Integer productID;
	private Integer director;
	private String targetType;
	private String dataType;
	private String targeUpperLimit;
	private String targeLowLimit;
	private String targeFactVal;
	private String unit;
	private String remark;
	private Integer x;
	private Integer y;
	private Integer width;
	private Integer height;
	private Integer isOpen;
	private Integer monitorStatus;
	private String createDT;
	private String creator;
	private String modifyDT;
	private String modifier;
	//产品名字
	private String productName;
	//单位转换
	private String unitName;
	private String directorName;
	private Integer modelID;
	private String targetTypeName;
	private Integer taskID;
	private String dataTypeName;
	private Integer upKanbanID;
	
	public String getDataTypeName() {
		return dataTypeName;
	}
	public void setDataTypeName(String dataTypeName) {
		this.dataTypeName = dataTypeName;
	}
	public Integer getTargetID() {
		return targetID;
	}
	public void setTargetID(Integer targetID) {
		this.targetID = targetID;
	}
	public Integer getParentTargetID() {
		return parentTargetID;
	}
	public void setParentTargetID(Integer parentTargetID) {
		this.parentTargetID = parentTargetID;
	}
	public String getNodePath() {
		return nodePath;
	}
	public void setNodePath(String nodePath) {
		this.nodePath = nodePath;
	}
	public String getTargetName() {
		return targetName;
	}
	public void setTargetName(String targetName) {
		this.targetName = targetName;
	}
	public Integer getProductID() {
		return productID;
	}
	public void setProductID(Integer productID) {
		this.productID = productID;
	}
	public Integer getDirector() {
		return director;
	}
	public void setDirector(Integer director) {
		this.director = director;
	}
	public String getTargetType() {
		return targetType;
	}
	public void setTargetType(String targetType) {
		this.targetType = targetType;
	}
	public String getDataType() {
		return dataType;
	}
	public void setDataType(String dataType) {
		this.dataType = dataType;
	}
	public String getTargeUpperLimit() {
		return targeUpperLimit;
	}
	public void setTargeUpperLimit(String targeUpperLimit) {
		this.targeUpperLimit = targeUpperLimit;
	}
	public String getTargeLowLimit() {
		return targeLowLimit;
	}
	public void setTargeLowLimit(String targeLowLimit) {
		this.targeLowLimit = targeLowLimit;
	}
	public String getTargeFactVal() {
		return targeFactVal;
	}
	public void setTargeFactVal(String targeFactVal) {
		this.targeFactVal = targeFactVal;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public Integer getX() {
		return x;
	}
	public void setX(Integer x) {
		this.x = x;
	}
	public Integer getY() {
		return y;
	}
	public void setY(Integer y) {
		this.y = y;
	}
	public Integer getWidth() {
		return width;
	}
	public void setWidth(Integer width) {
		this.width = width;
	}
	public Integer getHeight() {
		return height;
	}
	public void setHeight(Integer height) {
		this.height = height;
	}
	public Integer getIsOpen() {
		return isOpen;
	}
	public void setIsOpen(Integer isOpen) {
		this.isOpen = isOpen;
	}
	public Integer getMonitorStatus() {
		return monitorStatus;
	}
	public void setMonitorStatus(Integer monitorStatus) {
		this.monitorStatus = monitorStatus;
	}
	public String getCreateDT() {
		return createDT;
	}
	public void setCreateDT(String createDT) {
		this.createDT = createDT;
	}
	public String getCreator() {
		return creator;
	}
	public void setCreator(String creator) {
		this.creator = creator;
	}
	public String getModifyDT() {
		return modifyDT;
	}
	public void setModifyDT(String modifyDT) {
		this.modifyDT = modifyDT;
	}
	public String getModifier() {
		return modifier;
	}
	public void setModifier(String modifier) {
		this.modifier = modifier;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getUnitName() {
		return unitName;
	}
	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}
	public String getDirectorName() {
		return directorName;
	}
	public void setDirectorName(String directorName) {
		this.directorName = directorName;
	}
	public Integer getModelID() {
		return modelID;
	}
	public void setModelID(Integer modelID) {
		this.modelID = modelID;
	}
	public String getTargetTypeName() {
		return targetTypeName;
	}
	public void setTargetTypeName(String targetTypeName) {
		this.targetTypeName = targetTypeName;
	}
	public Integer getTaskID() {
		return taskID;
	}
	public void setTaskID(Integer taskID) {
		this.taskID = taskID;
	}
	public Integer getUpKanbanID() {
		return upKanbanID;
	}
	public void setUpKanbanID(Integer upKanbanID) {
		this.upKanbanID = upKanbanID;
	}
	
}
