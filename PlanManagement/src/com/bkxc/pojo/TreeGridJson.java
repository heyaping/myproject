package com.bkxc.pojo;

import java.util.List;

public class TreeGridJson {
	private int id;
	private String name;
	private List<TreeGridJson> children;
	private String iconCls;
	private String targetType;
	private String dataType;
	private String targeUpperLimit;
	private String targeLowLimit;
	private String targeFactVal;
	
	
	public String getTargetType() {
		return targetType;
	}
	public void setTargetType(String targetType) {
		this.targetType = targetType;
	}
	public String getDataType() {
		return dataType;
	}
	public void setDataType(String dataType) {
		this.dataType = dataType;
	}
	public String getTargeUpperLimit() {
		return targeUpperLimit;
	}
	public void setTargeUpperLimit(String targeUpperLimit) {
		this.targeUpperLimit = targeUpperLimit;
	}
	public String getTargeLowLimit() {
		return targeLowLimit;
	}
	public void setTargeLowLimit(String targeLowLimit) {
		this.targeLowLimit = targeLowLimit;
	}
	public String getTargeFactVal() {
		return targeFactVal;
	}
	public void setTargeFactVal(String targeFactVal) {
		this.targeFactVal = targeFactVal;
	}
	public String getIconCls() {
		return iconCls;
	}
	public void setIconCls(String iconCls) {
		this.iconCls = iconCls;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<TreeGridJson> getChildren() {
		return children;
	}
	public void setChildren(List<TreeGridJson> children) {
		this.children = children;
	}
	
}
