package com.bkxc.pojo;
/**
 * 一级指标看板信息实体类
 * @author luo
 *
 */
public class Upboard {
	private Integer upKanbanID;
	private Integer taskID;
	private Integer userID ;
	private Integer targetID;
	private String targetName;
	public Integer getUpKanbanID() {
		return upKanbanID;
	}
	public void setUpKanbanID(Integer upKanbanID) {
		this.upKanbanID = upKanbanID;
	}
	public Integer getTaskID() {
		return taskID;
	}
	public void setTaskID(Integer taskID) {
		this.taskID = taskID;
	}
	public Integer getUserID() {
		return userID;
	}
	public void setUserID(Integer userID) {
		this.userID = userID;
	}
	public Integer getTargetID() {
		return targetID;
	}
	public void setTargetID(Integer targetID) {
		this.targetID = targetID;
	}
	public String getTargetName() {
		return targetName;
	}
	public void setTargetName(String targetName) {
		this.targetName = targetName;
	}
	
}
