package com.bkxc.pojo;
/**
 * 产品结构实体类
 * @author luo
 *
 */
public class Productbom {
	private Integer productID;
	private Integer parentProductID;
	private String nodePath;
	private String productName;
	private String createDT;
	private String creator;
	private String modifyDT;
	private String modifier;
	private Integer taskID;
	
	
	public Integer getTaskID() {
		return taskID;
	}
	public void setTaskID(Integer taskID) {
		this.taskID = taskID;
	}
	public Integer getProductID() {
		return productID;
	}
	public void setProductID(Integer productID) {
		this.productID = productID;
	}
	public Integer getParentProductID() {
		return parentProductID;
	}
	public void setParentProductID(Integer parentProductID) {
		this.parentProductID = parentProductID;
	}
	public String getNodePath() {
		return nodePath;
	}
	public void setNodePath(String nodePath) {
		this.nodePath = nodePath;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getCreateDT() {
		return createDT;
	}
	public void setCreateDT(String createDT) {
		this.createDT = createDT;
	}
	public String getCreator() {
		return creator;
	}
	public void setCreator(String creator) {
		this.creator = creator;
	}
	public String getModifyDT() {
		return modifyDT;
	}
	public void setModifyDT(String modifyDT) {
		this.modifyDT = modifyDT;
	}
	public String getModifier() {
		return modifier;
	}
	public void setModifier(String modifier) {
		this.modifier = modifier;
	}
	
}
