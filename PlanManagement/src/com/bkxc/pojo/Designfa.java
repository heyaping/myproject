package com.bkxc.pojo;
/**
 * 设计方案实体类
 * @author luo
 *
 */
public class Designfa {
	private Integer taskID;
	private String taskName;
	private Integer director;
	private Integer status;
	private String remark;
	private String beginDT;
	private String endDT;
	private Integer modelID;
	private Integer productID;
	private Integer targetID;
	private String createDT;
	private String creator;
	private String modifyDT;
	private String modifier;
	private String leaderName;
	
	public String getLeaderName() {
		return leaderName;
	}
	public void setLeaderName(String leaderName) {
		this.leaderName = leaderName;
	}
	public Integer getTaskID() {
		return taskID;
	}
	public void setTaskID(Integer taskID) {
		this.taskID = taskID;
	}
	public String getTaskName() {
		return taskName;
	}
	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}
	public Integer getDirector() {
		return director;
	}
	public void setDirector(Integer director) {
		this.director = director;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getBeginDT() {
		return beginDT;
	}
	public void setBeginDT(String beginDT) {
		this.beginDT = beginDT;
	}
	public String getEndDT() {
		return endDT;
	}
	public void setEndDT(String endDT) {
		this.endDT = endDT;
	}
	public Integer getModelID() {
		return modelID;
	}
	public void setModelID(Integer modelID) {
		this.modelID = modelID;
	}
	public Integer getProductID() {
		return productID;
	}
	public void setProductID(Integer productID) {
		this.productID = productID;
	}
	public Integer getTargetID() {
		return targetID;
	}
	public void setTargetID(Integer targetID) {
		this.targetID = targetID;
	}
	public String getCreateDT() {
		return createDT;
	}
	public void setCreateDT(String createDT) {
		this.createDT = createDT;
	}
	public String getCreator() {
		return creator;
	}
	public void setCreator(String creator) {
		this.creator = creator;
	}
	public String getModifyDT() {
		return modifyDT;
	}
	public void setModifyDT(String modifyDT) {
		this.modifyDT = modifyDT;
	}
	public String getModifier() {
		return modifier;
	}
	public void setModifier(String modifier) {
		this.modifier = modifier;
	}
	
}
