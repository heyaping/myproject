package com.bkxc.pojo;
/**
 * 二级指标看板信息实体类
 * @author luo
 *
 */
public class Downboard {
	private Integer lowKanbanID;
	private Integer upKanbanID;
	private Integer userID;
	private Integer targetID;
	private String targetName;
	public Integer getLowKanbanID() {
		return lowKanbanID;
	}
	public void setLowKanbanID(Integer lowKanbanID) {
		this.lowKanbanID = lowKanbanID;
	}
	public Integer getUpKanbanID() {
		return upKanbanID;
	}
	public void setUpKanbanID(Integer upKanbanID) {
		this.upKanbanID = upKanbanID;
	}
	public Integer getUserID() {
		return userID;
	}
	public void setUserID(Integer userID) {
		this.userID = userID;
	}
	public Integer getTargetID() {
		return targetID;
	}
	public void setTargetID(Integer targetID) {
		this.targetID = targetID;
	}
	public String getTargetName() {
		return targetName;
	}
	public void setTargetName(String targetName) {
		this.targetName = targetName;
	}
	
}
