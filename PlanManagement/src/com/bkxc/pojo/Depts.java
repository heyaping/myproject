package com.bkxc.pojo;
/**
 * 部门实体类
 * @author luo
 *
 */
public class Depts {
	private Integer deptID;
	private String deptCode;
	private String deptShortName;
	private String deptLongName;
	private Integer leaderID;
	private String leaderName;
	private String memberNum;
	private String createDT;
	private String creator;
	private String modifyDT;
	private String modifier;
	private Integer ct;
	
	public Integer getCt() {
		return ct;
	}
	public void setCt(Integer ct) {
		this.ct = ct;
	}
	public String getLeaderName() {
		return leaderName;
	}
	public void setLeaderName(String leaderName) {
		this.leaderName = leaderName;
	}
	public Integer getDeptID() {
		return deptID;
	}
	public void setDeptID(Integer deptID) {
		this.deptID = deptID;
	}
	public String getDeptCode() {
		return deptCode;
	}
	public void setDeptCode(String deptCode) {
		this.deptCode = deptCode;
	}
	public String getDeptShortName() {
		return deptShortName;
	}
	public void setDeptShortName(String deptShortName) {
		this.deptShortName = deptShortName;
	}
	public String getDeptLongName() {
		return deptLongName;
	}
	public void setDeptLongName(String deptLongName) {
		this.deptLongName = deptLongName;
	}
	public Integer getLeaderID() {
		return leaderID;
	}
	public void setLeaderID(Integer leaderID) {
		this.leaderID = leaderID;
	}
	public String getMemberNum() {
		return memberNum;
	}
	public void setMemberNum(String memberNum) {
		this.memberNum = memberNum;
	}
	public String getCreateDT() {
		return createDT;
	}
	public void setCreateDT(String createDT) {
		this.createDT = createDT;
	}
	public String getCreator() {
		return creator;
	}
	public void setCreator(String creator) {
		this.creator = creator;
	}
	public String getModifyDT() {
		return modifyDT;
	}
	public void setModifyDT(String modifyDT) {
		this.modifyDT = modifyDT;
	}
	public String getModifier() {
		return modifier;
	}
	public void setModifier(String modifier) {
		this.modifier = modifier;
	}
	
}
