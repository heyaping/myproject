package com.bkxc.pojo;
/**
 * 参数表实体类
 * @author luo
 *
 */
public class Parameter {
	private Integer parameterID;
	private Integer parameterType;
	private String parameterUnit;
	public Integer getParameterID() {
		return parameterID;
	}
	public void setParameterID(Integer parameterID) {
		this.parameterID = parameterID;
	}
	public Integer getParameterType() {
		return parameterType;
	}
	public void setParameterType(Integer parameterType) {
		this.parameterType = parameterType;
	}
	public String getParameterUnit() {
		return parameterUnit;
	}
	public void setParameterUnit(String parameterUnit) {
		this.parameterUnit = parameterUnit;
	}
	
}
