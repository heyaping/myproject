package com.bkxc.pojo;
/**
 * 战技指标实体类
 * @author luo
 *
 */
public class Xhattr {
	private Integer attrID;
	private String attrName;
	private String attrVal;
	private Integer modelID;
	private String remark;
	public Integer getAttrID() {
		return attrID;
	}
	public void setAttrID(Integer attrID) {
		this.attrID = attrID;
	}
	public String getAttrName() {
		return attrName;
	}
	public void setAttrName(String attrName) {
		this.attrName = attrName;
	}
	public String getAttrVal() {
		return attrVal;
	}
	public void setAttrVal(String attrVal) {
		this.attrVal = attrVal;
	}
	public Integer getModelID() {
		return modelID;
	}
	public void setModelID(Integer modelID) {
		this.modelID = modelID;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
}
