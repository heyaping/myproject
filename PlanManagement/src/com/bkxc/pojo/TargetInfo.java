package com.bkxc.pojo;
/**
 * 指标基本信息
 * @author luo
 *
 */
public class TargetInfo {
	private String dataitem;
	private String value;
	public String getDataitem() {
		return dataitem;
	}
	public void setDataitem(String dataitem) {
		this.dataitem = dataitem;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
}
