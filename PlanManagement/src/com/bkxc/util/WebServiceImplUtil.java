package com.bkxc.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Vector;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import javax.xml.namespace.QName;
import javax.xml.rpc.ParameterMode;
import javax.xml.rpc.ServiceException;

import net.sf.json.JSONArray;

import org.apache.axis.client.Call;
import org.apache.axis.client.Service;
import org.apache.axis.encoding.XMLType;
import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;

import com.bkxc.pojo.TargetInfo;
import com.bkxc.pojo.TreeGridJson;
import com.bkxc.pojo.TreeJson;

public class WebServiceImplUtil {
		private static String newMxl;
		private int id = 0;
		private static String iisUrl;
	//调webService接口解析pxc并拼成json在前台展示
		public String webServiceImpl(String filePath){
			List<TreeGridJson> treelist = new ArrayList<TreeGridJson>();
			Service serive = new Service();
			Call call = null;
			String jsonTree = "";
			Properties props = new Properties();
			InputStream in = null;
			try {
				in = WebServiceImplUtil.class.getResourceAsStream("/system.properties");
				props.load(in);
				iisUrl = props.getProperty("iisUrl");
			} catch (FileNotFoundException e1) {
				e1.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}finally{
				try {
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			try {
				call = (Call)serive.createCall();
				call.setTargetEndpointAddress(new URL(iisUrl));
				call.setOperationName(new QName("http://tempuri.org/","ModelCenterReadPXC"));
				call.addParameter(new QName("http://tempuri.org/", "strPXC"),XMLType.SOAP_STRING,ParameterMode.IN);
				call.setReturnType(XMLType.SOAP_STRING);
				call.setUseSOAPAction(true);
				call.setSOAPActionURI("http://tempuri.org/ModelCenterReadPXC");
				String pxc = readXML(filePath);
				newMxl = call.invoke(new Object[]{pxc}).toString();
				try {
					Document doc = DocumentHelper.parseText(newMxl);
					Element root = doc.getRootElement();
					TreeGridJson t = new TreeGridJson();
					id++;
					t.setId(id);
					if(root.attributeValue("name")!=null){
						t.setName(root.attributeValue("name"));
					}
					if(root.attributeValue("value")!=null){
						if(root.attributeValue("value").indexOf("PreferredFormat")!=-1){
							t.setTargeFactVal(root.attributeValue("value").substring(0, root.attributeValue("value").indexOf("<")));
						}else{
							t.setTargeFactVal(root.attributeValue("value"));
						}
					}
					if(root.attributeValue("state")!=null){
						t.setTargetType(root.attributeValue("state"));
					}
					if(root.attributeValue("type")!=null){
						t.setDataType(root.attributeValue("type"));
					}
					if(root.attributeValue("linked")!=null&&root.attributeValue("linked").equals("True")){
						t.setIconCls("icon-tip");
					}else{
						if(root.attributeValue("state")!=null&&root.attributeValue("state").equals("Input")&&!root.attributeValue("type").equals("nurbs")){
							t.setIconCls("icon-redo");
						}else if(root.attributeValue("state")!=null&&root.attributeValue("state").equals("Output")&&!root.attributeValue("type").equals("nurbs")){
							t.setIconCls("icon-undo");
						}
					}
					t.setChildren(getNodes(root));
					treelist.add(t);
					jsonTree = JSONArray.fromObject(treelist).toString();
				} catch (DocumentException e) {
					e.printStackTrace();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return jsonTree;
		}
		//递归子节点
		public List<TreeGridJson> getNodes(Element node){
			List<TreeGridJson> list = new ArrayList<TreeGridJson>();
			List<Element> l = node.elements("Node");
			if(l.size()>0){
				for (Element element : l) {
					TreeGridJson t = new TreeGridJson();
					id++;
					t.setId(id);
					if(element.attributeValue("name")!=null){
						t.setName(element.attributeValue("name"));
					}
					if(element.attributeValue("value")!=null){
						if(element.attributeValue("value").indexOf("PreferredFormat")!=-1){
							t.setTargeFactVal(element.attributeValue("value").substring(0, element.attributeValue("value").indexOf("<")));
						}else{
							t.setTargeFactVal(element.attributeValue("value"));
						}
					}
					if(element.attributeValue("state")!=null){
						t.setTargetType(element.attributeValue("state"));
					}
					if(element.attributeValue("type")!=null){
						t.setDataType(element.attributeValue("type"));
					}
					if(element.attributeValue("linked")!=null&&element.attributeValue("linked").equals("True")){
						t.setIconCls("icon-tip");
					}else{
						if(element.attributeValue("state")!=null&&element.attributeValue("state").equals("Input")&&!element.attributeValue("type").equals("nurbs")){
							t.setIconCls("icon-redo");
						}else if(element.attributeValue("state")!=null&&element.attributeValue("state").equals("Output")&&!element.attributeValue("type").equals("nurbs")){
							t.setIconCls("icon-undo");
						}
					}
					t.setChildren(getNodes(element));
					list.add(t);
				}
			}
			return list;
		}
		//读xml文件
		public String readXML(String filePath) {
			//创建SAXReader对象  
			SAXReader reader = new SAXReader();
			//读取文件 转换成Document  
			Document document;
			String documentStr = "";
			FileInputStream input = null;
			try {
				input = new FileInputStream(new File(filePath));
				document = reader.read(input);
				
				//document转换为String字符串
				documentStr = document.asXML();
//				//获取根节点  
//				Element root = document.getRootElement();
//				//根节点转换为String字符串  
//				String rootStr = root.asXML();
//				System.out.println("root 字符串 " + rootStr);
//				//获取其中student1节点  
//				Element student1Node = root.element("student1");
//				//student1节点转换为String字符串
//				String student1Str = student1Node.asXML();
//				System.out.println("student1 字符串 " + student1Str);
			} catch (DocumentException e) {
				try {
					input.close();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				File file = new File(filePath);
				String filePath2 = filePath.replace(".pxc", ".zip");
				File zip = new File(filePath2);
				InputStream input2 = null;
				ZipOutputStream zipOut = null;
				try {
					input2 = new FileInputStream(file);
					zipOut = new ZipOutputStream(new FileOutputStream(zip));
					zipOut.putNextEntry(new ZipEntry(file.getName()));
					int temp = 0;
					while((temp = input2.read()) != -1) {
						zipOut.write(temp);
					}
				} catch (FileNotFoundException e2) {
					e2.printStackTrace();
				} catch (IOException e3) {
					e3.printStackTrace();
				} finally{
					try {
						zipOut.close();
						input2.close();
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}
				try {
					ZipFile zipFile = new ZipFile(filePath2);
					Enumeration<Enumeration> enu = (Enumeration<Enumeration>) zipFile.entries();
					while(enu.hasMoreElements()){
						Enumeration<ZipEntry> enu2 = enu.nextElement();
						while(enu2.hasMoreElements()){
							ZipEntry zipEntry = enu2.nextElement();
							String name = zipEntry.getName();
							System.out.println(name);
						}
					}
					for (Enumeration<? extends ZipEntry> entries = zipFile.entries(); entries.hasMoreElements();){
						ZipEntry zipEntry = entries.nextElement();
						InputStream inputStream = zipFile.getInputStream(zipEntry);
						SAXReader reader1 = new SAXReader();
						try {
							documentStr = reader1.read(inputStream).asXML();
						} catch (DocumentException e1) {
							e1.printStackTrace();
						} finally{
							inputStream.close();
						}
					}
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				e.printStackTrace();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} finally{
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			return documentStr;
		}
/*		List<TargetInfo> list = new ArrayList<TargetInfo>();
		public List<TargetInfo> getTargetInfo(String targetName){
			TargetInfo target = new TargetInfo();
			target.setDataitem("指标名称");
			target.setValue(targetName);
			list.add(target);
			try {
				Document doc = DocumentHelper.parseText(newMxl);
				Element el = doc.getRootElement();
				if(el.attributeValue("name").equals(targetName)){
					List<Attribute> atts = el.attributes();
					for (Attribute attribute : atts) {
						if(attribute.getName().equals("fullName")){
							TargetInfo target2 = new TargetInfo();
							target2.setDataitem("全称");
							target2.setValue(attribute.getValue());
							list.add(target2);
						}else if(attribute.getName().equals("state")){
							TargetInfo target2 = new TargetInfo();
							target2.setDataitem("指标类型");
							target2.setValue(attribute.getValue());
							list.add(target2);
						}else if(attribute.getName().equals("startValue")){
							TargetInfo target2 = new TargetInfo();
							target2.setDataitem("指标当前值");
							target2.setValue(attribute.getValue());
							list.add(target2);
						}else if(attribute.getName().equals("upperBound")){
							TargetInfo target2 = new TargetInfo();
							target2.setDataitem("指标上限");
							target2.setValue(attribute.getValue());
							list.add(target2);
						}else if(attribute.getName().equals("lowerBound")){
							TargetInfo target2 = new TargetInfo();
							target2.setDataitem("指标下限");
							target2.setValue(attribute.getValue());
							list.add(target2);
						}
					}
				}else{
					selectNode(el,targetName);
				}
			} catch (DocumentException e) {
				e.printStackTrace();
			}
			return list;
		}
		//遍历节点
		public void selectNode(Element node,String targetName){
			if(list.size()<2){
				List<Element> els = node.elements();
				for (Element element : els) {
					if(element.attributeValue("name").equals(targetName)){
						List<Attribute> atts = element.attributes();
						for (Attribute attribute : atts) {
							if(attribute.getName().equals("fullName")){
								TargetInfo target2 = new TargetInfo();
								target2.setDataitem("全称");
								target2.setValue(attribute.getValue());
								list.add(target2);
							}else if(attribute.getName().equals("state")){
								TargetInfo target2 = new TargetInfo();
								target2.setDataitem("指标类型");
								target2.setValue(attribute.getValue());
								list.add(target2);
							}else if(attribute.getName().equals("startValue")){
								TargetInfo target2 = new TargetInfo();
								target2.setDataitem("指标当前值");
								target2.setValue(attribute.getValue());
								list.add(target2);
							}else if(attribute.getName().equals("upperBound")){
								TargetInfo target2 = new TargetInfo();
								target2.setDataitem("指标上限");
								target2.setValue(attribute.getValue());
								list.add(target2);
							}else if(attribute.getName().equals("lowerBound")){
								TargetInfo target2 = new TargetInfo();
								target2.setDataitem("指标下限");
								target2.setValue(attribute.getValue());
								list.add(target2);
							}
						}
					}else{
						selectNode(element,targetName);
					}
				}
			}
		}*/
		public void writeTargetXml(String targetID,String state,String type,String fullName,String currentValue,String lowerBound,String upperBound,String optVarType,String path){
			String[] targetIDs = targetID.split(",");
			String[] states = state.split(",");
			String[] types = type.split(",");
			String[] fullNames = fullName.split(",");
			String[] currentValues = currentValue.split(",");
			String[] lowerBounds = lowerBound.split(",");
			String[] upperBounds = upperBound.split(",");
			Document document = DocumentHelper.createDocument();
			document.setXMLEncoding("UTF-8");
			OutputFormat format = OutputFormat.createPrettyPrint();
			Element root = document.addElement("Node");
			for (int i = 0; i < targetIDs.length; i++) {
				Element node = root.addElement("Node");
				node.addAttribute("targetID", targetIDs[i]);
				node.addAttribute("state", states[i]);
				node.addAttribute("type", types[i]);
				node.addAttribute("fullName", fullNames[i]);
				node.addAttribute("currentValue", currentValues[i]);
				node.addAttribute("lowerBound", lowerBounds[i]);
				node.addAttribute("upperBound", upperBounds[i]);
				node.addAttribute("optVarType", optVarType);
			}
			XMLWriter write = null;
			try {
				write = new XMLWriter(new FileWriter(new File(path)),format);
				write.write(document);
				write.close();
			} catch (IOException e) {
				e.printStackTrace();
			} finally{
				try {
					write.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		//运行MC解析
		public Vector<Object> runMapping(String pxcPath,String xmlPath){
			Service serive = new Service();
			Vector<Object> vector = null;
			Call call = null;
			try {
				call = (Call)serive.createCall();
				call.setTargetEndpointAddress(new URL(iisUrl));
				call.setOperationName(new QName("http://tempuri.org/","ModelCenterRun"));
				call.addParameter(new QName("http://tempuri.org/", "strPXC"),XMLType.SOAP_STRING,ParameterMode.IN);
				call.addParameter(new QName("http://tempuri.org/", "mapFileStr"),XMLType.SOAP_STRING,ParameterMode.IN);
				call.setReturnType(XMLType.SOAP_VECTOR);
				call.setUseSOAPAction(true);
				call.setSOAPActionURI("http://tempuri.org/ModelCenterRun");
				String pxc = readXML(pxcPath);
				String xml = readXML(xmlPath);
				try {
					vector = (Vector<Object>)call.invoke(new Object[]{pxc,xml});
				} catch (RemoteException e) {
					e.printStackTrace();
				}
			} catch (ServiceException e) {
				e.printStackTrace();
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}
			return vector;
		}
//		public static void main(String[] args) {
//			Service serive = new Service();
//			Call call = null;
//			try {
//				call = (Call)serive.createCall();
//				call.setTargetEndpointAddress(new URL("http://localhost:8082/modelCenterService.asmx"));
//				call.setOperationName(new QName("http://tempuri.org/","ModelCenterRun"));
//				call.addParameter(new QName("http://tempuri.org/", "strPXC"),XMLType.SOAP_STRING,ParameterMode.IN);
//				call.addParameter(new QName("http://tempuri.org/", "mapFileStr"),XMLType.SOAP_STRING,ParameterMode.IN);
//				call.setReturnType(XMLType.SOAP_VECTOR);
//				call.setUseSOAPAction(true);
//				call.setSOAPActionURI("http://tempuri.org/ModelCenterRun");
//				WebServiceImplUtil web = new WebServiceImplUtil();
//				String pxc = web.readXML("E:/temp/1/mcModelFile.pxc");
//				String xml = web.readXML("E:/temp/1/mappingFile.xml");
//				try {
//					Vector vec = (Vector)call.invoke(new Object[]{pxc,xml});
//					System.out.println();
//				} catch (RemoteException e) {
//					e.printStackTrace();
//				}
//			} catch (ServiceException e) {
//				e.printStackTrace();
//			} catch (MalformedURLException e) {
//				e.printStackTrace();
//			}
//		}
}
