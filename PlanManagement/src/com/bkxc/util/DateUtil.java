package com.bkxc.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {
	/**
	 * 获得当前日期并转换
	 * @return
	 */
	public static String getNowDateFormat(){
		Date date = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String newDate = dateFormat.format(date);
		return newDate;
	}
}
