package com.bkxc.interceptor;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.bkxc.pojo.Users;
/**
 * 用户登录与权限拦截器
 * @author luo
 *
 */
public class LoginInterceptor implements HandlerInterceptor {
	//放行的URL
	private List<String> uncheckUrls;
	//带权限的URL
	private List<String> authoritys;
	public void afterCompletion(HttpServletRequest request,
			HttpServletResponse response, Object object, Exception exception)
			throws Exception {
		
	}

	public void postHandle(HttpServletRequest arg0, HttpServletResponse arg1,
			Object arg2, ModelAndView arg3) throws Exception {
		// TODO Auto-generated method stub

	}
	/**
	 * 拦截操作
	 */
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response,
			Object object) throws Exception {
		//得到用户请求的路径
		String url = request.getRequestURI();
		//如果用户请求的URL是放行的URL就返回true代表放行
		if(uncheckUrls.contains(url)){
			return true;
		}else{
			//如果会话里面没有用户信息则返回到登录界面
			if(request.getSession().getAttribute("USER_INFO") == null){
				response.sendRedirect(request.getContextPath()+"/login.action");
				return false;
				//如果用户请求的URL带有权限则查询用户的权限看符不符合，不符合则返回登录界面
			}else if(authoritys.contains(url)){
				if(((Users)request.getSession().getAttribute("USER_INFO")).getRoleID()!=1){
					response.sendRedirect(request.getContextPath()+"/login.action");
					return false;
				}
			}
		}
		return true;
	}
	public List<String> getUncheckUrls() {
		return uncheckUrls;
	}

	public void setUncheckUrls(List<String> uncheckUrls) {
		this.uncheckUrls = uncheckUrls;
	}

	public List<String> getAuthoritys() {
		return authoritys;
	}

	public void setAuthoritys(List<String> authoritys) {
		this.authoritys = authoritys;
	}

}
