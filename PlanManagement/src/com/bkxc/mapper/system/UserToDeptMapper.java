package com.bkxc.mapper.system;

import java.util.List;

import com.bkxc.pojo.Users;
import com.bkxc.pojo.Uservsdept;

public interface UserToDeptMapper {
	public List<Users> selectUserByDept(String deptID);
	public void insertUserToDept(Uservsdept uservsdept);
	public List<Users> selectUserNotDept();
	public void deleteByID(String id);
	public void deleteByUserID(String userID);
	public void deleteByDeptID(String deptID);
}
