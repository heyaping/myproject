package com.bkxc.mapper.system;

import java.util.List;

import com.bkxc.pojo.Depts;
import com.bkxc.pojo.Jobs;

public interface DeptMapper {
	public List<Depts> selectDept();
	public void insertDept(Depts depts);
	public void updateDept(Depts depts);
	public void deleteDept(String deptID);
	public List<Depts> selectUserDept(Integer userID);
	
}
