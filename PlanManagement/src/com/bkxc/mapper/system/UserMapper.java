package com.bkxc.mapper.system;

import java.util.List;

import com.bkxc.pojo.Jobs;
import com.bkxc.pojo.Users;

public interface UserMapper {
	List<Users> selectUserAll();
	void insertUser(Users user);
	void updateUser(Users user);
	void deleteUser(String userID);
	String selectUserNameByID(Integer userID);
	public List<Jobs> selectAllJob();
	List<Users> selectRoleUsers();
	
}
