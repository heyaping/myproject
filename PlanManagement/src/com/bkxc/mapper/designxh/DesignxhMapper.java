package com.bkxc.mapper.designxh;

import java.util.List;
import java.util.Map;

import com.bkxc.pojo.Designxh;
import com.bkxc.pojo.Xhattr;

public interface DesignxhMapper {
	//查询所有设计型号
	public List<Designxh> selectAll();
	//添加设计型号
	public void insertDesignXh(Designxh designxh);
	//修改设计型号
	public void updateDesignXh(Designxh designxh);
	//删除设计型号
	public void deleteDesignXh(String designID);
	//根据设计型号查询战技指标
	public List<Xhattr> selectXhattrByXh(Designxh designxh);
	//添加战技指标
	public void insertXhattr(Xhattr xhattr);
	//修改战记指标
	public void updateXhattr(Xhattr xhattr);
	//删除战记指标
	public void deleteXhattrByID(String xhattrID);
	//根据设计型号删除战记指标
	public void deleteXhattrByXh(String xhID);
	//修改设计型号状态
	public void onModel(String modelID);
	public void offModel(String modelID);
	public List<Designxh> selectXhByName(Designxh designxh);
	public List<Xhattr> selectXhattrByModelAndName(Xhattr xhattr);
}
