package com.bkxc.mapper.role;

import java.util.List;

import com.bkxc.pojo.Roles;

public interface RoleMapper {
	List<Roles> selectRoleAll();
}
