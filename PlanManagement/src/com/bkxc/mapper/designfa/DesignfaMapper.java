package com.bkxc.mapper.designfa;

import java.util.List;
import java.util.Map;

import com.bkxc.pojo.Designfa;
import com.bkxc.pojo.Designxh;
import com.bkxc.pojo.Productbom;

public interface DesignfaMapper {
	public List<Designxh> selectOnXh();
	public List<Designfa> selectByXh(Designxh designxh);
	public void insertDesignfa(Designfa designfa);
	public void updateDesignfa(Designfa designfa);
	public void deleteDesignfa(String taskID);
	public void updateStatusOn(String taskID);
	public void updateStatusOff(String taskID);
	public List<Productbom> selectPro(Productbom productbom);
	public void insertPro(Productbom productbom);
	public void updateDesignfaPro(Productbom productbom);
	public void updatePro(Productbom productbom);
	public void deleteProByID(String productID);
	public void deleteProByParentID(Integer parentProductID);
	public int selectFaProByID(int taskID);
	public List<Integer> selectProByParentID(int parentID);
	public int selectProParentID(int productID);
	public void updateDesignProductID(int productID);
	public List<Productbom> selectProInfoByParentID(int parentID);
	public List<Designfa> selectFaByName(Designfa designfa);
}
