package com.bkxc.mapper.targetStatus;

import java.util.List;

import com.bkxc.pojo.Designfa;
import com.bkxc.pojo.Designxh;
import com.bkxc.pojo.Downboard;
import com.bkxc.pojo.Targetbom;
import com.bkxc.pojo.Upboard;

public interface TargetBoardMapper {
	public List<Designxh> selectAllDesignxh();
	public List<Designfa> selectFaByXh(Designfa designfa);
	public Integer selectTargetIDByDesignFa(int taskID);
	public List<Targetbom> selectTargetInfoByID(Integer targetID);
	public List<Targetbom> selectTargetChildInfo(Integer targetID);
	public List<Upboard> selectUpboardByFA(int taskID);
	public void insertUpboard(Upboard upboard);
	public List<Downboard> selectDownboard(int upKanbanID);
	public void insertDownBoard(Downboard downboard);
	public void deleteDownBoard(int lowKanbanID);
	public void deleteUpBoard(int upKanbanID);
	public void deleteDownBoardByUpKanbanID(int upKanbanID);
}
