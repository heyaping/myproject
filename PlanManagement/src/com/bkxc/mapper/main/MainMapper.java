package com.bkxc.mapper.main;

import java.util.List;

import com.bkxc.pojo.MainMenu;
import com.bkxc.pojo.Users;

public interface MainMapper {
		Users selectUserByUser(Users user);
		List<MainMenu> selectMainMenuAll();
}
