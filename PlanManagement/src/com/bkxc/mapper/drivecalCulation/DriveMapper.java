package com.bkxc.mapper.driveCalculation;

import java.util.List;

import com.bkxc.pojo.TargetInfo;
import com.bkxc.pojo.Targetbom;

public interface DriveMapper {
	public List<TargetInfo> selectTargetByID(int targetID);
	public void updateTargetValue(Targetbom targetbom);
}
