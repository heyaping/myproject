package com.bkxc.mapper.targetbom;

import java.util.List;
import java.util.Map;

import com.bkxc.pojo.Designfa;
import com.bkxc.pojo.Designxh;
import com.bkxc.pojo.Parameter;
import com.bkxc.pojo.Targetbom;
public interface TargetbomMapper {
	//下拉框型号查询
	public List<Designxh> selectDesignxh();
	//下拉框根据型号查方案
	public List<Designfa> selectDesignfa(int modelID);
	//根据选中的方案获得父节点
	public List<Targetbom> selectParentTarget(int taskID);
	//根据父节点找下一子节点
	public List<Targetbom> selectChildNode(int parentTargetID);
	//下拉框指标类型
	public List<Parameter> selectTargaType();
	//下拉框数据类型
	public List<Parameter> selectDateType();
	//下拉框国际单位
	public List<Parameter> selectUnit();
	//获取产品id
	public int selectProductID(int taskID);
	//增加指标节点
	public void addTargetbom(Targetbom targetbom);
	//或许当前最大id
	public  int selectMaxID();
	//方案关联当前指标结构
	public  void updateGlTargetbom();
	//修改方案对应的指标
	public void updateFaTarget(Designfa designfa);
	//单条查询
	public List<Targetbom> selectTargetbom(int TargetbomID);
	//删除指标
	public void deleteTargetbom(int TargetbomID);
	//修改指标
	public void updateTargetbom(Targetbom targetbom);
	//查询选中产品结构节点关联的指标
	public List<Targetbom> selectProducts(int product);
	//条件查询
	public List<Targetbom> selectSearch(Map<String, Object> map);
	//查询一级指标
	public List<Targetbom> selectUpboard(int taskID);
	//查询二级指标
	public List<Targetbom> selectDownboard(int upkanbanID);
}
