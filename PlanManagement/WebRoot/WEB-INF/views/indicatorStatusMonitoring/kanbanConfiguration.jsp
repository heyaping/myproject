<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"  %>
<%@page import="java.text.SimpleDateFormat"%> 
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"> <!-- HTML5 document type -->
<html>
  <head>
    <base href="<%=basePath%>">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>指标看板</title>
	<link rel="stylesheet" type="text/css" href="STATIC/css/default.css">
	<link rel="stylesheet" type="text/css" href="STATIC/js/jquery-easyui-1.3.5/themes/gray/easyui.css">
	<link rel="stylesheet" type="text/css" href="STATIC/js/jquery-easyui-1.3.5/themes/icon.css" />
	<link href="STATIC/css/main.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="STATIC/css/mainstyles.css" type="text/css"></link>
	<script type="text/javascript" src="STATIC/js/jquery.min.js"></script>
	<script type="text/javascript" src="STATIC/js/work.js"></script>
	<script type="text/javascript" src="STATIC/js/jquery-easyui-1.3.5/jquery-1.10.2.min.js"></script>
	<script type="text/javascript" src="STATIC/js/jquery-easyui-1.3.5/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="STATIC/js/JQuery-zTree-v3.5.15/jquery.ztree.all-3.5.min.js"></script>
	<script type="text/javascript" src="STATIC/js/index.js"></script>
	<script type="text/javascript" src="STATIC/js/extends.js"></script>
	<script type="text/javascript" src="STATIC/js/common.js"></script>
  </head>
  <script type="text/javascript">  
  	var url = "";
    /* 启动时加载 */  
    $(function(){  
         /*加载型号-方案*/
          $("#slelct_Model").combobox({editable:false});
          $('#slelct_Model').combobox({      
            url:'/PlanManagement/targetBoard/modelInfo.action',      
            valueField:'modelID', 
            panelHeight:'auto',     
            textField:'modelName',
            onChange:function(modelID){
		    	//$('#city').combobox('clear');
		    	$('#slelct_Project').combobox({
			    valueField:'taskID', //值字段
			    textField:'taskName', //显示的字段
			    url:'/PlanManagement/TargetbomManager/getDesignfa.action?modelID='+modelID,
			    panelHeight:'auto',
		 		});
			 }
            });
          $("#slelct_Project").combobox({editable:false});
          $('#slelct_Project').combobox({      
           	url:'/PlanManagement/targetBoard/projectInfo.action',      
            valueField:'taskID', 
            panelHeight:'auto',     
            textField:'taskName',
            onChange:function(taskID){
				initTree(taskID);
				$("#oneBoard_tab").datagrid("reload",{taskID:taskID});
			 }
            });
            /* 启动时加载 */  
     $("#oneBoard_tab").datagrid({  
            fitColumns :true, 
            singleSelect: true,
            selectOnCheck: true,
            striped:true,
            nowrap:true, 
            url: '/PlanManagement/targetBoard/searchOneBoard.action',  
            queryParams:{taskID:0},
            loadMsg:'加载中...',  
            fit: true,
            rowStyler:function(index,row){
			if (index%2==1){
				return 'background-color:#D2D7D3;';
			}
			 else{
			return 'background-color:#50a3a2;';
			}  
		},  
            onClickRow:function(rowIndex,rowData){
            	 $("#twoBoard_tab").datagrid("reload",{upKanbanID:rowData.upKanbanID});
            },
            columns: [[  
                { field: 'targetID', title: '指标编号', width:10},  
                { field: 'targetName', title: '指标名称', width:80}, 
            ]]            
        });
                 /* 启动时加载 */  
     $("#twoBoard_tab").datagrid({  
            fitColumns :true, 
            checkOnSelect:true,
            singleSelect: true,
            striped:true,
            nowrap:true, 
            url: '/PlanManagement/targetBoard/searchTwoBoard.action', 
            queryParams:{upKanbanID:-1}, 
            loadMsg:'加载中...',  
            fit: true,
            rowStyler:function(index,row){
			if (index%2==1){
				return 'background-color:#D2D7D3;';
			}
			 else{
			return 'background-color:#50a3a2;';
			}  
		},  
            columns: [[  
                { field: 'targetID', title: '指标编号', width:10},  
                { field: 'targetName', title: '指标名称', width:80},  
            ]]            
        });
      }) ;
        function initbutton(){
        	$("#button1").linkbutton("disable");
        	$("#button2").linkbutton("disable");
        	$("#button3").linkbutton("disable");
        	$("#button4").linkbutton("disable");
        }
        function addOne(){
        	var target = $("#targetTree").tree('getSelected');
        	var taskID = $("#slelct_Project").combobox("getValue");
        	if(target == null){
        		$.messager.alert("提示", "请先选择指标节点！", "info");
        		return;
        	}else{
				$.ajax({  
                    type: "POST",  
                    async: false,  
                    url: "/PlanManagement/targetBoard/addOneBoard.action?taskID="+taskID+"&targetID="+target.id+"&targetName="+target.text,  
                    data: 'json',  
                    success: function (result) {  
                        if (result == "true") {  
                            $.messager.alert("提示", "添加指标看板成功！", "info");  
                            $("#oneBoard_tab").datagrid('reload');  
                        } else {  
                            $.messager.alert("提示", "操作失败，请重新操作！", "info");  
                            return;  
                        }  
                    }  
                });  
        	}
        }
        function addTwo(){
        	var target = $("#targetTree").tree('getSelected');
        	var upboard = $("#oneBoard_tab").datagrid("getSelected");
        	if(upboard == null){
        		$.messager.alert("提示", "请先选择一级看板！", "info");
        		return;
        	}else if(target == null){
				$.messager.alert("提示", "请先选择指标节点！", "info");
				return;
			}else{
				$.ajax({  
                    type: "POST",  
                    async: false,  
                    url: "/PlanManagement/targetBoard/addTwoBoard.action?targetID="+target.id+"&targetName="+target.text+"&upKanbanID="+upboard.upKanbanID,  
                    data: 'json',  
                    success: function (result) {  
                        if (result == "true") {  
                            $.messager.alert("提示", "添加指标看板成功！", "info");  
                            $("#twoBoard_tab").datagrid('reload');  
                        } else {  
                            $.messager.alert("提示", "操作失败，请重新操作！", "info");  
                            return;  
                        }  
                    }  
                });  
			}
        }
        function removeOne(){
        	var upboard = $("#oneBoard_tab").datagrid("getSelected");
        	if(upboard == null){
        		$.messager.alert("提示", "请选择需要删除的一级看板！", "info");
        		return;
        	}else{
        		$.messager.confirm('提示', '是否删除选中的一级看板,并删除一级看板底下二级看板?', function (r) {  
                if (!r) {  
                    return;  
                }  
        		$.ajax({  
                    type: "POST",  
                    async: false,  
                    url: "/PlanManagement/targetBoard/removeUpboard.action?upKanbanID="+upboard.upKanbanID,  
                    data: 'json',  
                    success: function (result) {  
                        if (result == "true") {  
                            $.messager.alert("提示", "删除看板成功！", "info");  
                            $("#oneBoard_tab").datagrid('reload');  
                            $("#twoBoard_tab").datagrid('reload');  
                        } else {  
                            $.messager.alert("提示", "操作失败，请重新操作！", "info");  
                            return;  
                        }  
                    }  
                });  
                });
        	}
        }
        function removeTwo(){
        	var downboard = $("#twoBoard_tab").datagrid("getSelected");
        	if(downboard == null){
        		$.messager.alert("提示", "请选择需要删除的二级看板！", "info");
        		return;
        	}else{
        		$.messager.confirm('提示', '是否删除选中数据?', function (r) {  
                if (!r) {  
                    return;  
                }  
        		$.ajax({  
                    type: "POST",  
                    async: false,  
                    url: "/PlanManagement/targetBoard/removeDownboard.action?lowKanbanID="+downboard.lowKanbanID,  
                    data: 'json',  
                    success: function (result) {  
                        if (result == "true") {  
                            $.messager.alert("提示", "删除看板成功！", "info");  
                            $("#twoBoard_tab").datagrid('reload');  
                        } else {  
                            $.messager.alert("提示", "操作失败，请重新操作！", "info");  
                            return;  
                        }  
                    }  
                });  
                });
        	}
        }
       
    	function initTree(taskID){
	      $("#targetTree").tree({
			    url: '/PlanManagement/targetBoard/targetTree.action?taskID='+taskID,
			    onLoadSuccess:function(node,data){
				var t = $(this);
				t.tree('expandAll');
				},
			}); 
	      }
  	
     </script> 
      <body style="min-width:1280px;min-height:1024px;" id="layout" onload="init()">
		<div class="wrap">
			<div class="left-right-middle1">
			<jsp:include page="/WEB-INF/views/main/head.jsp" flush="true" ></jsp:include>
				<div class="easyui-layout"  style="min-width:1280px;min-height:908px;">
				<div data-options="region:'north',title:'指标看板配置',split:false,collapsible:false"   style="height:82px;background-color: #F1F3F7">
      	 <div style="width: 500px;height:50px; float:left;" >
         <table style=" height: 50px;" cellspacing="0" border="0">  
                <tr>  
                    <td> &nbsp 型号</td>  
                    <td><input id="slelct_Model" name="userid"  style="width: 180px;height: 26px;"></td> 
                    <td> &nbsp 方案</td>  
                    <td><input id="slelct_Project" name="userid"  style="width: 180px;height: 26px;"></td> 
                </tr>  
          </table> 
          </div>
        </div> 
		<div data-options="region:'center',split:false,collapsible:false"  style="overflow:hidden;background-color: #F1F3F7;">
		<div style="height:380px;width:966px;  border-top:1px solid #ccc; margin-top:3px">
		<table id="oneBoard_tab" border="0" title="一级看板"></table>
		</div>
		<div style="height:440px;width:966px;  ">
		<table id="twoBoard_tab" border="0" title="二级看板"></table>
		</div>
		</div>
		<div data-options="region:'west',split:false,collapsible:false"  style="width:316px;height:98%;background-color: #F1F3F7; overflow:hidden;">
		  <table border="0" style="width:98%; height:100%;" >
		   <tr>
		   <td>
		   <div style="width:220px;height:100%;border:1px solid #D2D7D3;  ">
		   <ul id="targetTree"></ul>
			</div> 
			</td>
			<td>
			 <div style="width:100px;height:100%;float:right; ">
			 <table style="width: auto; height: 50px;" cellspacing="0" border="0" > 
                <tr >  
                    <td style="padding-top:160px">
                   <a id="button1" href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" onclick="addOne();">增加</a> 
                    <a id="button2" href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-Remove" onclick="removeOne();">删除</a> 
                    </td>             
                </tr>  
                <tr>
                  <td style="padding-top:280px">
                   <a id="button3" href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" onclick="addTwo();">增加</a> 
                    <a id="button4" href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-Remove" onclick="removeTwo();">删除</a> 
                    </td>             
                </tr>
          </table> 
			</div>
			</td>
		   </tr>
		  </table>
		</div>
				</div>
			</div>
		</div>
	</body>
</html>
