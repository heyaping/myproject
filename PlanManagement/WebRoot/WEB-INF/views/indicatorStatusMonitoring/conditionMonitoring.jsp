<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"  %>
<%@page import="java.text.SimpleDateFormat"%> 
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"> <!-- HTML5 document type -->
<html>
  <head>
    <base href="<%=basePath%>">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>指标状态</title>
	<link rel="stylesheet" type="text/css" href="STATIC/css/default.css">
	<link rel="stylesheet" type="text/css" href="STATIC/js/jquery-easyui-1.3.5/themes/gray/easyui.css">
	<link rel="stylesheet" type="text/css" href="STATIC/js/jquery-easyui-1.3.5/themes/icon.css" />
	<link href="STATIC/css/main.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="STATIC/css/mainstyles.css" type="text/css"></link>
	<script type="text/javascript" src="STATIC/js/jquery.min.js"></script>
	<script type="text/javascript" src="STATIC/js/work.js"></script>
	<script type="text/javascript" src="STATIC/js/jquery-easyui-1.3.5/jquery-1.10.2.min.js"></script>
	<script type="text/javascript" src="STATIC/js/jquery-easyui-1.3.5/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="STATIC/js/JQuery-zTree-v3.5.15/jquery.ztree.all-3.5.min.js"></script>
	<script type="text/javascript" src="STATIC/js/index.js"></script>
	<script type="text/javascript" src="STATIC/js/extends.js"></script>
	<script type="text/javascript" src="STATIC/js/common.js"></script>
	<script src="STATIC/js/gojs/go.src.js"></script>
    <script type="text/javascript" src="STATIC/js/gojs/go-debug.js"></script>
  </head>
 
   <script type="text/javascript">  
  	var url = "";
  	var groudjson="";
    /* 启动时加载 */  
    $(function(){  
    $('#channelFeeTab').tabs({
	   border:false,
	   onSelect:function(title,index){
	   	if(index==0){
	   		
	   	}else if(index==1){
	
	   	  kanbanDiagram.model = go.Model.fromJson(groudjson);
	       }else if(index==2){
	       }else{
	       }
	   }
	});
    
         /*加载型号-方案*/
          $("#slelct_Model").combobox({editable:false});
          $('#slelct_Model').combobox({      
           url:'/PlanManagement/TargetbomManager/getDesignxh.action',      
            valueField:'modelID', 
            panelHeight:'auto',     
            textField:'modelName',
            onLoadSuccess: function () {
                $(this).combobox('setText', '--请选择--');
                
            },
             onChange:function(modelID){
		    	//$('#city').combobox('clear');
		    	$('#slelct_Project').combobox({
			    valueField:'taskID', //值字段
			    textField:'taskName', //显示的字段
			    url:'/PlanManagement/TargetbomManager/getDesignfa.action?modelID='+modelID,
			    panelHeight:'auto',
			    value:'--请选择--',
		 	});
		    }
            });
          $("#slelct_Project").combobox({editable:false});
      		$('#slelct_Project').combobox({
		    valueField:'taskID', //值字段
		    textField:'taskName', //显示的字段
		    //url:'/PlanManagement/TargetbomManager/getDesignfa.action?modelID=${Designfa.modelID}',
		    panelHeight:'auto',
		    value:'${Designfa.modelID}',
		    onChange:function(taskID){
		       //提交  
                $.ajax({  
                    type: "POST",  
                    async: false,  
                    url: "/PlanManagement/TargetbomManager/getjson.action?taskID=" + taskID,  
                    data: 'string',  
                    success: function (str) {  
                    json=str;
                    }
							 });
				//看板请求
				 $.ajax({  
                    type: "POST",  
                    async: false,  
                    url: "/PlanManagement/TargetbomManager/getKbjson.action?taskID=" + taskID,  
                    data: 'json',  
                    success: function (kbstr) {  
                    groudjson=kbstr;
                    }
							 });			 
		 		//请求后台传给前台json数据
                  if(json==""){
                  json="[]";
                  $.messager.alert("提示", "当前方案下没有指标树！", "info");
                  }
                  var obj = eval('(' + json + ')');
               myDiagram.model = new go.TreeModel(obj); 
               kanbanDiagram.model = go.Model.fromJson(groudjson);
		    }
		 });
      }) ;
  	 function init() {
		    var $ = go.GraphObject.make;  // for conciseness in defining templates
		    myDiagram =
		      $(go.Diagram, "myDiagramDiv",
        {
          isReadOnly: true, 
          initialContentAlignment: go.Spot.Left,
          // define the layout for the diagram
          layout: $(go.TreeLayout, { nodeSpacing: 20, layerSpacing: 40 })
        });
    // Define a simple node template consisting of text followed by an expand/collapse button
    myDiagram.nodeTemplate =
      $(go.Node, "Horizontal",
        { selectionChanged: nodeSelectionChanged },  // this event handler is defined below
        $(go.Panel, "Table",{height:28},
        { defaultAlignment: go.Spot.Left},
         $(go.RowColumnDefinition, { column: 0, separatorStroke: "#1F4963" }),
          $(go.RowColumnDefinition, { column: 1, separatorStroke: "white"},new go.Binding("background", "color")),
           $(go.RowColumnDefinition, { row: 0, separatorStroke: "#1F4963" }),
          $(go.Shape, "Rectangle",  
          { fill: "#1F4963", stroke: null ,width:188}
           ,
            new go.Binding("width","index",function(i){
            return parseInt(i)>0 ? 136  : 188;
            })
          ),
          $(go.TextBlock,
            { row: 0, column: 0},
            { font: "bold 13px Helvetica, bold Arial, sans-serif",
              stroke: "white", margin:10 ,width: 85,
              isMultiline:false,textAlign:'left',
               },
            new go.Binding("text", "key")
            ),
              $(go.TextBlock,
            { row: 0, column: 1},
            { font: "bold 13px Helvetica, bold Arial, sans-serif",
              stroke: "white", margin: 1 ,width: 52,
           		isMultiline:false ,visible:false,textAlign:'left',
               },
            new go.Binding("text", "index"),
            new go.Binding("visible","index",function(i){
            return parseInt(i)>0 ? true  : false;
            })
            )
            ),
        $("TreeExpanderButton")
      );
    // Define a trivial link template with no arrowhead.
    myDiagram.linkTemplate =
      $(go.Link,
        { selectable: false },
        $(go.Shape));  // the link shape
 	 var nodeDataArray = [];
 	  myDiagram.model = new go.TreeModel(nodeDataArray); 
  		}
	  function nodeSelectionChanged(node) {
	    if (node.isSelected) {
	    //  names[node.data.name].style.backgroundColor = "lightblue";
	    var targetbomID=node.data.id;
	    thisemp=node.data;
	    $("#targetbom_tab").datagrid({  
            checkOnSelect: false,  
            fitColumns :true, 
            checkOnSelect:true,
            singleSelect: false,
            selectOnCheck: true,
            nowrap:true, 
            url: '/PlanManagement/TargetbomManager/selectTargetbom.action?TargetbomID=' + targetbomID, 
            loadMsg:'加载中...',  
            fit: true,
            rowStyler:function(index,row){
			if (index%2==1){
				return 'background-color:#CDB38B;';
			}
			 else{
			return 'background-color:#50a3a2;';
			} 
		},  
            columns: [[  
                { field: 'targetID', title: '编号', width:50},  
                { field: 'targetName', title: '指标名称', width:100},  
                { field: 'productName', title: '产品名称', width:100},
                { field: 'productID', title: '产品编号', width:100 },  
                { field: 'targeUpperLimit', title: '上限值', width:160},  
                { field: 'targeLowLimit', title: '下限值', width:160},  
                { field: 'targeFactVal', title: '当前值', width:100},  
                { field: 'unit', title: '单位', width:100},
                { field: 'unitName', title: '单位名称', width:100},
                { field: 'remark', title: '备注', width:150},  
                { field: 'director', title: '负责人', width:150},
                { field: 'targetType', title: '指标类型', width:150},
                { field: 'dataType', title: '数据类型', width:150},
                { field: 'directorName', title: '负责人名字', width:150},
            ]]
        });
          $("#targetbom_tab").datagrid("hideColumn", "productID"); // 设置隐藏列   
          $("#targetbom_tab").datagrid("hideColumn", "unit"); // 设置隐藏列  
          $("#targetbom_tab").datagrid("hideColumn", "director"); // 设置隐藏列  
          $("#targetbom_tab").datagrid("hideColumn", "targetType"); // 设置隐藏列  
          $("#targetbom_tab").datagrid("hideColumn", "dataType"); // 设置隐藏列  
           $("#targetbom_tab").datagrid("hideColumn", "directorName"); // 设置隐藏列   
	    } else {
	    //  names[node.data.name].style.backgroundColor = "";
      $('#targetbom_tab').datagrid('loadData',{total:0,rows:[]});
	    }
	  }
	  
  // For the layout
  var MINLENGTH = 200;  // this controls the minimum length of any swimlane
  var MINBREADTH = 100;  // this controls the minimum breadth of any non-collapsed swimlane
  // some shared functions
  // this is called after nodes have been moved
  function relayoutDiagram() {
    kanbanDiagram.selection.each(function(n) { n.invalidateLayout(); });
    kanbanDiagram.layoutDiagram();
  }
  // compute the minimum size of the whole diagram needed to hold all of the Lane Groups
  function computeMinPoolSize() {
    var len = MINLENGTH;
    kanbanDiagram.findTopLevelGroups().each(function(lane) {
      var holder = lane.placeholder;
      if (holder !== null) {
        var sz = holder.actualBounds;
        len = Math.max(len, sz.height);
      }
      var box = lane.selectionObject;
      // naturalBounds instead of actualBounds to disregard the shape's stroke width
      len = Math.max(len, box.naturalBounds.height);
    });
    return new go.Size(NaN, len);
  }
  // compute the minimum size for a particular Lane Group
  function computeLaneSize(lane) {
    // assert(lane instanceof go.Group);
    var sz = computeMinLaneSize(lane);
    if (lane.isSubGraphExpanded) {
      var holder = lane.placeholder;
      if (holder !== null) {
        var hsz = holder.actualBounds;
        sz.width = Math.max(sz.width, hsz.width);
      }
    }
    // minimum breadth needs to be big enough to hold the header
    var hdr = lane.findObject("HEADER");
    if (hdr !== null) sz.width = Math.max(sz.width, hdr.actualBounds.width);
    return sz;
  }
  // determine the minimum size of a Lane Group, even if collapsed
  function computeMinLaneSize(lane) {
    if (!lane.isSubGraphExpanded) return new go.Size(1, MINLENGTH);
    return new go.Size(MINBREADTH, MINLENGTH);
  }
  // define a custom grid layout that makes sure the length of each lane is the same
  // and that each lane is broad enough to hold its subgraph
  function PoolLayout() {
    go.GridLayout.call(this);
    this.cellSize = new go.Size(1, 1);
    this.wrappingColumn = Infinity;
    this.wrappingWidth = Infinity;
    this.spacing = new go.Size(0, 0);
    this.alignment = go.GridLayout.Position;
  }
  go.Diagram.inherit(PoolLayout, go.GridLayout);
  /** @override */
  PoolLayout.prototype.doLayout = function(coll) {
    var diagram = this.diagram;
    if (diagram === null) return;
    diagram.startTransaction("PoolLayout");
    // make sure all of the Group Shapes are big enough
    var minsize = computeMinPoolSize();
    diagram.findTopLevelGroups().each(function(lane) {
      if (!(lane instanceof go.Group)) return;
      var shape = lane.selectionObject;
      if (shape !== null) {  // change the desiredSize to be big enough in both directions
        var sz = computeLaneSize(lane);
        shape.width = (!isNaN(shape.width)) ? Math.max(shape.width, sz.width) : sz.width;
        shape.height = (isNaN(shape.height) ? minsize.height : Math.max(shape.height, minsize.height));
        var cell = lane.resizeCellSize;
        if (!isNaN(shape.width) && !isNaN(cell.width) && cell.width > 0) shape.width = Math.ceil(shape.width / cell.width) * cell.width;
        if (!isNaN(shape.height) && !isNaN(cell.height) && cell.height > 0) shape.height = Math.ceil(shape.height / cell.height) * cell.height;
      }
    });
    // now do all of the usual stuff, according to whatever properties have been set on this GridLayout
    go.GridLayout.prototype.doLayout.call(this, coll);
    diagram.commitTransaction("PoolLayout");
  };
  // end PoolLayout class
  function kanbaninit() {
    var $ = go.GraphObject.make;
    kanbanDiagram =
      $(go.Diagram, "myKbDiagramDiv",
        {
        isReadOnly: true, 
          // start everything in the middle of the viewport
          contentAlignment: go.Spot.TopCenter,
          // use a simple layout to stack the top-level Groups next to each other
          layout: $(PoolLayout),
          // disallow nodes to be dragged to the diagram's background
          mouseDrop: function(e) {
            e.diagram.currentTool.doCancel();
          },
          // a clipboard copied node is pasted into the original node's group (i.e. lane).
          "commandHandler.copiesGroupKey": true,
          // automatically re-layout the swim lanes after dragging the selection
          "SelectionMoved": relayoutDiagram,  // this DiagramEvent listener is
          "SelectionCopied": relayoutDiagram, // defined above
          "animationManager.isEnabled": false,
          "undoManager.isEnabled": true
        });
    // Customize the dragging tool:
    // When dragging a Node set its opacity to 0.7 and move it to the foreground layer
    kanbanDiagram.toolManager.draggingTool.doActivate = function() {
      go.DraggingTool.prototype.doActivate.call(this);
      this.currentPart.opacity = 0.7;
      this.currentPart.layerName = "Foreground";
    }
    kanbanDiagram.toolManager.draggingTool.doDeactivate = function() {
      this.currentPart.opacity = 1;
      this.currentPart.layerName = "";
      go.DraggingTool.prototype.doDeactivate.call(this);
    }
    // There are only three note colors by default, blue, red, and yellow but you could add more here:
   /*  var noteColors = ['white','#009CCC', '#CC293D', '#FFD700'];
    function getNoteColor(num) {
      return noteColors[Math.min(num, noteColors.length - 1)];
    } */
    kanbanDiagram.nodeTemplate =
      $(go.Node, "Horizontal",
        new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
        $(go.Panel, "Auto",
          $(go.Shape, "Rectangle", { fill: "white", stroke: '#CCCCCC' },new go.Binding("fill", "color")),
          $(go.Panel, "Table",
            { width: 130, minSize: new go.Size(NaN, 50) },
            $(go.TextBlock,
              {
                name: 'TEXT',
                margin: 6, font: 'bold 15px Helvetica, bold Arial, sans-serif', editable: false,
                stroke: "#000", maxSize: new go.Size(130, NaN),
                alignment: go.Spot.TopLeft
              },
              new go.Binding("text", "text").makeTwoWay())
          )
        )
      );
    // unmovable node that acts as a button
  /*   myDiagram.nodeTemplateMap.add('newbutton',
      $(go.Node, "Horizontal",
        {
          selectable: false,
          click: function(e, node) {
            myDiagram.startTransaction('add node');
            var newdata = { group:"Problems", loc:"0 50", text: "New item " + node.containingGroup.memberParts.count, color: 0 };
            myDiagram.model.addNodeData(newdata);
            myDiagram.commitTransaction('add node');
            var node = myDiagram.findNodeForData(newdata);
            myDiagram.select(node);
            myDiagram.commandHandler.editTextBlock();
          },
          background: 'white'
        },
        $(go.Panel, "Auto",
          $(go.Shape, "Rectangle", { strokeWidth: 0, stroke: null, fill: '#6FB583' }),
          $(go.Shape, "PlusLine", { margin: 6, strokeWidth: 2, width: 12, height: 12, stroke: 'white', background: '#6FB583' })
        ),
        $(go.TextBlock, "New item", { font: '10px Lato, sans-serif', margin: 6,  })
      )
    ); */
    // While dragging, highlight the dragged-over group
    function highlightGroup(grp, show) {
      if (show) {
        var part = kanbanDiagram.toolManager.draggingTool.currentPart;
        if (part.containingGroup !== grp) {
          grp.isHighlighted = true;
          return;
        }
      }
      grp.isHighlighted = false;
    }
    kanbanDiagram.groupTemplate =
      $(go.Group, "Vertical",
        {
          copyable: false,
          movable: false,
          deletable: false,
          selectionAdorned: false,
          selectionObjectName: "SHAPE", // even though its not selectable, this is used in the layout
          layerName: "Background",  // all lanes are always behind all nodes and links
          layout: $(go.GridLayout,  // automatically lay out the lane's subgraph
                    {
                      wrappingColumn: 1,
                      cellSize: new go.Size(1, 1),
                      spacing: new go.Size(5, 5),
                      alignment: go.GridLayout.Position,
                      comparer: function(a, b) {  // can re-order tasks within a lane
                        var ay = a.location.y;
                        var by = b.location.y;
                        if (isNaN(ay) || isNaN(by)) return 0;
                        if (ay < by) return -1;
                        if (ay > by) return 1;
                        return 0;
                      }
                    }),
          computesBoundsAfterDrag: true,  // needed to prevent recomputing Group.placeholder bounds too soon
          handlesDragDropForMembers: true,  // don't need to define handlers on member Nodes and Links
          mouseDragEnter: function(e, grp, prev) { highlightGroup(grp, true); },
          mouseDragLeave: function(e, grp, next) { highlightGroup(grp, false); },
          mouseDrop: function(e, grp) {  // dropping a copy of some Nodes and Links onto this Group adds them to this Group
            // don't allow drag-and-dropping a mix of regular Nodes and Groups
            if (e.diagram.selection.all(function(n) { return !(n instanceof go.Group); })) {
              var ok = grp.addMembers(grp.diagram.selection, true);
              if (!ok) grp.diagram.currentTool.doCancel();
            }
          },
          subGraphExpandedChanged: function(grp) {
            var shp = grp.selectionObject;
            if (grp.diagram.undoManager.isUndoingRedoing) return;
            if (grp.isSubGraphExpanded) {
              shp.width = grp._savedBreadth;
            } else {
              grp._savedBreadth = shp.width;
              shp.width = NaN;
            }
          }
        },
        new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
        new go.Binding("isSubGraphExpanded", "expanded").makeTwoWay(),
        // the lane header consisting of a TextBlock and an expander button
 /*        $(go.Panel, "Auto",
          { name: "HEADER",
            angle: 0,  // maybe rotate the header to read sideways going up
            alignment: go.Spot.Left },
        //  $("SubGraphExpanderButton", { margin: 5 }),  // this remains always visible
          $(go.Panel, "Horizontal",  // this is hidden when the swimlane is collapsed
            new go.Binding("visible", "isSubGraphExpanded").ofObject(),
            $(go.TextBlock,  // the lane label
              { font: "15px Lato, sans-serif", editable: false, margin: new go.Margin(2, 0, 0, 0) },
              new go.Binding("text", "text").makeTwoWay())
          )
        ),  // end Horizontal Panel  */
        $(go.Panel, "Auto",
          $(go.Shape, "Rectangle", { fill: "white", stroke: '#CCCCCC' ,margin: 6},new go.Binding("fill", "color")),
          $(go.Panel, "Table",
            { width: 170, minSize: new go.Size(NaN, 50) },
            $(go.TextBlock,
              {
                name: 'TEXT',
                margin: 6, font: 'bold 13px Helvetica, bold Arial, sans-serif', editable: false,
                stroke: "#000", maxSize: new go.Size(130, NaN),
                alignment: go.Spot.TopLeft,margin: new go.Margin(10)
              },
              new go.Binding("text", "text").makeTwoWay())
          )
        ),
        
        $(go.Panel, "Auto",  // the lane consisting of a background Shape and a Placeholder representing the subgraph
          $(go.Shape, "Rectangle",  // this is the resized object
            { name: "SHAPE", fill: "#F1F1F1", stroke: null, strokeWidth: 4 },
            new go.Binding("fill", "isHighlighted", function(h) { return h ? "#D6D6D6" : "#F1F1F1"; }).ofObject(),
            new go.Binding("desiredSize", "size", go.Size.parse).makeTwoWay(go.Size.stringify)),
          $(go.Placeholder,
            { padding: 12, alignment: go.Spot.TopLeft }),
          $(go.TextBlock,  // this TextBlock is only seen when the swimlane is collapsed
            { name: "LABEL",
              font: "bold 15px Helvetica, bold Arial, sans-serif", editable: false,
              angle: 90, alignment: go.Spot.TopLeft, margin: new go.Margin(4, 0, 0, 2) },
            new go.Binding("visible", "isSubGraphExpanded", function(e) { return !e; }).ofObject(),
            new go.Binding("text", "text").makeTwoWay())
        )  // end Auto Panel
      );  // end Group
    load();
      // Set up a Part as a legend, and place it directly on the diagram
    kanbanDiagram.add(
      $(go.Part, "Table",
        { position: new go.Point(300, 10), selectable: false },
        $(go.TextBlock, "Key",
          { row: 0, font: "700 14px Droid Serif, sans-serif" }),  // end row 0
        $(go.Panel, "Horizontal",
          { row: 1, alignment: go.Spot.Left },
          $(go.Shape, "Rectangle",
            { desiredSize: new go.Size(10, 10), selectable: false,fill: '#CC293D', margin: 5 }),
          $(go.TextBlock, "Halted",
            { font: "700 13px Droid Serif, sans-serif" })
        ),  // end row 1
        $(go.Panel, "Horizontal",
          { row: 2, alignment: go.Spot.Left },
          $(go.Shape, "Rectangle",
            { desiredSize: new go.Size(10, 10), fill: '#FFD700',selectable: false, margin: 5 }),
          $(go.TextBlock, "In Progress",
            { font: "700 13px Droid Serif, sans-serif" })
        ),  // end row 2
        $(go.Panel, "Horizontal",
          { row: 3, alignment: go.Spot.Left },
          $(go.Shape, "Rectangle",
            { desiredSize: new go.Size(10, 10), fill: '#009CCC',selectable: false, margin: 5 }),
          $(go.TextBlock, "Completed",
            { font: "700 13px Droid Serif, sans-serif" })
        )  // end row 3
      ));
  }  // end init
  function load() {
    kanbanDiagram.model = go.Model.fromJson(groudjson);
    kanbanDiagram.isModified = false;
  } 
    
</script> 


 <body style="min-width:1280px;min-height:1024px;" id="layout" onclick="init();kanbaninit()">
		<div class="wrap">
			<div class="left-right-middle1">
			<jsp:include page="/WEB-INF/views/main/head.jsp" flush="true" ></jsp:include>
				<div class="easyui-layout"  style="min-width:1280px;min-height:908px;">
				<div data-options="region:'north',title:'指标状态监控',split:false,collapsible:false"   style="height:78px;background-color: #F1F3F7">
         <table style=" height: 48px;" cellspacing="0" border="0">  
                <tr>  
                    <td> &nbsp 型号</td>  
                    <td><input id="slelct_Model" name="userid"  style="width: 180px;height: 26px;"></td> 
                    <td> &nbsp 方案</td>  
                    <td><input id="slelct_Project" name="userid"  style="width: 180px;height: 26px;"></td> 
                </tr>  
          </table>  
        </div> 
		<div data-options="region:'center',split:false,collapsible:false"   style="padding:8px; height:50%;background-color: #F1F3F7; overflow:hidden;">
           <div class="easyui-tabs" fit="true"   style="width: auto; height:100px;" id="channelFeeTab"   >  
        <div  title="指标树视图" style="padding:10px; overflow:hidden;width:auto;height:60%;background-color: #F1F3F7"  >  
         <div class="easyui-layout" fit="true" style="width:auto;">
       <div data-options="region:'north'  ,collapsible:false"  style="height:55px;background-color: #F1F3F7"  >
        <table style="width: auto; height: 50px;padding:10px" cellspacing="0"   >  
                <tr>
                <td>
                <p style="color:#50a3a2;font-family:楷体;font-size:18px;font-weight:bolder"> &nbsp;<img src="STATIC/img/Red_Icon.png" height="16px" width="16px"></img>指标异常 &nbsp <img src="STATIC/img/Blue_Icon.png" height="16px" width="16px"></img>指标正常 &nbsp <img src="STATIC/img/Yellow_Icon.png" height="16px" width="16px"></img>指标有风险</p>
                </td>
                </tr>
          </table>  
          </div>
        <div data-options="region:'center' ,split:false,collapsible:false" style="height:300px; background-color: #F1F3F7" id="myDiagramDiv"   >
      </div>
  		<div data-options="region:'south',split:false,collapsible:false"  style="height:68px;background-color: #F1F3F7"   >
			<table  id="targetbom_tab" ></table>
		</div>
        </div>  
        </div>
        
       	  <div  title="指标看板视图" style="padding:10px; overflow:hidden;width:auto;height:100%;background-color: #F1F3F7"  >  
         <div class="easyui-layout" fit="true" id="kanban" style="width:auto;">
       <div data-options="region:'north'  ,collapsible:false"  style="height:55px;background-color: #F1F3F7"  >
        <table style="width: auto; height: 50px;padding:10px" cellspacing="0"   >  
                <tr>
                <td>
                <p style="color:#50a3a2;font-family:楷体;font-size:18px;font-weight:bolder"> &nbsp;<img src="STATIC/img/Red_Icon.png" height="16px" width="16px"></img>指标异常 &nbsp <img src="STATIC/img/Blue_Icon.png" height="16px" width="16px"></img>指标正常 &nbsp <img src="STATIC/img/Yellow_Icon.png" height="16px" width="16px"></img>指标有风险</p>
                </td>
                </tr> 
          </table>  
          </div>
       <div data-options="region:'center' ,split:false,collapsible:false"  id="myKbDiagramDiv"  style="background-color: #F1F3F7;" >
      </div>
        </div>   
        </div> 
				</div>
			</div>
		</div>
 </body>

</html>
