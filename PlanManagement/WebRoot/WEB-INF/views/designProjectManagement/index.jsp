<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@page import="java.text.SimpleDateFormat"%> 
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
  <head>
    <base href="<%=basePath%>">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>方案管理</title>
	<link rel="stylesheet" type="text/css" href="STATIC/css/default.css">
	<link rel="stylesheet" type="text/css" href="STATIC/js/jquery-easyui-1.3.5/themes/gray/easyui.css">
	<link rel="stylesheet" type="text/css" href="STATIC/js/jquery-easyui-1.3.5/themes/icon.css" />
	<link href="STATIC/css/main.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="STATIC/css/mainstyles.css" type="text/css"></link>
	<script type="text/javascript" src="STATIC/js/jquery.min.js"></script>
	<script type="text/javascript" src="STATIC/js/work.js"></script>
	<script type="text/javascript" src="STATIC/js/jquery-easyui-1.3.5/jquery-1.10.2.min.js"></script>
	<script type="text/javascript" src="STATIC/js/jquery-easyui-1.3.5/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="STATIC/js/JQuery-zTree-v3.5.15/jquery.ztree.all-3.5.min.js"></script>
	<script type="text/javascript" src="STATIC/js/index.js"></script>
	<script type="text/javascript" src="STATIC/js/extends.js"></script>
	<script type="text/javascript" src="STATIC/js/common.js"></script>
  </head>

  <script type="text/javascript">
  	var proID = '';
  	var parentNodeName = '';
  	var parentNodeID = '';
  	var nodeName = '';
  	var nodeID = '';
  	  
    /* 启动时加载 */  
    $(function(){ 
   	 initButton();
   	 initRight();
     $("#model_list").datagrid({  
            url: '/PlanManagement/designfaManager/getPlanByModel.action',  
            queryParams:{modelID:'0'},
            loadMsg:'加载中...',  
            checkOnSelect: false,  
            fitColumns :true, 
            checkOnSelect:true,
            singleSelect: false,
            selectOnCheck: true,
             rowStyler:function(index,row){
			if (index%2==1){
				return 'background-color:#D2D7D3;';
			}
			 else{
			return 'background-color:#50a3a2;';
			} 
		},  
             //点击行 触发 右边属性显示
            onClickRow:function(rowIndex,rowData){
            removeAttr();
            initpar();
            proID = rowData.productID;
            initTree(proID);
            $(this).datagrid("unselectAll");//取消选中当前所有行
			$(this).datagrid("selectRow",rowIndex);//选中当前点击的行
            XZButton(rowData.status);
         	$('#taskID').val(rowData.taskID);
         	$('#taskName').val(rowData.taskName);
         	$('#leaderName').val(rowData.leaderName);
         	$('#status').val(rowData.status);
         	if(rowData.status == '0'){
         		$('#statusName').val('未启用');
         	}else if(rowData.status == '-1'){
         		$('#statusName').val('已停用');
         	}else{
         		$('#statusName').val('已启用');
         	}
         	$('#remark').val(rowData.remark);
         	$('#beginDT').datebox("setValue",rowData.beginDT);
         	$('#endDT').datebox("setValue",rowData.endDT);
         	$('#director').val(rowData.director);
  					}, 
            columns: [[  
                { field: 'id', checkbox: true},  
                { field: 'taskID', title: '编号', width:20},
                { field: 'taskName', title: '名称', width:100},    
            ]]            
        });
        
          $("#model").combobox({editable:false});
          $('#model').combobox({      
            url:'/PlanManagement/designfaManager/getOnModel.action',      
            valueField:'modelID', 
            panelHeight:'auto',     
            textField:'modelName',
            onSelect:function(){
            	var modelID = $(this).combobox("getValue");
            	if(modelID != '0'){
            		initButton2();
	            	$("#model_list").datagrid("reload",{modelID:modelID});
            	}
            }
        }); 
      }) ;
      function initpar(){
	  	 parentNodeName = '';
	  	 parentNodeID = '';
	  	 nodeName = '';
	  	 nodeID = '';
      }
      function initTree(productID){
      $("#products").tree({
		    url: '/PlanManagement/designfaManager/proTree.action?productID='+productID,
		    onLoadSuccess:function(node,data){
			var t = $(this);
			t.tree('expandAll');
			},
		    onSelect:function(node){
		    	$("#attr2").linkbutton("enable");
		    	$("#attr3").linkbutton("enable");
		    	if($(this).tree("getParent",node.target)){
				parentNodeName = $(this).tree("getParent",node.target).text;
				parentNodeID = $(this).tree("getParent",node.target).id;
		    	}else{
		    	parentNodeName="";
		    	parentNodeID = "";
		    	}
				nodeName = node.text;
				nodeID = node.id;
		    },
		}); 
      }
       //右边只读状态   
      function initRight(){
     // $('input:text').attr("disabled","disabled");//设为不可用
      $('input:text').attr("readonly","readonly");//设为只读
       $("#model").removeAttr("readonly","readonly");//设为只读
      $("#zg").linkbutton("disable");
      $("#remark").attr("readonly","readonly");
      $("#productName").removeAttr("readonly","readonly");
      }
      //右边可修改状态
      function removeAttr(){
      $("#taskName").removeAttr("readonly");//取消只读的设置
      $("#zg").linkbutton("enable");
       $("#remark").removeAttr("readonly","readonly");
      }
      function addButton(){
      	$("#okButton").linkbutton("enable"); 
		$("#removeButton").linkbutton("disable");
    	$("#QYButton").linkbutton("disable");
     	$("#TYButton").linkbutton("disable");
      }
      function initButton(){
      	 $("#addButton").linkbutton("disable");
      	 $("#removeButton").linkbutton("disable");
    	 $("#okButton").linkbutton("disable");
     	 $("#QYButton").linkbutton("disable");
    	 $("#TYButton").linkbutton("disable");
    	 $("#attr1").linkbutton("disable");
    	 $("#attr2").linkbutton("disable");
    	 $("#attr3").linkbutton("disable");
      }
      function initButton2(){
      	 $("#addButton").linkbutton("enable");
      	 $("#removeButton").linkbutton("enable");
    	 $("#okButton").linkbutton("disable");
     	 $("#QYButton").linkbutton("disable");
    	 $("#TYButton").linkbutton("disable");
    	 $("#attr1").linkbutton("disable");
    	 $("#attr2").linkbutton("disable");
    	 $("#attr3").linkbutton("disable");
      }
      function XZButton(status){
      	 $("#removeButton").linkbutton("enable");
    	 $("#okButton").linkbutton("enable");
    	 $("#attr1").linkbutton("enable");
    	  	 $("#attr2").linkbutton("disable");
    	 	 $("#attr3").linkbutton("disable");
    	 if(status == 0 || status == -1){
    	 	$("#okButton").linkbutton("enable");
 	    	 $("#QYButton").linkbutton("enable");
			$("#TYButton").linkbutton("disable");
    	 }else{
    	 	$("#okButton").linkbutton("disable");
	    	 $("#TYButton").linkbutton("enable");
	    	  $("#QYButton").linkbutton("disable");
    	 }
      }
      function initProButton(){
      	$("#attr2").linkbutton("disable");
    	 	 $("#attr3").linkbutton("disable");
      }
      function checkInputAdd(){
      		var taskID = $("#taskID").val();
      		var productID = $("#productID").val();
	    	$("#fmtwo").form("submit", {  
	            url: "/PlanManagement/designfaManager/addPro.action?taskID="+taskID+"&productID="+productID,  
	            onsubmit: function () {  
	                return $(this).form("validate");  
	            },  
	            success: function (result) {  
	                if (result == "true") {  
	                    $.messager.alert("提示信息", "操作成功");
						$('#enditTab').dialog('close');
						$("#products").tree("reload");
   						$("#products").tree('expandAll');
   						initProButton();
   						initpar();
	                }else if(result == "exist"){
	                	 $.messager.alert("提示信息", "该方案已有产品结构"); 
	                }
	                else {  
	                    $.messager.alert("提示信息", "保存数据失败");  
	                }  
	            }  
	        });  
    	}
      
      
      function addPlan(){ 
		addButton();
		removeAttr();
      	$("#fm").form("clear"); 
		$('#model_list').datagrid('clearSelections');
    	$("#products").tree("loadData",[]);
    		}
      function bcPlan(){
      		
      		var modelID = $('#model').combobox("getValue");
    	   $("#fm").form("submit", {  
            url: '/PlanManagement/designfaManager/updatePlan.action?modelID='+modelID,  
            onsubmit: function () {  
                return $(this).form("validate");
            },  
            success: function (result) {  
                if (result == "add") {  
                    $.messager.alert("提示信息", "添加方案成功");  
                    $('#model_list').datagrid('clearSelections');
                    $("#model_list").datagrid("load"); 
                    $("#fm").form("clear"); 
                    initButton2();
                }else if(result == "update"){
               		 $('#model_list').datagrid('reload');
                	$.messager.alert("提示信息", "操作成功");
                }else if(result == "exist"){
					$.messager.alert("提示信息", "此型号下已存在该方案");                
                }  
                else {  
                    $.messager.alert("提示信息", "保存方案失败");  
                }  
            }  
        });
    	}
    	function removePlan(){
    		 //返回选中多行  
        var selRow = $('#model_list').datagrid('getSelections');  
        //判断是否选中行  
        if (selRow.length==0) {  
            $.messager.alert("提示", "请选择要删除的行！", "info");  
            return;  
        }else{      
            var taskID="";  
            //批量获取选中行的ID  
            for (i = 0; i < selRow.length;i++) {  
                if (taskID =="") {  
                    taskID = selRow[i].taskID;  
                } else {  
                    taskID = selRow[i].taskID + "," + taskID;  
                }                 
            }  
            $.messager.confirm('提示', '是否删除选中数据?', function (r) {  
                if (!r) {  
                    return;  
                }  
                //提交  
                $.ajax({  
                    type: "POST",  
                    async: false,  
                    url: "/PlanManagement/designfaManager/removePlan.action?taskIDs=" + taskID,  
                    data: 'json',  
                    success: function (result) {  
                        if (result == "true") {  
                            $('#model_list').datagrid('clearSelections');  
                            $.messager.alert("提示", "恭喜您，信息删除成功！", "info");  
                            $('#model_list').datagrid('reload');  
                            $("#fm").form("clear");
                  			initButton2();
                  			treeReload('0');
                        } else {  
                            $.messager.alert("提示", "删除失败，请重新操作！", "info");  
                            return;  
                        }  
                    }  
                });  
            });  
    	}
    	}
    	
      function qyPlan(){
    		var taskID = $("#taskID").val();
    		if(taskID==""){
    			return;
    		}
			$.ajax({  
                    type: "POST",  
                    async: false,  
                    url: "/PlanManagement/designfaManager/statusOn.action?taskID=" + taskID,  
                    data: 'json',  
                    success: function (result) {  
                        if (result == "true") {  
                            $.messager.alert("提示", "恭喜您，型号启动成功！", "info");  
                            $('#model_list').datagrid('reload');  
							$("#statusName").val('已启动');
							XZButton(1);
                        } else {  
                            $.messager.alert("提示", "启动失败，请重新操作！", "info");  
                            return;  
                        }  
                    }  
                });  
        }
    	function tyPlan(){
    		var taskID = $("#taskID").val();
    		if(taskID==""){
    			return;
    		}
			$.ajax({  
                    type: "POST",  
                    async: false,  
                    url: "/PlanManagement/designfaManager/statusOff.action?taskID=" + taskID,  
                    data: 'json',  
                    success: function (result) {  
                        if (result == "true") {  
                            $.messager.alert("提示", "恭喜您，型号停用成功！", "info");  
                            initRight();
                            $('#model_list').datagrid('reload');  
							$("#statusName").val('已停用');
							XZButton(-1);
                        } else {  
                            $.messager.alert("提示", "停用失败，请重新操作！", "info");  
                            return;  
                        }  
                    }  
                });  
        }
          /* 添加产品结构信息 */  
   	 function addPro(){  
   	 	$("#fmtwo").form("clear");
   	 	$("#parentName").val(nodeName);
   	 	$("#parentProductID").val(nodeID);
        $("#enditTab").dialog("open").dialog('setTitle', '添加产品结构');    
        $("#enditTab").panel("move",{top:$(document).scrollTop() + ($(window).height()-250) * 0.5}); 
    	}
    	/* 修改产品结构信息 */
     function updatePro(){
     	$("#fmtwo").form("clear");
     	$("#parentName").val(parentNodeName);
     	$("#productName").val(nodeName);
     	$("#productID").val(nodeID);
    	 $("#enditTab").dialog("open").dialog('setTitle', '修改产品结构');    
        $("#enditTab").panel("move",{top:$(document).scrollTop() + ($(window).height()-250) * 0.5}); 
     }
     function treeReload(productID){
    //$("#products").tree("options").url='/PlanManagement/designfaManager/proTree.action?productID='+productID;
    $("#products").tree("reload");
    $("#products").tree('expandAll');
}
     
     	/* 删除产品结构信息 */
     function removePro(){
     	$.messager.confirm('提示', '删除节点并删除子节点,是否删除?', function (r) {  
                if (!r) {  
                    return;  
                }  
                //提交  
                $.ajax({  
                    type: "POST",  
                    async: false,  
                    url: "/PlanManagement/designfaManager/removePro.action?productID="+nodeID,  
                    data: 'json',  
                    success: function (result) {  
                        if (result == "true") {  
                            $.messager.alert("提示", "恭喜您，信息删除成功！", "info");
                             parentNodeName = '';
							 parentNodeID = '';
							 nodeName = '';
							 nodeID = ''; 
							 initpar();
							  $("#products").tree("reload");
    							$("#products").tree('expandAll');
    							initProButton();
                        } else {  
                            $.messager.alert("提示", "删除失败，请重新操作！", "info");  
                            return;  
                        }  
                    }  
                });  
            });  
     }
    	/*选择负责人*/
    	 function checkCharge(){  
    	     $("#user_tab").datagrid({  
            checkOnSelect: false,  
            fitColumns :true, 
            checkOnSelect:true, 
            singleSelect: true,
            selectOnCheck: true,
            nowrap:true, 
            url: '/PlanManagement/userManager/search.action',  
            loadMsg:'加载中...',  
            fit: true,
            rowStyler:function(index,row){
			if (index%2==1){
				return 'background-color:#D2D7D3;';
			}
			 else{
			return 'background-color:#50a3a2;';
			} 
		},  
            columns: [[  
                { field: 'userID', checkbox: true},  
                { field: 'userCode', title: '用户帐号', width:100},  
                { field: 'userName', title: '用户姓名', width:100},  
                { field: 'sex', title: '性别', width:100},  
                { field: 'tel', title: '电话', width:160},  
                { field: 'email', title: '电子邮件', width:160},  
                { field: 'jobName', title: '职务', width:100},  
                { field: 'roleID', title: '系统角色', width:100,
                formatter:function(value,row,index){if(value=='1'){return'系统管理员';}if(value=='2'){return'管理者';}if(value=='3'){return'普通用户';}}},  
            ]]            
        });
        $("#userTab").dialog("open").dialog('setTitle', '为当前群组选择负责人');    
        $("#userTab").panel("move",{top:$(document).scrollTop() + ($(window).height()-250) * 0.5}); 
      	//url = "/PlanManagement/userManager/add.action";
      	
      	
    	}  
    	
    	 function checkOk(){
          var row = $("#user_tab").datagrid("getSelected");  
         $('#leaderName').val(row.userName);
         $('#director').val(row.userID);
         $('#userTab').dialog('close');
         }  
        //格式化日期
        $.fn.datebox.defaults.formatter = function(date){
		var y = date.getFullYear();
		var m = date.getMonth()+1;
		var d = date.getDate();
		return y+'-'+m+'-'+d;
		}
     </script> 
      <body style="min-width:1280px;min-height:1024px;">
		<div class="wrap">
			<div class="left-right-middle1">
			<jsp:include page="/WEB-INF/views/main/head.jsp" flush="true" ></jsp:include>
			<div class="easyui-layout"  style="min-width:1280px;min-height:908px;">
			<div data-options="region:'north',title:'设计方案管理',split:false" collapsible="flase" style="height:90px;background-color: #F1F3F7">
        <table style="width: auto; height: 60px;" cellspacing="0" border="0">  
                <tr>  
                    <td> &nbsp 型号</td>  
                    <td><input id="model" name="model"  style="width: 180px;height: 26px;"></td> 
                    <td>
                    &nbsp<a id="addButton" href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" onclick="addPlan();">增加</a> 
                    &nbsp<a id="removeButton" href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" onclick="removePlan();">删除</a> 
                    &nbsp<a id="okButton" href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-save" onclick="bcPlan();">保存</a>
                    &nbsp<a id="QYButton" href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-ok" onclick="qyPlan();">启用</a>
                    &nbsp<a id="TYButton" href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-no" onclick="tyPlan();">停用</a>
                    </td>             
                </tr>  
          </table>  
        </div> 
		<div region="west" split="false" title="方案列表" style="width:360px;"  >
			 <table id="model_list" border="0"></table>
		</div>
		<div id="content" region="center" title="方案详细"  split="false"  style="padding:5px; height:auto;background-color: #F1F3F7;">
		 <form id="fm" method="post" >  
            <table border="0" style="width:800px;" >  
                <input type="hidden"  id="moveid" name="moveid"  value=""  style="width: 180px;height: 20px;">  
                <tr style="height: 35px; ">  
                    <td width="88px" style="padding-left: 28px">方案编号</td>  
                    <td><input id="taskID" name="taskID" readonly="readonly" style="width: 180px;height: 20px;"></td>  
                    <td width="88px">方案名称<span style="color:#ff0000">*</span></td>  
                    <td><input  id="taskName" name="taskName" class="easyui-validatebox" data-options="required:true" missingMessage="方案名称不能为空" value=""  style="width: 180px;height: 20px;"></td>  
                </tr>  
               <tr style="height: 35px;">  
                    <td width="88px" style="padding-left: 28px">方案主管<span style="color:#ff0000">*</span></td>  
                    <td><input id="leaderName" readonly="readonly" class="easyui-validatebox" data-options="required:true" missingMessage="型号主管不能为空" style="width: 180px;height: 20px;">
                    	<input id="director" name="director" type="hidden">
                    <a href="javascript:void(0)" class="easyui-linkbutton" plain="true" id="zg" iconCls="icon-search" onclick="checkCharge();" style="height: 15px;"  ></a>
                    </td>  
                    <td>方案状态</td>  
                    <td><input  id="statusName" readonly="readonly"  value=""  style="width: 180px;height: 20px;">
                    	<input  id="status" name="status" type="hidden">
                   </td>  
                </tr>
                <tr style="height: 35px;">  
                    <td width="88px" style="padding-left: 28px" >方案描述</td>  
                    <td colspan="4"> <textarea id="remark" name="remark" value="" style="resize:none; width:593px; height:100px;background-color: #F1F3F7"></textarea></td>  
                </tr>
                <tr style="height: 35px;">  
                    <td width="88px" style="padding-left: 28px">开始时间<span style="color:#ff0000">*</span></td>  
                    <td><input id="beginDT" name="beginDT" type="text"  class="easyui-datebox" missingMessage="开始时间不能为空" style="width: 187px;height: 28px;" required="required" value="" ></input></td>  
                    <td>结束时间<span style="color:#ff0000">*</span></td>  
                    <td><input id="endDT" name="endDT" type="text"  class="easyui-datebox" missingMessage="结束时间不能为空" style="width: 187px;height: 28px;" required="required" value="" ></input></td>  
                </tr>
                <tr style="height: 280px;">  
                    <td width="88px" style="padding-left: 28px">产品结构</td>
                    <td colspan="4"> 
                    <ul style="border:1px solid #ccc;height: 380px ; overflow:auto;" id="products"></ul> 
					</td>  
               		 </tr>
                <tr style="height: 50px;">
                	<td></td> 
                    <td colspan="4" align="left">  
               &nbsp<a id="attr1" href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" onclick="addPro();">添加</a> &nbsp
                    <a id="attr2" href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" onclick="removePro();">删除</a>  &nbsp
                    <a id="attr3" href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-edit" onclick="updatePro();">编辑</a>  
                    </td>  
                </tr>  
            </table>   
        </form>
		</div>
		     <div id="enditTab" class="easyui-dialog" style="width: 380px;border:1px solid #D2D7D3; height: auto; padding: 10px 20px;" closed="true" buttons="#dlg-buttons" shadow="false" >    
        <form id="fmtwo" method="post" >  
            <table border="0" style="width:98%;">  
                <tr style="height: 35px;">  
                    <td>父节点<span style="color:#ff0000">*</span></td>  
                    <td><input  id="parentName" readonly="readonly" value=""  style="width: 180px;height: 20px;">
                    	<input type="hidden" id="parentProductID" name="parentProductID" value="" >
                    </td>  
                </tr>  
               <tr style="height: 35px;">  
                    <td>当前节点<span style="color:#ff0000">*</span></td>  
                    <td><input id="productName" name="productName" class="easyui-validatebox" data-options="required:true" missingMessage="节点名不能为空" style="width: 180px;height: 20px;">
                    	<input id="productID" name="productID" type="hidden">
                    </td>  
                </tr>
                <tr style="height: 50px;">  
                    <td colspan="4" align="center">  
                        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-ok" onclick="checkInputAdd();">确认</a>      
                        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#enditTab').dialog('close')">取消</a>  
                    </td>  
                </tr>  
            </table>   
        </form>    
    </div>
    <div id="userTab" class="easyui-dialog" style="width: 780px;border:1px solid #D2D7D3; height:400px; padding: 10px 20px; overflow:hidden;"  closed="true" buttons="#dlg-buttons" shadow="false"  >  
       <div id="tb" style="width: auto; height: 48px;">  
            <table style="width: auto; height: 48px;" cellspacing="0" border="0">  
                <tr>  
                    <td>&nbsp <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-ok" onclick="checkOk();">确认</a>  &nbsp
                      <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#userTab').dialog('close')">取消</a> 
                 	</td>            
                </tr>  
            </table>  
        </div>    
        	<table id="user_tab" ></table>  
     </div>
			</div>
		    </div>
	    </div>
	   </body>
    
</html>
