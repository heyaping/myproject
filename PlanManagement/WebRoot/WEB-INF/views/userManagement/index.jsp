<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"  %>
<%@page import="java.text.SimpleDateFormat"%> 
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<html>
  <head>
    <base href="<%=basePath%>">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>用户管理</title>
    <link rel="stylesheet" type="text/css" href="STATIC/css/default.css">
	<link rel="stylesheet" type="text/css" href="STATIC/js/jquery-easyui-1.3.5/themes/gray/easyui.css">
	<link rel="stylesheet" type="text/css" href="STATIC/js/jquery-easyui-1.3.5/themes/icon.css" />
	<link href="STATIC/css/main.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="STATIC/css/mainstyles.css" type="text/css"></link>
	<script type="text/javascript" src="STATIC/js/jquery.min.js"></script>
	<script type="text/javascript" src="STATIC/js/work.js"></script>
	<script type="text/javascript" src="STATIC/js/jquery-easyui-1.3.5/jquery-1.10.2.min.js"></script>
	<script type="text/javascript" src="STATIC/js/jquery-easyui-1.3.5/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="STATIC/js/JQuery-zTree-v3.5.15/jquery.ztree.all-3.5.min.js"></script>
	<script type="text/javascript" src="STATIC/js/index.js"></script>
	<script type="text/javascript" src="STATIC/js/extends.js"></script>
	<script type="text/javascript" src="STATIC/js/common.js"></script>
	
<style type="text/css">
</style> 
  </head>
  <script type="text/javascript">  
  	var url = "";
    /* 启动时加载 */  
    $(function(){  
   
     $("#user_tab").datagrid({  
            title: '用户信息管理',  
             checkOnSelect: false,  
            toolbar: '#tb',
            fitColumns :true, 
            checkOnSelect:true,
            singleSelect: false,
            selectOnCheck: true,
            nowrap:true, 
            rowStyler:function(index,row){
			if (index%2==1){
				return 'background-color:#D2D7D3;font-weight:bold';
			}
			 else{
			return 'background-color:#50a3a2;font-weight:bold';
			} 
		},  
            url: '/PlanManagement/userManager/search.action',  
            loadMsg:'加载中...',  
            fit: true,  
            columns: [[  
                { field: 'userID', checkbox: true},  
                { field: 'userCode', title: '用户帐号', width:100},  
                { field: 'userName', title: '用户姓名', width:100},  
                { field: 'sex', title: '性别', width:100},  
                { field: 'tel', title: '电话', width:160},  
                { field: 'email', title: '电子邮件', width:160},  
                { field: 'jobName', title: '职务', width:100},  
                { field: 'roleName', title: '系统角色', width:100},
                { field: 'createDT', title: '创建时间', width:150},  
                { field: 'modifyDT', title: '修改时间', width:150 },  
            ]]            
        });
         
      }) ;
        /* 添加用户信息 */  
   	 function addUser(){  
       	selectUser();
        $("#enditTab").dialog("open").dialog('setTitle', '添加用户信息');    
        $("#enditTab").panel("move",{top:$(document).scrollTop() + ($(window).height()-250) * 0.5}); 
      	$("#fm").form("clear"); 
      	url = "/PlanManagement/userManager/add.action";
    	}   
    	/* 下拉框元素填充 */  
   	 function selectUser(){  
        $('#select_user_sex').combobox({      
            url:'/PlanManagement/userManager/query.action?type=sex',   
            valueField:'value',      
            textField:'name',
            panelHeight:'auto',
            
        });  
        /*下拉框只可选择不可输入*/
        $("#select_user_sex").combobox({editable:false});
          $('#select_user_post').combobox({      
            url:'/PlanManagement/userManager/query.action?type=job',      
            valueField:'jobID', 
            panelHeight:'auto',     
            textField:'jobName' 
                
        }); 
        /*主要群组列表*/
          $("#select_user_groups").combobox({editable:false});
          $('#select_user_groups').combobox({      
            url:'/PlanManagement//deptManager/searchMajorGroups.action?userID=5',      
            valueField:'deptID', 
            panelHeight:'auto',     
            textField:'deptLongName' 
                
        }); 
         $("#select_user_post").combobox({editable:false});
         $('#select_user_role').combobox({      
          	url:'/PlanManagement/userManager/query.action?type=role',      
            valueField:'roleID', 
            panelHeight:'auto',     
            textField:'roleName'     
        }); 
        $("#select_user_role").combobox({editable:false});
        }
         /* 数据校验后提交 */  
    	function checkInputAdd(){  
        var userCode = $('#userCode').val();   
        if(userCode==''){  
            $.messager.alert('提示','用户帐号不能为空');  
            return false;  
        }  
        var userName = $('#userName').val();   
        if(userName==''){  
            $.messager.alert('提示','用户姓名不能为空');  
            return false;  
        }
        var select_user_sex = $('#select_user_sex').combobox('getValue'); 
        if(select_user_sex==''||select_user_sex==0){  
            $.messager.alert('提示','性别不能为空');  
            return false;  
        }  
        var password = $('#password').val();   
        if(password==''){  
            $.messager.alert('提示','密码不能为空');  
            return false;  
        }
        var repassword = $('#repassword').val();   
        if(repassword==''){  
            $.messager.alert('提示','确认密码不能为空');  
            return false;  
        }
        if(password!=repassword){  
            $.messager.alert('提示','密码与确认密码不一致');  
            return false;  
        }
         var jobName = $('#select_user_post').combobox('getValue');   
        if(jobName==''||jobName==0){  
            $.messager.alert('提示','职务情况不能为空');  
            return false;  
        }  
         var roleID = $('#select_user_role').combobox('getValue');   
        if(roleID==''||roleID==0){  
            $.messager.alert('提示','分配角色不能为空');  
            return false;  
        }  
        $("#fm").form("submit", {  
            url: url,  
            onsubmit: function () {  
                return $(this).form("validate");  
            },  
            success: function (result) {  
                if (result == "true") {  
                    $.messager.alert("提示信息", "操作成功");  
                    $("#enditTab").dialog("close");  
                    $("#user_tab").datagrid("load");  
                }  
                else {  
                    $.messager.alert("提示信息", "保存数据失败");  
                }  
            }  
        });  
    }  
        /* 修改用户信息 */  
    function editUser(){  
        var checkedItems = $('#user_tab').datagrid('getSelections');  
        var userIds = [];  
        var num = 0;  
        $.each(checkedItems, function(index, item){  
            userIds.push(item.userID);  
            num++;  
        });   
        if(num != 1){  
            $.messager.alert('提示','请选择一条数据进行修改');  
            return false;  
        }  
        var row = $("#user_tab").datagrid("getSelected");  
           selectUser();
        if (row) {  
            $("#enditTab").dialog("open").dialog('setTitle', '修改用户信息');  
            $("#enditTab").panel("move",{top:$(document).scrollTop() + ($(window).height()-250) * 0.5}); 
            $("#fm").form("load", row);  
            $("#repassword").val(row.password);
           url = "/PlanManagement/userManager/update.action";  
        }  
        $('#select_user_sex').combobox("select", row.sex);  
        $('#select_user_post').combobox("select", row.jobID);  
        //修改商品时把编码和批号传入隐藏文本框（禁用后获取不到值）  
        $('#select_user_role').combobox("select", row.roleID);
    }
      function removeUser() {  
        //返回选中多行  
        var selRow = $('#user_tab').datagrid('getSelections');  
        //判断是否选中行  
        if (selRow.length==0) {  
            $.messager.alert("提示", "请选择要删除的行！", "info");  
            return;  
        }else{      
            var userID="";  
            //批量获取选中行的ID  
            for (i = 0; i < selRow.length;i++) {  
                if (userID =="") {  
                    userID = selRow[i].userID;  
                } else {  
                    userID = selRow[i].userID + "," + userID;  
                }                 
            }  
            $.messager.confirm('提示', '是否删除选中数据?', function (r) {  
                if (!r) {  
                    return;  
                }  
                //提交  
                $.ajax({  
                    type: "POST",  
                    async: false,  
                    url: "/PlanManagement/userManager/remove.action?userIDs=" + userID,  
                    data: 'json',  
                    success: function (result) {  
                        if (result == "true") {  
                            $('#user_tab').datagrid('clearSelections');  
                            $.messager.alert("提示", "恭喜您，信息删除成功！", "info");  
                            $('#user_tab').datagrid('reload');  
                        } else {  
                            $.messager.alert("提示", "删除失败，请重新操作！", "info");  
                            return;  
                        }  
                    }  
                });  
            });  
        }  
    }; 
     </script> 
		<body style="min-width:1280px;min-height:1024px;">
		<div class="wrap">
		<div class="left-right-middle1">
		<jsp:include page="/WEB-INF/views/main/head.jsp" flush="true" ></jsp:include>
		<div class="align-center" style="padding: 0;margin:0;">
			<div id="tb" style=" height: 48px;">  
		            <table style="height:48px;" cellspacing="0" border="0">  
		                <tr>  
		                    <td>&nbsp<a href="javascript:void(0)" class="easyui-linkbutton" id="add" iconCls="icon-add" onclick="addUser();">添加</a> &nbsp
		                    <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" onclick="removeUser();">删除</a>  &nbsp
		                    <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-edit" onclick="editUser();">编辑</a></td>            
		                </tr>  
		            </table>  
		 	</div>  
		</div>
		<table id="user_tab" ></table>
		<div id="enditTab" class="easyui-dialog" style="width: 680px; height: auto; padding: 10px 20px;border:1px solid #D2D7D3;" closed="true" buttons="#dlg-buttons" shadow="false" >    
        <form id="fm" method="post" >  
            <table border="0" style="width:98%;">  
                <input type="hidden"  id="userid" name="userid"  value=""  style="width: 180px; "/>  
                <tr style="height: 35px;">  
                    <td>用户编号</td>  
                    <td><input id="userid" name="userID" readonly="readonly" style="width: 180px;height: 20px;"/></td>  
                    <td>用户帐号<span style="color:#ff0000">*</span></td>  
                    <td><input  id="userCode" name="userCode"  value=""  style="width: 180px;height: 20px;"/></td>  
                </tr>  
               <tr style="height: 35px;">  
                    <td>用户姓名<span style="color:#ff0000">*</span></td>  
                    <td><input id="userName" name="userName" style="width: 180px;height: 20px;"></td>  
                    <td>性别</td>  
                    <td><input  id="select_user_sex"  name="sex"  value=""  style="width: 185px;height: 30px;">
                   </td>  
                </tr>
                <tr style="height: 35px;">  
                    <td>密码<span style="color:#ff0000">*</span></td>  
                    <td><input id="password" name="password" style="width: 180px;height: 20px;"></td>  
                    <td>确认密码<span style="color:#ff0000">*</span></td>  
                    <td><input  id="repassword" name="repassword"  value=""  style="width: 180px;height: 20px;"></td>  
                </tr>
                <tr style="height: 35px;">  
                    <td>联系电话</td>  
                    <td><input id="tel" name="tel" style="width: 180px;height: 20px;"></td>  
                    <td>电子邮件</td>  
                    <td><input  id="tb_movement_code" name="email"  value=""  style="width: 180px;height: 20px;"></td>  
                </tr>
                <tr style="height: 35px;">  
                    <td>职务情况</td>  
                 	 <td><input  id=select_user_post name="jobID"  value=""  style="width: 185px;height: 30px;"></td>   
                    <td>分配角色</td>  
                    <td><input  id="select_user_role" name="roleID"  value=""  style="width: 185px;height: 30px;"></td>  
                </tr>
                <tr style="height: 35px;">  
                    <td>主要群组</td>  
                 	 <td><input  id=select_user_groups name="majorGroups"  value=""  style="width: 185px;height: 30px;"></td>   
                    <td>其他群组</td>  
                    <td><input  id="select_user_role" name="roleID"  value=""  style="width: 185px;height: 30px;"></td>  
                </tr>
                <tr style="height: 50px;">  
                    <td colspan="4" align="center">  
                        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-ok" onclick="checkInputAdd();">确认</a>      
                        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#enditTab').dialog('close')">取消</a>  
                    </td>  
                </tr>  
            </table>   
        </form>    
    </div>  
		</div>
		</div>
		
		</body> 
</html>
