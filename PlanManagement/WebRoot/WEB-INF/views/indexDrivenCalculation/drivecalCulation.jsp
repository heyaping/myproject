<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"  %>
<%@page import="java.text.SimpleDateFormat"%> 
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"> <!-- HTML5 document type -->
<html>
  <head>
    <base href="<%=basePath%>">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>驱动计算</title>
<link rel="stylesheet" type="text/css" href="STATIC/css/default.css">
	<link rel="stylesheet" type="text/css" href="STATIC/js/jquery-easyui-1.3.5/themes/gray/easyui.css">
	<link rel="stylesheet" type="text/css" href="STATIC/js/jquery-easyui-1.3.5/themes/icon.css" />
	<link href="STATIC/css/main.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="STATIC/css/mainstyles.css" type="text/css"></link>
	<script type="text/javascript" src="STATIC/js/jquery.min.js"></script>
	<script type="text/javascript" src="STATIC/js/work.js"></script>
	<script type="text/javascript" src="STATIC/js/jquery-easyui-1.3.5/jquery-1.10.2.min.js"></script>
	<script type="text/javascript" src="STATIC/js/jquery-easyui-1.3.5/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="STATIC/js/JQuery-zTree-v3.5.15/jquery.ztree.all-3.5.min.js"></script>
	<script type="text/javascript" src="STATIC/js/index.js"></script>
	<script type="text/javascript" src="STATIC/js/extends.js"></script>
	<script type="text/javascript" src="STATIC/js/common.js"></script>
	
  </head>
   <style type="text/css">
   .style1{

	width:49%;
	float:left;
	height:90%;
	
	margin: 0px auto;

	margin-bottom:20px;
	
	border:1px solid #9BDF70;
	
	background-color: #F0FBEB

	}
   .style2{

	width:49%;
	float:right;
	height:90%;
	
	margin: 0px auto;

	margin-bottom:20px;
	
	border:1px solid #FFDD99;
	
	background-color: #FFF9ED

	}
	</style>
  <script type="text/javascript">  
  	var url = "";
  	//全称
  	var fullName = "";
    /* 启动时加载 */  
    $(function(){  
    	  $("#targettext").tooltip({
			    position: 'top',
			    content: '<span style="color:#fff"></span>',
			    onShow: function(){
					$(this).tooltip('tip').css({
						backgroundColor: 'white',
						borderColor: 'green'
					});
			    }
			}); 
    	  $("#modeltext").tooltip({
			    position: 'top',
			    content: '<span style="color:#fff"></span>',
			    onShow: function(){
					$(this).tooltip('tip').css({
						backgroundColor: 'white',
						borderColor: '#666'
					});
			    }
			}); 
         /*加载型号-方案*/
          $("#slelct_Model").combobox({editable:false});
          $('#slelct_Model').combobox({      
            url:'/PlanManagement/targetBoard/modelInfo.action',      
            valueField:'modelID', 
            panelHeight:'auto',     
            textField:'modelName',
            onChange:function(modelID){
		    	//$('#city').combobox('clear');
		    	$('#slelct_Project').combobox({
			    valueField:'taskID', //值字段
			    textField:'taskName', //显示的字段
			    url:'/PlanManagement/TargetbomManager/getDesignfa.action?modelID='+modelID,
			    panelHeight:'auto',
		 		});
			 }
            });
          $("#slelct_Project").combobox({editable:false});
          $('#slelct_Project').combobox({      
           	url:'/PlanManagement/targetBoard/projectInfo.action',      
            valueField:'taskID', 
            panelHeight:'auto',     
            textField:'taskName',
            onChange:function(taskID){
            	$("#targettext").val(""); 
	            $("#modeltext").val("");
	            $("#map_tab").datagrid('loadData',{total:0,rows:[]});
				initTree(taskID);
			 }
            });
            
          $("#modelinfo").treegrid({
			url:'',
			idField:'id',
			treeField:'name',
			fitColumns : true,
			width:function(){return document.body.clientWidth*0.9},
			columns:[[
				{title:'名称',field:'name',width:180},
				{title:'指标类型',field:'targetType',width:60,align:'right'},
				{title:'数据类型',field:'dataType',width:60,align:'right'},
				{title:'当前值',field:'targeFactVal',width:240,align:'right'},
					]],
		});
		
		  $("#targetinfo").treegrid({
				url:'',
			    idField:'id',
			    treeField:'name',
			    fitColumns : true,
			    width:function(){return document.body.clientWidth*0.9},
			    columns:[[
					{title:'名称',field:'name',width:180},
					{title:'指标类型',field:'targetType',width:60,align:'right'},
					{title:'数据类型',field:'dataType',width:60,align:'right'},
					{title:'指标上限',field:'targeUpperLimit',width:60,align:'right'},
					{title:'指标下限',field:'targeLowLimit',width:60,align:'right'},
					{title:'当前值',field:'targeFactVal',width:60,align:'right'},
			    ]]
			});
            /* 启动时加载 */  
    /*  $("#target_tab").datagrid({  
            fitColumns :true, 
            checkOnSelect:true,
            singleSelect: false,
            selectOnCheck: true,
            striped:true,
            nowrap:true, 
            url: '/PlanManagement/driveCount/searchTarget.action', 
            queryParams:{targetID:-1}, 
            loadMsg:'加载中...',  
            fit: true,  
            columns: [[  
                { field: 'dataitem', title: '数据项', width:100},  
                { field: 'value', title: '值', width:100},  
            ]]            
        });
     $("#model_tab").datagrid({  
            checkOnSelect: false,  
            fitColumns :true, 
            checkOnSelect:true,
            singleSelect: false,
            selectOnCheck: true,
            striped:true,
            nowrap:true, 
            url: '/PlanManagement/driveCount/searchModel.action',  
            queryParams:{targetName:-1},
            loadMsg:'加载中...',  
            fit: true,  
            columns: [[  
                { field: 'dataitem', title: '数据项', width:100},  
                { field: 'value', title: '值', width:100},  
            ]]            
        }); */
        
          $("#map_tab").datagrid({  
            checkOnSelect: false,  
            fitColumns :true, 
            checkOnSelect:true,
            singleSelect: true,
            selectOnCheck: true,
            striped:true,
            nowrap:true,
            fit: true, 
            url: '',  
            loadMsg:'加载中...',  
            rowStyler:function(index,row){
			if (index%2==1){
				return 'background-color:#D2D7D3;';
			}
			 else{
			return 'background-color:#50a3a2;';
			} 
		},  
            columns: [[  
                { field: 'targetName', title: '指标', width:100},  
                { field: 'modelName', title: '模型参数', width:100},  
                { field: 'targetID', hidden:true},  
                { field: 'targeUpperLimit', hidden:true},  
                { field: 'targeLowLimit', hidden:true},  
                { field: 'targeFactVal', hidden:true},  
                { field: 'state', hidden:true},  
                { field: 'type', hidden:true},  
            ]]            
        });
      }) ;
        function initTree(taskID){
			$("#targetinfo").treegrid({
				url:'/PlanManagement/driveCount/returnTargetTree.action?taskID='+taskID,
			    idField:'id',
			    treeField:'name',
			    fitColumns : true,
			    loadMsg:'正在处理，请稍后···',
			    rowStyler:function(index,row){
			if (index%2==1){
				return 'background-color:#D2D7D3;';
			}
			 else{
			return 'background-color:#D2D7D3;';
			}},
			    width:function(){return document.body.clientWidth*0.9},
			    columns:[[
					{title:'名称',field:'name',width:180},
					{title:'指标类型',field:'targetType',width:60,align:'right'},
					{title:'数据类型',field:'dataType',width:60,align:'right'},
					{title:'指标上限',field:'targeUpperLimit',width:60,align:'right'},
					{title:'指标下限',field:'targeLowLimit',width:60,align:'right'},
					{title:'当前值',field:'targeFactVal',width:60,align:'right'},
			    ]],
			    onClickRow:function(row){
			    	if(row.id!=$("#targetinfo").treegrid("getRoot").id){
				    	var name = $("#targetinfo").treegrid("getParent",row.id).name;
				    	fullName = row.name;
				    	insert_flg(row.id,1);
				    	$("#targettext").val(fullName);
			    	}else{
			    		$("#targettext").val(row.name);
			    	}
				}
			});
	      }
	      //树结构全称递归
  		function insert_flg(treeid,index){
				if(index==1){
					if(treeid != $("#targetinfo").treegrid("getRoot").id){
						var name = $("#targetinfo").treegrid("getParent",treeid).name;
						var id = $("#targetinfo").treegrid("getParent",treeid).id;
						fullName = name+'.'+fullName;
						insert_flg(id,1);
					}
				}else if(index==2){
					if(treeid != $("#modelinfo").treegrid("getRoot").id){
						var name = $("#modelinfo").treegrid("getParent",treeid).name;
						var id = $("#modelinfo").treegrid("getParent",treeid).id;
						fullName = name+'.'+fullName;
						insert_flg(id,2);
					}
				}			 	
			}
  		function filechange(){
	        	var path=document.getElementById("file").value;
	        	if(path.length!=0){    
	            	var pathHouZhui=path.split(".");
	            	if(pathHouZhui[pathHouZhui.length-1]=="pxc"){
	            		$("#targettext").val(""); 
	            		$("#modeltext").val("");
						$("#modelinfo").treegrid('loadData',[]);
	            		$("#map_tab").datagrid('loadData',{total:0,rows:[]});
	                	$("#fileformname").form("submit", {  
				            url: "/PlanManagement/driveCount/upload.action",  
				            success: function (result) {  
				                if (result != "false") {  
									$("#modelinfo").treegrid({
				                    		url:'/PlanManagement/driveCount/modelTree.action',
										    idField:'id',
										    treeField:'name',
										    fitColumns : true,
										    loadMsg:'正在处理，请稍后···',
										        rowStyler:function(index,row){
												if (index%2==1){
													return 'background-color:#50a3a2;';
												}
												 else{
												return 'background-color:#D2D7D3;';
												}},
										    width:function(){return document.body.clientWidth*0.9},
										    columns:[[
												{title:'名称',field:'name',width:180},
												{title:'指标类型',field:'targetType',width:60,align:'right'},
												{title:'数据类型',field:'dataType',width:60,align:'right'},
												{title:'当前值',field:'targeFactVal',width:240,align:'right'},
										    ]],
										    onClickRow:function(row){
										    	if(row.id!=$("#modelinfo").treegrid("getRoot").id){
											    	var name = $("#modelinfo").treegrid("getParent",row.id).name;
											    	fullName = row.name;
											    	insert_flg(row.id,2);
											    	$("#modeltext").val(fullName);
										    	}else{
										    		$("#modeltext").val(row.name);
										    	}
											},
				                    	});
				                    }
				                else {  
				                    $.messager.alert("提示信息", "上传失败");  
				                }  
				            }  
				        });  
	             	}else{
	                	$.messager.alert("提示信息","只能选择pxc文件");
	             	}
	        	}else{
	           		$.messager.alert("提示信息","请选定一个文件");
	        	}
	    	}
	    function addmapping(){
	    	var q = 0;
	    	if($("#targettext").val()==""||$("#modeltext").val()==""){
	    		$.messager.alert("提示", "请选择映射指标与映射模型", "info");  
                           return;  
	    	}
	    	var rows=$("#map_tab").datagrid("getRows");
	    	var targetText = $("#targettext").val();
	    	var modelText = $("#modeltext").val();
	    	$.each(rows, function(index, item){ 
	    		if(item.targetName==targetText){
	    			q = 1;
	    			$.messager.alert("提示", "该指标已经建立映射", "info");
	    			return;
	    		}else if(item.modelName==modelText){
	    			q = 1;
	    			$.messager.alert("提示", "该模型参数已经建立映射", "info");
	    			return;
	    		}
     	 	  });   
     	 	if(q!=0){
     	 		return;
     	 	}
	    	var targetID = $("#targetinfo").treegrid("getSelected").id;
	    	var targeUpperLimit = $("#targetinfo").treegrid("getSelected").targeUpperLimit;
	    	var targeLowLimit = $("#targetinfo").treegrid("getSelected").targeLowLimit;
	    	var targeFactVal = $("#targetinfo").treegrid("getSelected").targeFactVal;
	    	var state = $("#modelinfo").treegrid("getSelected").targetType;
	    	var type = $("#modelinfo").treegrid("getSelected").dataType;
	    	
                           $.messager.alert("提示", "建立映射成功", "info");  
                           $("#map_tab").datagrid('insertRow',{
								index: 0, 
								row: {
									targetName:targetText,
									modelName:modelText,
									targetID:targetID,
					                targeUpperLimit:targeUpperLimit,
					                targeLowLimit:targeLowLimit,
					                targeFactVal:targeFactVal,
					                state:state,
					                type:type
								}
							});
	    }
	    function removemapping(){
	    	var row = $("#map_tab").datagrid("getSelected");
	    	if(row==null){
	    		 $.messager.alert("提示", "请选择一个映射", "info");
	    		 return;
	    	}else{
	    		$.messager.confirm('提示', '是否删除选中数据?', function (r) {  
                if (!r) {  
                    return;  
                	} 
	    		var index = $("#map_tab").datagrid("getRowIndex",row);
	    		$("#map_tab").datagrid("deleteRow", index); 
                }); 
	    	}
	    }
	    function checkOk(){
	    	var rows = $("#map_tab").datagrid("getRows");
	    	if(rows[0]==null){
	    		 $.messager.alert("提示", "请先建立映射", "info");
	    		 return;
	    	}else{
		    	var mcOrCl= $('input:radio[name="Fruit"]:checked').val();
		    	if(mcOrCl == null){
		    		$.messager.alert("提示", "请选择执行方式", "info");
		    		return;
		    	}else if(mcOrCl == "ModelCenter"){
		    			var targetIDs = "";
		    			var states = "";
		    			var types = "";
		    			var modelNames = "";
		    			var targeFactVals = "";
		    			var targeLowLimits = "";
		    			var targeUpperLimits = "";
				    	for(var i=0;i<rows.length;i++){
				    		if(i==0){
				    			targetIDs = rows[i].targetID;
					    		states = rows[i].state;
					    		types = rows[i].type;
					    		modelNames = rows[i].modelName;
					    		targeFactVals = rows[i].targeFactVal;
					    		targeLowLimits = rows[i].targeLowLimit;
					    		targeUpperLimits = rows[i].targeUpperLimit;
				    		}else{
				    		targetIDs = targetIDs+","+rows[i].targetID;
				    		states = states+","+rows[i].state;
				    		types = types+","+rows[i].type;
				    		modelNames = modelNames+","+rows[i].modelName;
				    		targeFactVals = targeFactVals+","+rows[i].targeFactVal;
				    		targeLowLimits = targeLowLimits+","+rows[i].targeLowLimit;
				    		targeUpperLimits = targeUpperLimits+","+rows[i].targeUpperLimit;
				    		}
				    	}
		    			$.ajax({  
		                    type: "POST",  
		                    url: "/PlanManagement/driveCount/addMapping.action?targetID="+targetIDs+"&state="+states+"&type="+types+"&fullName="+modelNames+"&currentValue="+targeFactVals+"&lowerBound="+targeLowLimits+"&upperBound="+targeUpperLimits+"&optVarType=",  
		                    data: 'json',  
		                    beforeSend: function(){
							var win = $.messager.progress({
							text:'执行中。。。'
							});
							},
							complete: function(){
							//AJAX请求完成时隐藏loading提示
							$(document).ready(function(){$.messager.progress('close');});
							},
		                    success: function (result) {  
		                        if (result == "true") {  
		                            $.messager.alert("提示", "恭喜您，指标驱动模型计算成功！", "info");
		                            $("#targetinfo").treegrid("reload"); 
		                             
		                        } else if(result=="databaseError") {  
		                            $.messager.alert("提示", "数据库发生异常，请重新操作！", "info");  
		                            return;  
		                        }  else{
		                        	$.messager.alert("提示", result, "info");
		                        	return;
		                        }
		                    },
		                }); 
		    		}else{
		    		$.messager.alert("提示", "CL正在开发中^_^", "info");
		    		return;
		    	}
	    	}
	    	} 
	    
	    function mconclick(){
	    	$("#ht").prop('disabled',false);
	    	$("#bd").prop('disabled',false);
	    }
	    function clonclick(){
	    	$("#ht").prop('disabled',true);
	    	$("#bd").prop('disabled',true);
	    	$("#ht").attr("checked",false);
	    	$("#bd").attr("checked",false);
	    }
	    function ontooltipT(){
	    	var text = $("#targettext").val();
	    	$("#targettext").tooltip("update",text);
	    }
	    function ontooltipM(){
	    	var text = $("#modeltext").val();
	    	$("#modeltext").tooltip("update",text);
	    }
     </script> 
     <body style="min-width:1280px;min-height:1024px;"  > 
     	<div class="wrap"> 
		<div class="left-right-middle1">
		<jsp:include page="/WEB-INF/views/main/head.jsp" flush="true" ></jsp:include>
		<div class="easyui-layout"  style="width:auto;height:908px;"  >
			
      <div data-options="region:'north',title:'指标驱动计算',split:false,collapsible:false"   style="height:150px;">
      	 <div style="width: 460px;height:42px; float:left; " >
         <table style=" height: 40px; ;" cellspacing="0" border="0">  
                <tr>  
                    <td> &nbsp 型号</td>  
                    <td><input id="slelct_Model" name="userid"  style="width: 180px;height: 26px;"></td> 
                    <td> &nbsp 方案</td>  
                    <td><input id="slelct_Project" name="userid"  style="width: 180px;height: 26px;"></td> 
                </tr>  
          </table> 
          </div>
          <div style="width: 400px;height:42px;float:right; " >
           <form id="fileformname"  method="post" enctype="multipart/form-data">
           <table style=" height: 38px;  " cellspacing="0" border="0" >  
                <tr>  
                    <td> &nbsp 计算模型</td>  
                    <td>
						<input type="file" style="width: 300px;" id="file" name="pxcFile" onchange="filechange();"/>
                    </td>
                    <td></td> 
                </tr>  
          </table> 
          
         </form>
          </div> 
			<table style=";width:100%;height: auto;" >
		 <tr align="center">
          <td colspan="4">指标<input  id="targettext" readonly="readonly" style="width: 200px;" onmousemove="ontooltipT();">  &nbsp&nbsp <input  id="modeltext" readonly="readonly" style="width: 200px;" onmousemove="ontooltipM();">模型</td>
		 </tr>
		 <tr align="center">  
             <td colspan="4">
               <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-edit" onclick="addmapping();">建立映射</a>
     &nbsp&nbsp<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" onclick="removemapping();">删除映射</a>
             </td>             
          </tr> 
		 </table>
        </div> 
		<div data-options="region:'center',split:false,collapsible:false" border="0" style="height:500px;">
			<div class="style1" style="overflow:auto">
				<table id="targetinfo"  >
					
				  </table>
			</div>
			<div class="style2" style="overflow:auto">
				<table id="modelinfo" >
					
				  </table>
			</div>
		</div>
		<div data-options="region:'south',split:false,collapsible:false" border="0" style="height:300px;">
			<table style="width:100%;" >
          <tr align="left">
           <td colspan="4">计算方式 <label><input id = "mcRadio" name="Fruit" type="radio" value="ModelCenter" style="width: 20px;" onclick="mconclick();" />ModelCenter</label>
			<label><input name="Fruit" id="clRadio" type="radio" value="CenterLink" style="width: 20px;" onclick="clonclick();" />CenterLink</label></td>
         	<td colspan="3">
			<a href="javascript:void(0)" class="easyui-linkbutton"  onclick="checkOk();">执行</a>
			</td>
          <tr>
          <td>
          </td>
          </tr>
		 </table>
		 <div id="map_tab" style="width:auto;height: 500px;border:1px solid #ccc " > </div>
		</div>
		</div>
		</div>
		</div>
    </body>  
</html>
