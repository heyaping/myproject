<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"  %>
<%@page import="java.text.SimpleDateFormat"%> 
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<html>
  <head>
    <base href="<%=basePath%>">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>用户群组管理</title>
	<link rel="stylesheet" type="text/css" href="STATIC/css/default.css">
	<link rel="stylesheet" type="text/css" href="STATIC/js/jquery-easyui-1.3.5/themes/gray/easyui.css">
	<link rel="stylesheet" type="text/css" href="STATIC/js/jquery-easyui-1.3.5/themes/icon.css" />
	<link href="STATIC/css/main.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="STATIC/css/mainstyles.css" type="text/css"></link>
	<script type="text/javascript" src="STATIC/js/jquery.min.js"></script>
	<script type="text/javascript" src="STATIC/js/work.js"></script>
	<script type="text/javascript" src="STATIC/js/jquery-easyui-1.3.5/jquery-1.10.2.min.js"></script>
	<script type="text/javascript" src="STATIC/js/jquery-easyui-1.3.5/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="STATIC/js/JQuery-zTree-v3.5.15/jquery.ztree.all-3.5.min.js"></script>
	<script type="text/javascript" src="STATIC/js/index.js"></script>
	<script type="text/javascript" src="STATIC/js/extends.js"></script>
	<script type="text/javascript" src="STATIC/js/common.js"></script>
  </head>
  <script type="text/javascript">  
  	var url = "";
    /* 启动时加载 */  
    $(function(){  
     $("#department_tab").datagrid({  
            title: '用户群组信息管理',  
            checkOnSelect: false,  
            toolbar: '#tb',
            fitColumns :true, 
            checkOnSelect:true,
            singleSelect: false,
            selectOnCheck: true,
            nowrap:true, 
            url: '/PlanManagement/deptManager/search.action',  
            loadMsg:'加载中...',  
            fit: true,
            rowStyler:function(index,row){
			if (index%2==1){
				return 'background-color:#D2D7D3;';
			}
			 else{
			return 'background-color:#50a3a2;';
			} 
		},    
            columns: [[  
                { field: 'deptID', checkbox: true},  
                { field: 'deptCode', title: '用户群组编码', width:100},  
                { field: 'deptShortName', title: '用户群组简称', width:100},  
                { field: 'deptLongName', title: '用户群组全称', width:160},  
                { field: 'leaderName', title: '群组管理', width:160},  
                { field: 'createDT', title: '创建时间', width:150},  
                { field: 'creator', title: '创建者', width:150},  
                { field: 'modifyDT', title: '修改时间', width:150 },  
                { field: 'modifier', title: '修改者', width:150 },  
            ]]            
        });
      }) ;
        
        /* 添加用户群组信息 */  
   	 function addUser(){  
        $("#enditTab").dialog("open").dialog('setTitle', '添加用户群组信息');    
        $("#enditTab").panel("move",{top:$(document).scrollTop() + ($(window).height()-250) * 0.5}); 
      	$("#fm").form("clear"); 
      	url = "/PlanManagement/deptManager/add.action";
    	}
    	/*选择负责人*/
    	 function checkCharge(){  
    	     $("#user_tab").datagrid({  
            checkOnSelect: false,  
            fitColumns :true, 
            checkOnSelect:true,
            singleSelect: true,
            selectOnCheck: true,
            nowrap:true, 
            url: '/PlanManagement/userManager/roleSearch.action',  
            loadMsg:'加载中...',  
            fit: true,
            rowStyler:function(index,row){
		     if (index%2==1){
				return 'background-color:#D2D7D3;';
			}
			 else{
			return 'background-color:#50a3a2;';
			}  
		},    
            columns: [[  
                { field: 'userID', checkbox: true},  
                { field: 'userCode', title: '用户帐号', width:100},  
                { field: 'userName', title: '用户姓名', width:100},  
                { field: 'sex', title: '性别', width:100},  
                { field: 'tel', title: '电话', width:160},  
                { field: 'email', title: '电子邮件', width:160},  
                { field: 'jobName', title: '职务', width:100},  
                { field: 'roleID', title: '系统角色', width:100,
                formatter:function(value,row,index){if(value=='1'){return'系统管理员';}if(value=='2'){return'管理者';}if(value=='3'){return'普通用户';}}},  
            ]]            
        });
        $("#userTab").dialog("open").dialog('setTitle', '为当前用户群组选择负责人');    
        $("#userTab").panel("move",{top:$(document).scrollTop() + ($(window).height()-250) * 0.5}); 
      	//url = "/PlanManagement/userManager/add.action";
      	
      	
    	}
    	   
         function checkOk(){
          var row = $("#user_tab").datagrid("getSelected");  
         $('#leaderName').val(row.userName);
         $('#leaderID').val(row.userID);
         $('#userTab').dialog('close');
         }
         /* 数据校验后提交 */  
    	function checkInputAdd(){  
        
        $("#fm").form("submit", {  
            url: url,  
            onsubmit: function () {  
                return $(this).form("validate");  
            },  
            success: function (result) {  
                if (result == "true") {  
                    $.messager.alert("提示信息", "操作成功");  
                    $("#enditTab").dialog("close");  
                    $("#department_tab").datagrid("load");  
                }  
                else {  
                    $.messager.alert("提示信息", "保存数据失败");  
                }  
            }  
        });  
    }  
        /* 修改用户群组信息 */  
    function editUser(){  
        var checkedItems = $('#department_tab').datagrid('getSelections');  
        var deptIds = [];  
        var num = 0;  
        $.each(checkedItems, function(index, item){  
            deptIds.push(item.deptID);  
            num++;  
        });   
        if(num != 1){  
            $.messager.alert('提示','请选择一条数据进行修改');  
            return false;  
        }  
        var row = $("#department_tab").datagrid("getSelected");  
        if (row) {  
            $("#enditTab").dialog("open").dialog('setTitle', '修改用户信息');  
            $("#enditTab").panel("move",{top:$(document).scrollTop() + ($(window).height()-250) * 0.5}); 
            $("#fm").form("load", row);  
           url = "/PlanManagement/deptManager/modify.action";  
        }  
    }
      function removeUser() {  
        //返回选中多行  
        var selRow = $('#department_tab').datagrid('getSelections');  
        //判断是否选中行  
        if (selRow.length==0) {  
            $.messager.alert("提示", "请选择要删除的行！", "info");  
            return;  
        }else{      
            var deptID="";  
            //批量获取选中行的ID  
            for (i = 0; i < selRow.length;i++) {  
                if (deptID =="") {  
                    deptID = selRow[i].deptID;  
                } else {  
                    deptID = selRow[i].deptID + "," + deptID;  
                }                 
            }  
            $.messager.confirm('提示', '是否删除选中数据?', function (r) {  
                if (!r) {  
                    return;  
                }  
                //提交  
                $.ajax({  
                    type: "POST",  
                    async: false,  
                    url: "/PlanManagement/deptManager/remove.action?deptIDs=" + deptID,  
                    data: 'json',  
                    success: function (result) {  
                        if (result == "true") {  
                            $('#department_tab').datagrid('clearSelections');  
                            $.messager.alert("提示", "恭喜您，信息删除成功！", "info");  
                            $('#department_tab').datagrid('reload');  
                        } else {  
                            $.messager.alert("提示", "删除失败，请重新操作！", "info");  
                            return;  
                        }  
                    }  
                });  
            });  
        }  
    }; 
     </script> 
    <body style="min-width:1280px;min-height:1024px;">
		<div class="wrap">
			<div class="left-right-middle1">
			<jsp:include page="/WEB-INF/views/main/head.jsp" flush="true" ></jsp:include>
			  <div id="tb" style="width: auto; height: 48px;">  
            <table style="width: auto; height: 48px;" cellspacing="0" border="0">  
                <tr>  
                    <td>&nbsp<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" onclick="addUser();">添加</a> &nbsp
                    <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" onclick="removeUser();">删除</a>  &nbsp
                    <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-edit" onclick="editUser();">编辑</a></td>            
                </tr>  
            </table>  
        </div>  
        <table id="department_tab" ></table>  
          <div id="enditTab" class="easyui-dialog" style="border:1px solid #D2D7D3;width: 620px; height: auto; padding: 10px 20px;" closed="true" buttons="#dlg-buttons" shadow="false" >    
        <form id="fm" method="post" >  
            <table border="0" style="width:98%;">  
                <input type="hidden"  id="userid" name="userid"  value=""  style="width: 180px;height: 20px;">  
                <tr style="height: 35px;">  
                    <td>群组编号</td>  
                    <td><input id="deptID" name="deptID" readonly="readonly" style="width: 180px;height: 20px;"></td>  
                    <td></td>
                    <td>群组代号<span style="color:#ff0000">*</span></td>  
                    <td><input  id="deptCode" name="deptCode"  value=""  style="width: 180px;height: 20px;"></td>  
                </tr>  
               <tr style="height: 35px;">  
                    <td>群组简称<span style="color:#ff0000">*</span></td>  
                    <td><input id="deptShortName" name="deptShortName" style="width: 180px;height: 20px;"></td>  
                     <td></td>
                    <td>群组全称</td>  
                    <td><input  id="deptLongName"  name="deptLongName"  value=""  style="width: 180px;height: 20px;">
                   </td>  
                </tr>
                <tr style="height: 35px;">  
                    <td>负责人<span style="color:#ff0000">*</span> </td>  
                    <td><input id="leaderName" name="leaderName" readonly="readonly" style="width: 180px;height: 20px;">
                    	<input id= "leaderID" name="leaderID" type="hidden" >
                     </td>
                    <td><a href="javascript:void(0)" class="easyui-linkbutton" plain="true" iconCls="icon-search"   onclick="checkCharge();"   ></a></td>  
                </tr>
                <tr style="height: 50px;">  
                    <td colspan="5" align="center">  
                        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-ok" onclick="checkInputAdd();">确认</a>      
                        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#enditTab').dialog('close')">取消</a>  
                    </td>  
                </tr>  
            </table>   
        </form>    
    </div>  
    
      <div id="userTab" class="easyui-dialog" style="border:1px solid #D2D7D3;width: 780px; height:400px; padding: 10px 20px; overflow:hidden;"  closed="true" buttons="#dlg-buttons" shadow="false"  >  
       <div id="tb" style="width: auto; height: 48px;">  
            <table style="width: auto; height: 48px;" cellspacing="0" border="0">  
                <tr>  
                    <td>&nbsp <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-ok" onclick="checkOk();">确认</a>  &nbsp
                      <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#userTab').dialog('close')">取消</a> 
                 	</td>            
                </tr>  
            </table>  
        </div>    
        	<table id="user_tab" ></table>  
     </div>
			</div>
		</div>
    </body>
</html>
