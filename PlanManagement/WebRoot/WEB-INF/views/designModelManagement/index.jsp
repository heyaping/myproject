<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@page import="java.text.SimpleDateFormat"%> 
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
  <head>
    <base href="<%=basePath%>">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>设计型号管理</title>
	<link rel="stylesheet" type="text/css" href="STATIC/css/default.css">
	<link rel="stylesheet" type="text/css" href="STATIC/js/jquery-easyui-1.3.5/themes/gray/easyui.css">
	<link rel="stylesheet" type="text/css" href="STATIC/js/jquery-easyui-1.3.5/themes/icon.css" />
	<link href="STATIC/css/main.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="STATIC/css/mainstyles.css" type="text/css"></link>
	<script type="text/javascript" src="STATIC/js/jquery.min.js"></script>
	<script type="text/javascript" src="STATIC/js/work.js"></script>
	<script type="text/javascript" src="STATIC/js/jquery-easyui-1.3.5/jquery-1.10.2.min.js"></script>
	<script type="text/javascript" src="STATIC/js/jquery-easyui-1.3.5/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="STATIC/js/JQuery-zTree-v3.5.15/jquery.ztree.all-3.5.min.js"></script>
	<script type="text/javascript" src="STATIC/js/index.js"></script>
	<script type="text/javascript" src="STATIC/js/extends.js"></script>
	<script type="text/javascript" src="STATIC/js/common.js"></script>
  </head>
  <script type="text/javascript">  
    /* 启动时加载 */  
    $(function(){  
    var srow=0;
    initButton();
    initRight();
     $("#model_list").datagrid({  
            url: '/PlanManagement/designxhManager/searchDesignxh.action',  
            loadMsg:'加载中...',  
            checkOnSelect: false,  
            fitColumns :true, 
            checkOnSelect:true,
            singleSelect: false,
            selectOnCheck: true,
            rowStyler:function(index,row){
			if (index%2==1){
				return 'background-color:#D2D7D3;';
			}
			 else{
			return 'background-color:#50a3a2;';
			} 
		},
            nowrap:true, 
             //点击行 触发 右边属性显示
            onClickRow:function(rowIndex,rowData){
            removeAttr();
            $(this).datagrid("unselectAll");//取消选中当前所有行
			$(this).datagrid("selectRow",rowIndex);//选中当前点击的行
            XZButton(rowData.status);
            srow=rowIndex;
         	$('#modelID').val(rowData.modelID);
         	$('#modelName').val(rowData.modelName);
         	$('#leaderName').val(rowData.leaderName);
         	$('#status').val(rowData.status);
         	if(rowData.status == '0'){
         		$('#statusName').val('未启用');
         	}else if(rowData.status == '-1'){
         		$('#statusName').val('已停用');
         	}else{
         		$('#statusName').val('已启用');
         	}
         	$('#remark').val(rowData.remark);
         	$('#beginDT').datebox("setValue",rowData.beginDT);
         	$('#endDT').datebox("setValue",rowData.endDT);
         	$('#leaderID').val(rowData.director);
         	$("#model_list2").datagrid('reload',{modelID:rowData.modelID});
  					},    
            columns: [[  
                { field: 'id', checkbox: true},  
                { field: 'modelID', title: '编号', width:20},
                { field: 'modelName', title: '名称', width:100},    
            ]]            
        });
           $("#model_list2").datagrid({  
            url:'/PlanManagement/designxhManager/searchXhattr.action',  
            queryParams:{modelID:'0'},
            loadMsg:'加载中...',  
            fit: true,
            checkOnSelect: false,  
            fitColumns :true, 
            checkOnSelect:true,
            singleSelect: false,
            selectOnCheck: true,
            rowStyler:function(index,row){
			if (index%2==1){
				return 'background-color:#D2D7D3;';
			}
			 else{
			return 'background-color:#50a3a2;';
			} 
		},
            onClickRow:function(rowIndex,rowData){
            $(this).datagrid("unselectAll");//取消选中当前所有行
			$(this).datagrid("selectRow",rowIndex);//选中当前点击的行
			},
            columns: [[  
                { field: 'attrID', checkbox: true},  
                	{ field: 'attrName', title: '名称', width:10},
                	{ field: 'attrVal', title: '数值', width:20}, 
                	{ field: 'remark', title: '描述', width:50},   
            ]]            
        });
      }) ;
          /* 添加战术指标信息 */  
   	 function addTacticalIndex(){  
        $("#enditTab").dialog("open").dialog('setTitle', '添加战技指标');    
        $("#enditTab").panel("move",{top:$(document).scrollTop() + ($(window).height()-250) * 0.5}); 
      	$("#fmtwo").form("clear"); 
      	//url = "/PlanManagement/userManager/add.action";
    	}
    	
    	
    	/*选择负责人*/
    	 function checkCharge(){  
    	     $("#user_tab").datagrid({  
            checkOnSelect: false,  
            fitColumns :true, 
            checkOnSelect:true,
            singleSelect: true,
            selectOnCheck: true,
            nowrap:true, 
	        url: '/PlanManagement/userManager/search.action',  
            loadMsg:'加载中...',  
            fit: true, 
            rowStyler:function(index,row){
			if (index%2==1){
				return 'background-color:#D2D7D3;';
			}
			 else{
			return 'background-color:#50a3a2;';
			}  
		},
            columns: [[  
                { field: 'userID', checkbox: true},  
                { field: 'userCode', title: '用户帐号', width:100},  
                { field: 'userName', title: '用户姓名', width:100},  
                { field: 'sex', title: '性别', width:100},  
                { field: 'tel', title: '电话', width:160},  
                { field: 'email', title: '电子邮件', width:160},  
                { field: 'jobName', title: '职务', width:100},  
                { field: 'roleID', title: '系统角色', width:100,
                formatter:function(value,row,index){if(value=='1'){return'系统管理员';}if(value=='2'){return'管理者';}if(value=='3'){return'普通用户';}}},  
            ]]            
        });
        $("#userTab").dialog("open").dialog('setTitle', '为当前群组选择负责人');    
        $("#userTab").panel("move",{top:$(document).scrollTop() + ($(window).height()-250) * 0.5}); 
      	//url = "/PlanManagement/userManager/add.action";
      	
      	
    	}  
    	function checkInputAdd(){
    		var modelID = $("#modelID").val();
	    	$("#fmtwo").form("submit", {  
	            url: "/PlanManagement/designxhManager/addXhattr.action?modelID="+modelID,  
	            onsubmit: function () {  
	                return $(this).form("validate");  
	            },  
	            success: function (result) {  
	                if (result == "true") {  
	                    $.messager.alert("提示信息", "操作成功");  
						$('#enditTab').dialog('close');
	                    $("#model_list2").datagrid('reload');
	                }else if(result == "exist"){
	                	$.messager.alert("提示信息", "此型号下已存在该战技指标");
	                }  
	                else {  
	                    $.messager.alert("提示信息", "保存数据失败");  
	                }  
	            }  
	        });  
    	}
    	 function checkOk(){
         var row = $("#user_tab").datagrid("getSelected");  
         $('#leaderName').val(row.userName);
         $('#leaderID').val(row.userID);
         $('#userTab').dialog('close');
         }
        //格式化日期
        $.fn.datebox.defaults.formatter = function(date){
		var y = date.getFullYear();
		var m = date.getMonth()+1;
		var d = date.getDate();
		return y+'-'+m+'-'+d;
		};
    	
    	  /* 修改技战指标 */  
    function editUser(){  
        var checkedItems = $('#model_list2').datagrid('getSelections');  
        var userIds = [];  
        var num = 0;  
        $.each(checkedItems, function(index, item){  
            userIds.push(item.userID);  
            num++;  
        });   
        if(num != 1){  
            $.messager.alert('提示','请选择一条数据进行修改');  
            return false;  
        }  
        var row = $("#model_list2").datagrid("getSelected");  
        if (row) {  
            $("#enditTab").dialog("open").dialog('setTitle', '修改用户信息');  
            $("#enditTab").panel("move",{top:$(document).scrollTop() + ($(window).height()-250) * 0.5}); 
            $("#fmtwo").form("load", row);  
        }  
    }
      /*删除技战指标*/
      function removeAttr() {  
        //返回选中多行  
        var selRow = $('#model_list2').datagrid('getSelections');  
        //判断是否选中行  
        if (selRow.length==0) {  
            $.messager.alert("提示", "请选择要删除的行！", "info");  
            return;  
        }else{      
            var attrID="";  
            //批量获取选中行的ID  
            for (i = 0; i < selRow.length;i++) {  
                if (attrID =="") {  
                    attrID = selRow[i].attrID;  
                } else {  
                    attrID = selRow[i].attrID + "," + attrID;  
                }                 
            }  
            $.messager.confirm('提示', '是否删除选中数据?', function (r) {  
                if (!r) {  
                    return;  
                }  
                //提交  
                $.ajax({  
                    type: "POST",  
                    async: false,  
                    url: "/PlanManagement/designxhManager/removeAttr.action?attrIDs=" + attrID,  
                    data: 'json',  
                    success: function (result) {  
                        if (result == "true") {  
                            $('#model_list2').datagrid('clearSelections');  
                            $.messager.alert("提示", "恭喜您，信息删除成功！", "info");  
                            $('#model_list2').datagrid('reload');  
                        } else {  
                            $.messager.alert("提示", "删除失败，请重新操作！", "info");  
                            return;  
                        }  
                    }  
                });  
            });  
        }  
    }; 
    //右边只读状态   
      function initRight(){
     // $('input:text').attr("disabled","disabled");//设为不可用
      $('input:text').attr("readonly","readonly");//设为只读
      $("#zg").linkbutton("disable");
      $("#remark").attr("readonly","readonly");
      $("#attrName").removeAttr("readonly","readonly");
      $("#attrVal").removeAttr("readonly","readonly");
      $("#zjremark").removeAttr("readonly","readonly");
      }
      //右边可修改状态
      function removeAttr(){
      $("#modelName").removeAttr("readonly");//取消只读的设置
      $("#zg").linkbutton("enable");
      $("#remark").removeAttr("readonly","readonly");
      }
      function addButton(){
      	$('#okButton').linkbutton('enable'); 
		$("#removeButton").linkbutton("disable");
    	$("#QYButton").linkbutton("disable");
     	$("#TYButton").linkbutton("disable");
      }
      function initButton(){
    	 $("#okButton").linkbutton("disable");
     	 $("#QYButton").linkbutton("disable");
    	 $("#TYButton").linkbutton("disable");
    	 $("#attr1").linkbutton("disable");
    	 $("#attr2").linkbutton("disable");
    	 $("#attr3").linkbutton("disable");
      }
      function XZButton(status){
      	 $("#removeButton").linkbutton("enable");
    	 $("#okButton").linkbutton("enable");
    	 if(status == 0 || status == -1){
 	    	 $("#QYButton").linkbutton("enable");
			$("#TYButton").linkbutton("disable");
			$("#okButton").linkbutton("enable");
			$("#attr1").linkbutton("disable");
    	  	 $("#attr2").linkbutton("disable");
    	 	 $("#attr3").linkbutton("disable");
    	 }else{
	    	 $("#TYButton").linkbutton("enable");
	    	  $("#QYButton").linkbutton("disable");
	    	  $("#okButton").linkbutton("disable");
	    	 $("#attr1").linkbutton("enable");
    	  	 $("#attr2").linkbutton("enable");
    	 	 $("#attr3").linkbutton("enable");
    	 }
      }
      
      
		function addModel(){ 
		addButton();
		removeAttr();
      	$("#fm").form("clear"); 
		$('#model_list').datagrid('clearSelections');
		$('#model_list2').datagrid('loadData',{total:0,rows:[]}); 
    		}
    	function removeModel(){
    	 //返回选中多行  
        var selRow = $('#model_list').datagrid('getSelections');  
        //判断是否选中行  
        if (selRow.length==0) {  
            $.messager.alert("提示", "请选择要删除的行！", "info");  
            return;  
        }else{      
            var modelID="";  
            //批量获取选中行的ID  
            for (i = 0; i < selRow.length;i++) {  
                if (modelID =="") {  
                    modelID = selRow[i].modelID;  
                } else {  
                    modelID = selRow[i].modelID + "," + modelID;  
                }                 
            }  
            $.messager.confirm('提示', '是否删除选中数据?', function (r) {  
                if (!r) {  
                    return;  
                }  
                //提交  
                $.ajax({  
                    type: "POST",  
                    async: false,  
                    url: "/PlanManagement/designxhManager/removeModel.action?modelIDs=" + modelID,  
                    data: 'json',  
                    success: function (result) {  
                        if (result == "true") {  
                            $('#model_list').datagrid('clearSelections');  
                            $.messager.alert("提示", "恭喜您，信息删除成功！", "info");  
                            $('#model_list').datagrid('reload');  
                            $("#fm").form("clear");
                             $('#model_list2').datagrid('loadData',{total:0,rows:[]}); 
                  			initButton();
                        } else {  
                            $.messager.alert("提示", "删除失败，请重新操作！", "info");  
                            return;  
                        }  
                    }  
                });  
            });  
        } 
    	
    	}
    	function bcModel(){
    	   $("#fm").form("submit", {  
            url: '/PlanManagement/designxhManager/addDesignxh.action',  
            onsubmit: function () {  
                return $(this).form("validate");  
            },  
            success: function (result) {  
                if (result == "add") {  
                    $.messager.alert("提示信息", "操作成功");  
                    $('#model_list').datagrid('clearSelections');
                    $("#model_list").datagrid("load"); 
                    $("#fm").form("clear"); 
                    initButton();
                }else if(result == "update"){
               		 $('#model_list').datagrid('reload');
                	$.messager.alert("提示信息", "操作成功");
                }else if(result = "exist"){
                	$.messager.alert("提示信息", "设计型号名已存在");
                }  
                else {  
                    $.messager.alert("提示信息", "保存数据失败");  
                }  
            }  
        });
    	}
    	function qyModel(){
    		var modelID = $("#modelID").val();
    		if(modelID==""){
    			return;
    		}
			$.ajax({  
                    type: "POST",  
                    async: false,  
                    url: "/PlanManagement/designxhManager/qyDesignxh.action?modelID=" + modelID,  
                    data: 'json',  
                    success: function (result) {  
                        if (result == "true") {  
                            $.messager.alert("提示", "恭喜您，型号启动成功！", "info");  
                            $('#model_list').datagrid('reload');  
							$("#statusName").val('已启动');
							XZButton(1);
                        } else {  
                            $.messager.alert("提示", "启动失败，请重新操作！", "info");  
                            return;  
                        }  
                    }  
                });  
        }
    	function tyModel(){
    		var modelID = $("#modelID").val();
    		if(modelID==""){
    			return;
    		}
			$.ajax({  
                    type: "POST",  
                    async: false,  
                    url: "/PlanManagement/designxhManager/tyDesignxh.action?modelID=" + modelID,  
                    data: 'json',  
                    success: function (result) {  
                        if (result == "true") {  
                            $.messager.alert("提示", "恭喜您，型号停用成功！", "info");
                            initRight();  
                            $('#model_list').datagrid('reload');  
							$("#statusName").val('已停用');
							XZButton(-1);
                        } else {  
                            $.messager.alert("提示", "停用失败，请重新操作！", "info");  
                            return;  
                        }  
                    }  
                });  
        }
     </script> 
    <body style="min-width:1280px;min-height:1024px;">
		<div class="wrap">
			<div class="left-right-middle1">
			<jsp:include page="/WEB-INF/views/main/head.jsp" flush="true" ></jsp:include>
			<div class="easyui-layout"  style="min-width:1280px;min-height:908px;">
			<div data-options="region:'north',title:'设计型号管理',split:false" collapsible="false" style="height:90px; background-color: #F1F3F7">
        		<table style="width: auto; height: 60px;" cellspacing="0" border="0" >  
               		<tr>  
                    	<td>
                    &nbsp<a id="addButton" href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" onclick="addModel();">增加</a> 
                    &nbsp<a id="removeButton" href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" onclick="removeModel();">删除</a> 
                    &nbsp<a id="okButton" href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-save" onclick="bcModel();">保存</a>
                    &nbsp<a id="QYButton" href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-ok" onclick="qyModel();">启用</a>
                    &nbsp<a id="TYButton" href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-no" onclick="tyModel();">停用</a>
                    	</td>             
                	</tr>  
         		 </table>  
       		 </div>
       		  <div region="west" split="false" title="型号列表" style="width:360px;background-color: #F1F3F7"   >
			 <table id="model_list" border="0"></table>
			 </div>
			<div id="content" region="center" title="型号详细"  split="false"  style="padding:5px;background-color: #F1F3F7">
		 <form id="fm" method="post" >  
            <table border="0" style="width:800px;" >  
                <input type="hidden"  id="moveid" name="moveid"  value=""  style="width: 180px;">  
                <tr style="height: 35px;">  
                    <td width="88px" style="padding-left: 28px">型号编号</td>  
                    <td ><input id="modelID" name="modelID" readonly="readonly" style="width: 180px;height: 20px;"></td>
                    <td width="88px">型号名称<span style="color:#ff0000">*</span></td>  
                    <td><input  id="modelName" name="modelName" class="easyui-validatebox" data-options="required:true" missingMessage="型号名称不能为空" value=""  style=" width: 180px;height: 20px;"></td>  
                </tr>  
               <tr style="height: 38px;">  
                    <td style="padding-left: 28px">型号主管<span style="color:#ff0000">*</span></td>  
                    <td><input id="leaderName" name="leaderName" readonly="readonly" class="easyui-validatebox" data-options="required:true" missingMessage="型号主管不能为空" style="width: 180px;height: 20px;">
                    	<input id="leaderID" name="director" type="hidden">
                     <a href="javascript:void(0)" class="easyui-linkbutton" id="zg" plain="true"  iconCls="icon-search" onclick="checkCharge();" style="height: 15px;"  ></a>
                     </td> 
                    <td>型号状态</td>
                    <td><input  id="statusName" name="statusName" readonly="readonly"  value=""  style="width: 180px;height: 20px;">
                    	<input type="hidden" id="status" name="status" value="">
                   </td>  
                </tr>
                <tr style="height: 35px;">  
                    <td style="padding-left: 28px">型号描述</td>  
                    <td colspan="4"> <textarea id="remark" name="remark" value=""  style="resize:none; width:593px;background-color: #F1F3F7; height:100px;"></textarea></td>  
                </tr>
                <tr style="height: 35px;">  
                    <td style="padding-left: 28px">开始时间<span style="color:#ff0000">*</span></td>  
                    <td><input id="beginDT" name="beginDT" type="text"  class="easyui-datebox"  missingMessage="开始时间不能为空" style="width: 187px;height: 28px;" required="required" value="" ></input></td>
                    <td>结束时间<span style="color:#ff0000">*</span></td>  
                    <td><input id="endDT" name="endDT" type="text"  class="easyui-datebox" style="width: 187px;height: 28px;" required="required" missingMessage="结束时间不能为空" value="" ></input></td>  
                </tr>
                <tr style="height: 300px;">  
                    <td style="padding-left: 28px">技战指标</td>
                    <td colspan="4">  <table id="model_list2" style="width:400px;" ></table> </td>  
                </tr>
                <tr style="height: 50px;">  
                <td></td> 
                    <td colspan="3" align="left"> 
               &nbsp<a id="attr1" href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" onclick="addTacticalIndex();">添加</a> &nbsp
                    <a id="attr2" href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" onclick="removeAttr();">删除</a>  &nbsp
                    <a id="attr3" href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-edit" onclick="editUser();">编辑</a>  
                    </td>  
                </tr>  
            </table>   
        </form>
		</div>
		    <div id="enditTab" class="easyui-dialog"  style="width: 320px;border:1px solid #D2D7D3; height: 280; padding: 10px 20px;" closed="true" buttons="#dlg-buttons" shadow="false" >    
        <form id="fmtwo" method="post" >  
            <table border="0"  style="width:98%;">  
                <input type="hidden"  id="attrID" name="attrID"  value=""  style="width: 180px;">  
                <tr  style="height: 35px;">  
                    <td >名称<span style="color:#ff0000">*</span></td>  
                    <td><input  id="attrName" name="attrName" class="easyui-validatebox" data-options="required:true" missingMessage="战技指标名不能为空"  value=""  style="width: 180px;height: 20px;"></td>  
                </tr>  
               <tr style="height: 35px;">  
                    <td>数值<span style="color:#ff0000">*</span></td>  
                    <td><input id="attrVal" name="attrVal" class="easyui-validatebox" data-options="required:true" missingMessage="数值不能为空" style="width: 180px;height: 20px;"></td>  
                </tr>
                <tr style="height: 35px;">  
                    <td>描述</td>  
                    <td><input id="zjremark" name="remark" style="width: 180px;height: 20px;"></td>  
                </tr>
                <tr style="height: 50px;">  
                    <td colspan="4" align="center">  
                        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-ok" onclick="checkInputAdd();">确认</a>      
                        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#enditTab').dialog('close')">取消</a>  
                    </td>  
                </tr>  
            </table>   
        </form>    
    </div>
     <div id="userTab" class="easyui-dialog" style="width: 780px; height:400px;border:1px solid #D2D7D3; padding: 10px 20px; overflow:hidden;"  closed="true" buttons="#dlg-buttons" shadow="false"  >  
       <div id="tb" style="width: auto; height: 48px;">  
            <table style="width: auto; height: 48px;" cellspacing="0" border="0">  
                <tr>  
                    <td>&nbsp <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-ok" onclick="checkOk();">确认</a>  &nbsp
                      <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#userTab').dialog('close')">取消</a> 
                 	</td>            
                </tr>  
            </table>  
        </div>    
        	<table id="user_tab" ></table>  
     </div>
			</div>
		</div>
		</div>
	</body>	
</html>
