<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"  %>
<%@page import="java.text.SimpleDateFormat"%> 
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<html>
  <head>
    <base href="<%=basePath%>">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>用户归属用户群组</title>
	<link rel="stylesheet" type="text/css" href="STATIC/css/default.css">
	<link rel="stylesheet" type="text/css" href="STATIC/js/jquery-easyui-1.3.5/themes/gray/easyui.css">
	<link rel="stylesheet" type="text/css" href="STATIC/js/jquery-easyui-1.3.5/themes/icon.css" />
	<link href="STATIC/css/main.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="STATIC/css/mainstyles.css" type="text/css"></link>
	<script type="text/javascript" src="STATIC/js/jquery.min.js"></script>
	<script type="text/javascript" src="STATIC/js/work.js"></script>
	<script type="text/javascript" src="STATIC/js/jquery-easyui-1.3.5/jquery-1.10.2.min.js"></script>
	<script type="text/javascript" src="STATIC/js/jquery-easyui-1.3.5/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="STATIC/js/JQuery-zTree-v3.5.15/jquery.ztree.all-3.5.min.js"></script>
	<script type="text/javascript" src="STATIC/js/index.js"></script>
	<script type="text/javascript" src="STATIC/js/extends.js"></script>
	<script type="text/javascript" src="STATIC/js/common.js"></script>
	
  </head>
  <script type="text/javascript">  
  	var deptID = "";
  	var url = "";
    /* 启动时加载 */  
    $(function(){  
     $("#department_tab").datagrid({  
            title: '用户群组对应用户',  
            checkOnSelect: false,  
            fitColumns :true, 
            checkOnSelect:true,
            singleSelect: true,
            selectOnCheck: false,
            nowrap:true, 
            url: '/PlanManagement/deptManager/search.action',  
            loadMsg:'加载中...',  
            fit: true,
            rowStyler:function(index,row){
			if (index%2==1){
				return 'background-color:#D2D7D3;';
			}
			 else{
			return 'background-color:#50a3a2;';
			}  
		},    
            //当用户点击当前行触发
            onClickRow:function(rowIndex,rowData){
            deptID = rowData.deptID;
   				$("#departmentUser_tab").datagrid({  
            checkOnSelect: false,  
            fitColumns :true, 
            checkOnSelect:true,
            singleSelect: false,
            selectOnCheck: true,
            nowrap:true,
            rowStyler:function(index,row){
			if (index%2==1){
				return 'background-color:#D2D7D3;';
			}
			 else{
			return 'background-color:#50a3a2;';
			}  
		},     
            url: '/PlanManagement/userToDeptManager/searchByDept.action?deptID='+deptID,  
            loadMsg:'加载中...',  
            fit: true,  
            columns: [[  
                { field: 'userID', checkbox: true},  
                { field: 'userCode', title: '用户帐号', width:100},  
                { field: 'userName', title: '用户姓名', width:100},  
                { field: 'sex', title: '性别', width:100},  
                { field: 'tel', title: '电话', width:160},  
                { field: 'email', title: '电子邮件', width:160},  
                { field: 'jobName', title: '职务', width:100},  
                { field: 'roleID', title: '系统角色', width:100,
                formatter:function(value,row,index){if(value=='1'){return'系统管理员';}if(value=='2'){return'管理者';}if(value=='3'){return'普通用户';}}},  
            ]]            
        });
  					},  
            columns: [[  
                { field: 'deptID', title: '用户群组编号', width:100},  
                { field: 'deptCode', title: '用户群组代号', width:100},  
                { field: 'deptShortName', title: '用户群组简称', width:100},  
                { field: 'deptLongName', title: '用户群组全称', width:160},  
                { field: 'leaderName', title: '群组管理', width:160},  
                { field: 'ct', title: '成员数', width:100},  
                { field: 'createDT', title: '创建时间', width:150},  
                { field: 'modifyDT', title: '修改时间', width:150 },  
            ]]   
             });
          	$("#departmentUser_tab").datagrid({  
            checkOnSelect: false,  
            fitColumns :true, 
            checkOnSelect:true,
            singleSelect: false,
            selectOnCheck: true,
            rowStyler:function(index,row){
			if (index%2==1){
				return 'background-color:#D2D7D3;';
			}
			 else{
			return 'background-color:#50a3a2;';
			}  
		},    
            nowrap:true, 
            //url: '/PlanManagement/userManager/search.action',  
            loadMsg:'加载中...',  
            fit: true,  
            columns: [[  
                { field: 'userID', checkbox: true},  
                { field: 'userCode', title: '用户帐号', width:100},  
                { field: 'userName', title: '用户姓名', width:100},  
                { field: 'sex', title: '性别', width:100},  
                { field: 'tel', title: '电话', width:160},  
                { field: 'email', title: '电子邮件', width:160},  
                { field: 'jobName', title: '职务', width:100},  
                { field: 'roleID', title: '系统角色', width:100,
                formatter:function(value,row,index){if(value=='1'){return'系统管理员';}if(value=='2'){return'管理者';}if(value=='3'){return'普通用户';}}},  
            ]]            
        });
         
      }) ;
        
        /* 为用户群组添加用户成员信息 */  
    	function addDepartmentUser(){
    		var deselRow = $('#department_tab').datagrid('getSelections');
    	 if (deselRow.length==0) {  
            $.messager.alert("提示", "请先选择用户群组！", "info");  
            return;  }else{
    	  $("#user_tab").datagrid({  
            checkOnSelect: false,  
            fitColumns :true, 
            checkOnSelect:true,
            singleSelect: false,
            selectOnCheck: true,
            nowrap:true,
            rowStyler:function(index,row){
			if (index%2==1){
				return 'background-color:#D2D7D3;';
			}
			 else{
			return 'background-color:#50a3a2;';
			} 
		},     
            url: '/PlanManagement/userToDeptManager/searchNotDept.action',  
            loadMsg:'加载中...',  
            fit: true,  
            columns: [[  
                { field: 'userID', checkbox: true},  
                { field: 'userCode', title: '用户帐号', width:100},  
                { field: 'userName', title: '用户姓名', width:100},  
                { field: 'sex', title: '性别', width:100},  
                { field: 'tel', title: '电话', width:160},  
                { field: 'email', title: '电子邮件', width:160},  
                { field: 'jobName', title: '职务', width:100},  
                { field: 'roleID', title: '系统角色', width:100,
                formatter:function(value,row,index){if(value=='1'){return'系统管理员';}if(value=='2'){return'管理者';}if(value=='3'){return'普通用户';}}},  
            ]]            
        });
        $("#userTab").dialog("open").dialog('setTitle', '为当前用户群组选择成员用户');    
        $("#userTab").panel("move",{top:$(document).scrollTop() + ($(window).height()-250) * 0.5}); 
      	//url = "/PlanManagement/userManager/add.action";
    	}
    	}
    	/*用户群组选择添加成员-点击确定按钮触发*/
    	function checkOk(){
         //返回选中多行  
        var selRow = $('#user_tab').datagrid('getSelections');  
        //判断是否选中行  
        if (selRow.length==0) {  
            $.messager.alert("提示", "请选择要添加的成员！", "info");  
            return;  
        }else{      
            var userID="";  
            //批量获取选中行的ID  
            for (i = 0; i < selRow.length;i++) {  
                if (userID =="") {  
                    userID = selRow[i].userID;  
                } else {  
                    userID = selRow[i].userID + "," + userID;  
                }                 
            }  
            $.messager.confirm('提示', '是否添加选中用户?', function (r) {  
                if (!r) {  
                    return;  
                }  
                //提交  
                $.ajax({  
                    type: "POST",  
                    async: false,  
                    url: "/PlanManagement/userToDeptManager/add.action?deptID="+deptID+"&userIDs=" + userID,
                    data: 'json',  
                    success: function (result) {  
                        if (result == "true") {  
                            $('#departmentUser_tab').datagrid('clearSelections');  
                            $.messager.alert("提示", "恭喜您，信息添加成功！", "info");  
                            $('#departmentUser_tab').datagrid('reload'); 
                            $('#user_tab').datagrid('reload'); 
                        } else {  
                            $.messager.alert("提示", "添加失败，请重新操作！", "info");  
                            return;  
                        }  
                    }  
                });  
            });  
        }  
         }
         /*删除用户群组中的成员*/
       function  removeDepartmentUser(){
        //返回选中多行  
        var selRow = $('#departmentUser_tab').datagrid('getSelections');  
        //判断是否选中行  
        if (selRow.length==0) {  
            $.messager.alert("提示", "请选择要删除的成员！", "info");  
            return;  
        }else{      
            var userID="";  
            //批量获取选中行的ID  
            for (i = 0; i < selRow.length;i++) {  
                if (userID =="") {  
                    userID = selRow[i].userID;  
                } else {  
                    userID = selRow[i].userID + "," + userID;  
                }                 
            }  
            $.messager.confirm('提示', '是否删除选中用户?', function (r) {  
                if (!r) {  
                    return;  
                }  
                //提交  
                $.ajax({  
                    type: "POST",  
                    async: false,  
                    url: "/PlanManagement/userToDeptManager/remove.action?userIDs=" + userID,  
                    data: 'json',  
                    success: function (result) {  
                        if (result == "true") {  
                            $('#departmentUser_tab').datagrid('clearSelections');  
                            $.messager.alert("提示", "恭喜您，信息删除成功！", "info");  
                            $('#departmentUser_tab').datagrid('reload');  
                        } else {  
                            $.messager.alert("提示", "删除失败，请重新操作！", "info");  
                            return;  
                        }  
                    }  
                });  
            });  
        }  
       }
    	
     </script> 
    <!--  <body class="easyui-layout" style="width:100%;min-width:1280px;height:100%;min-height:1024px;">  
      <div data-options="region:'north',split:false,collapsible:false"  style="height:360px;border:1px solid #CDB38B;">
        <table id="department_tab" border="0"></table >
        </div> 
		<div data-options="region:'center',split:false,collapsible:false"   style="padding:20px; height:100px;">
            <table cellspacing="0" border="0">  
                <tr>  
                    <td><a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" onclick="addDepartmentUser();">添加</a> &nbsp
                    <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-Remove" onclick="removeDepartmentUser();">删除</a>  &nbsp 
                 	</td>            
                </tr>  
            </table>  
		</div>
		<div data-options="region:'south' ,split:false,collapsible:false"  style="height:600px;border:1px solid #CDB38B;"  >
			 <table id="departmentUser_tab" border="0"></table>
		</div>
		  <div id="userTab" class="easyui-dialog" style="border:1px solid #CDB38B;width: 780px; height:400px; padding: 10px 20px; overflow:hidden;"  closed="true" buttons="#dlg-buttons" shadow="false"   >  
       <div id="tb" style="width: auto; height: 48px;">  
            <table style="width: auto; height: 48px;" cellspacing="0" border="0">  
                <tr>  
                    <td>&nbsp <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-ok" onclick="checkOk();">确认</a>  &nbsp
                      <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-Cancel" onclick="javascript:$('#userTab').dialog('close')">取消</a> 
                 	</td>            
                </tr>  
            </table>  
        </div>    
        	<table id="user_tab"  ></table>  
     </div>
    </body>   -->
     <body style="min-width:1280px;min-height:1024px;">
		<div class="wrap">
			<div class="left-right-middle1">
			
			<jsp:include page="/WEB-INF/views/main/head.jsp" flush="true" ></jsp:include>
				<div data-options="region:'north',split:false,collapsible:false"  style="height:360px;">
       		 <table  id="department_tab" ></table >
        		</div> 
        		<div data-options="region:'center',split:false,collapsible:false"   style="padding:20px; height:28px;">
            <table cellspacing="0" >  
                <tr>  
                    <td><a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" onclick="addDepartmentUser();">添加</a> &nbsp
                    <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" onclick="removeDepartmentUser();">删除</a>  &nbsp 
                 	</td>            
                </tr>  
            </table>  
				</div>
		<div data-options="region:'south' ,split:false,collapsible:false"  style="height:460px;"  >
			 <table id="departmentUser_tab" ></table>
		</div>
		 <div id="userTab" class="easyui-dialog" style="border:1px solid #D2D7D3;width: 780px; height:400px; padding: 10px 20px; overflow:hidden;"  closed="true" buttons="#dlg-buttons" shadow="false"   >  
       <div id="tb" style="width: auto; height: 48px;">  
            <table style="width: auto; height: 48px;" cellspacing="0" border="0">  
                <tr>  
                    <td>&nbsp <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-ok" onclick="checkOk();">确认</a>  &nbsp
                      <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#userTab').dialog('close')">取消</a> 
                 	</td>            
                </tr>  
            </table>  
        </div>    
        	<table id="user_tab" ></table>  
     </div>
		    </div>
		</div>
	</body>
</html>
