<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"  %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
 
  
  <script>
    function logout(){
                $.ajax({ 
				                    url: "",  // 后台地址
				                    type:"GET", 
				                    dataType:"json", 
				                    data:null,  //自己需要传递的数据 {}
				                    success: function(data){
                        //成功
                        //window.location.href ="xx.com"; 跳转页面
                        //或者处理其他注销后的逻辑

				                    },
				                    error:function(){
                       //出错
                    }
			                    }); 
    }
    $(".navbg").capacityFixed();
 
function js_winopen(url){
 //var newwin=window.open(url,'_blank','scrollbars=yes,left=0,top=0,width=800,height=800,channelmode=yes,menubar=yes');
// javascript:window.location.href(url);
         window.open(url,'_blank');  
}
</script>

<!-- 头部标题 -->
<div  style="height:108px; padding:5px;position:relative; z-index:88;" > 
	<div  style="height:40px;">
	<h1 >复杂装备多专业协同设计系统</h1>
	 
	 <span  class="loginInfo" style="float:left; padding:50px 50px;font-size:14px; " >【登录用户】${USER_INFO.userCode }&nbsp;&nbsp;姓名：${USER_INFO.userName }&nbsp;&nbsp;
     <a href="javascript:void(0)" style="font-size:14px;font-weight:bold;text-decoration: underline;color:#50a3a2;">修改密码</a> 
    <a href="javascript:void(0)" style="font-size:14px;font-weight:bold;text-decoration: underline;color:#50a3a2;" onclick="logout();" >注销</a> 
    </span>
	
    </div>
    <div class="navbg"  style="height:40px;position:relative; z-index:66; ">
	 <ul id="navul" class="cl" >
      <c:forEach items="${MAIN_MENU }" var="menus">
      	<c:if test="${menus.parentMenuID == 0 }">
      		<c:if test="${menus.viewUrl eq '0' and menus.menuName ne '系统管理'}">
      			<li>${menus.menuName }
      			<ul>
      				<c:forEach items="${MAIN_MENU }" var="menu">
      					<c:if test="${menu.parentMenuID == menus.menuID }">
      						<li><a class="link"   onclick="js_winopen('<%=basePath%>${menu.viewUrl }')"> ${menu.menuName }</a></li>
      					</c:if>
      				</c:forEach>
      				</ul>
      			</li>
      		</c:if>
      		<c:if test="${menus.viewUrl ne '0' }">
      			<li><a class="link"   onclick="js_winopen('<%=basePath%>${menus.viewUrl }')">${menus.menuName }</a></li>
      		</c:if>
      	</c:if>
      </c:forEach>
      <c:forEach items="${MAIN_MENU }" var="menu2">
      	<c:if test="${menu2.menuName eq '系统管理' and USER_INFO.roleID == 1}">
      		<li>${menu2.menuName }
      			<ul>
      				<c:forEach items="${MAIN_MENU }" var="menu3">
      					<c:if test="${menu3.parentMenuID == menu2.menuID }">
      						<li><a class="link"   onclick="js_winopen('<%=basePath%>${menu3.viewUrl }')"> ${menu3.menuName }</a></li>
      					</c:if>
      				</c:forEach>
      				</ul>
      			</li>
      	</c:if>
      </c:forEach>
    </ul>
    </div>
</div>
