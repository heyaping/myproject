<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 5.01 Transitional//EN">
<html>
<head>
<meta charset="utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
<title>系统登陆</title>
<meta name="author" content="DeathGhost" />
<link rel="stylesheet" href="STATIC/css/styles.css" type="text/css"></link>
<style>
body{height:100%;background:#16a085;overflow:hidden;}
canvas{z-index:-1;position:absolute;}
</style>
<script type="text/javascript" src="STATIC/js/jquery-easyui-1.3.5/jquery-2.1.1.min.js"></script>
	<script type="text/javascript" src="STATIC/js/jquery-easyui-1.3.5/jquery.easyui.min.js"></script>

</head>

<script type="text/javascript"> 
$("#login-button").click(function (event) {
	event.preventDefault();
	$('form').fadeOut(0);
	$('.wrapper').addClass('form-success');
});


</script>
<body>
<div class="htmleaf-container">
	<div class="wrapper">
		<div class="container">
			<h1>复杂装备多专业协同设计系统</h1>
			
			<form class="form" action="<%=basePath%>main.action" method="post" >
				<input type="text" placeholder="Username" name="userCode" id="userCode">
				<input type="password" placeholder="Password" name="password">
				<button type="submit" id="login-button" >Login</button>
			</form>
		</div>
		
		<ul class="bg-bubbles">
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
		</ul>
	</div>
</div>
</body>
</html>
