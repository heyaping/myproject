<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"  %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<html>
  <head>
    <base href="<%=basePath%>">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>主界面</title>
	<link rel="stylesheet" type="text/css" href="STATIC/css/default.css">
	<link rel="stylesheet" type="text/css" href="STATIC/js/jquery-easyui-1.3.5/themes/gray/easyui.css">
	<link rel="stylesheet" type="text/css" href="STATIC/js/jquery-easyui-1.3.5/themes/icon.css" />
	<link rel="stylesheet" type="text/css" href="STATIC/js/JQuery-zTree-v3.5.15/css/zTreeStyle/zTreeStyle.css">
	<link href="STATIC/css/main.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="STATIC/css/mainstyles.css" type="text/css"></link>
	<script type="text/javascript" src="STATIC/js/jquery.min.js"></script>
	<script type="text/javascript" src="STATIC/js/work.js"></script>
	<script type="text/javascript" src="STATIC/js/jquery-easyui-1.3.5/jquery-1.10.2.min.js"></script>
	<script type="text/javascript" src="STATIC/js/jquery-easyui-1.3.5/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="STATIC/js/JQuery-zTree-v3.5.15/jquery.ztree.all-3.5.min.js"></script>
	<script type="text/javascript" src="STATIC/js/index.js"></script>
	<script type="text/javascript" src="STATIC/js/extends.js"></script>
	<script type="text/javascript" src="STATIC/js/common.js"></script>
	   <style type="text/css">
html,body{
margin:0;
padding:0;
height:100%;
position:relative;
}
.wrap{
width:100%;
height:100%;
margin:0px;
background-color:#F1F3F7;

}
.left-right-middle1{
width:100%;
height:100%;
background-color:#F1F3F7;
margin:0 auto;
}
	</style>
  </head>
  
<script  type="text/javascript"> 
$(".navbg").capacityFixed();
 
function js_winopen(url){
 //var newwin=window.open(url,'_blank','scrollbars=yes,left=0,top=0,width=800,height=800,channelmode=yes,menubar=yes');
// javascript:window.location.href(url);
         window.open(url,'_blank');  
}
</script>
  

<body style="min-width:1280px;min-height:1024px;">
		<div class="wrap">
		<div class="left-right-middle1">
		
<!-- 头部标题 -->
<%-- <div data-options="region:'north',border:false" style="height:66px; padding:5px;  background:#F5F5F5; " > 
	<div  style="height:40px; ">
	<h1>复杂装备多专业协同设计系统</h1>
    <span  class="loginInfo" style="float:left; padding:10px 20px;font-size:14px; " >【登录用户】${USER_INFO.userCode }&nbsp;&nbsp;姓名：${USER_INFO.userName }&nbsp;&nbsp;
     <a href="javascript:void(0)" style="font:14px;font-weight:bold;text-decoration: underline;color:Blue;">修改密码</a> 
    <a href="javascript:void(0)" style="font:14px;font-weight:bold;text-decoration: underline;color:Blue;" onclick="logout();" >注销</a> 
    </span>
    </div>
</div> --%>
<!-- 页脚信息 -->
<jsp:include page="head.jsp"></jsp:include>
<div id="center"  style="height:788px;width:100%; position:relative; z-index:22;"> 
<div  style="height:922px;" class="main">
<ul class="bg-bubbles">
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
		</ul>
</div>

</div> 

			</div>
		</div>
	</body> 
</html>
