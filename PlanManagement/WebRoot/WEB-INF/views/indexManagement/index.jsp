<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"  %>
<%@page import="java.text.SimpleDateFormat"%> 
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"> <!-- HTML5 document type -->
<html>
  <head>
    <base href="<%=basePath%>">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>指标信息</title>
	<link rel="stylesheet" type="text/css" href="STATIC/css/default.css">
	<link rel="stylesheet" type="text/css" href="STATIC/js/jquery-easyui-1.3.5/themes/gray/easyui.css">
	<link rel="stylesheet" type="text/css" href="STATIC/js/jquery-easyui-1.3.5/themes/icon.css" />
	<link href="STATIC/css/main.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="STATIC/css/mainstyles.css" type="text/css"></link>
	<script type="text/javascript" src="STATIC/js/jquery.min.js"></script>
	<script type="text/javascript" src="STATIC/js/work.js"></script>
	<script type="text/javascript" src="STATIC/js/jquery-easyui-1.3.5/jquery-1.10.2.min.js"></script>
	<script type="text/javascript" src="STATIC/js/jquery-easyui-1.3.5/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="STATIC/js/JQuery-zTree-v3.5.15/jquery.ztree.all-3.5.min.js"></script>
	<script type="text/javascript" src="STATIC/js/index.js"></script>
	<script type="text/javascript" src="STATIC/js/extends.js"></script>
	<script type="text/javascript" src="STATIC/js/common.js"></script>
	<script src="STATIC/js/gojs/go.src.js"></script>
  </head>

  <script type="text/javascript">  
  	var url = "";
  	var json='';
  	var exunll="";
  	var bs=0;
  	var thisemp="";
  	var PrductID=0;
    /* 启动时加载 */  
    $(function(){  
    initButton();
         /*加载型号-方案*/
          $("#select_Model").combobox({editable:false});
          $('#select_Model').combobox({      
           url:'/PlanManagement/TargetbomManager/getDesignxh.action',      
            valueField:'modelID', 
            panelHeight:'auto',     
            textField:'modelName',
            onLoadSuccess: function () {
                $(this).combobox('setText', '--请选择--');
                
            },
             onChange:function(modelID){
		    	//$('#city').combobox('clear');
		    	$('#select_Project').combobox({
			    valueField:'taskID', //值字段
			    textField:'taskName', //显示的字段
			    url:'/PlanManagement/TargetbomManager/getDesignfa.action?modelID='+modelID,
			    panelHeight:'auto',
			    value:'--请选择--',
		 	});
		    }
            });
           $('#select_indexTree').combobox({      
           //url:'/PlanManagement/TargetbomManager/getDesignxh.action',      
            valueField:'', 
            panelHeight:'auto',     
            textField:'',
            onLoadSuccess: function () {
                $(this).combobox('setText', '--请选择--');
                
            },
            });
          $("#select_Project").combobox({editable:false});
      		$('#select_Project').combobox({
		    valueField:'taskID', //值字段
		    textField:'taskName', //显示的字段
		    url:'/PlanManagement/TargetbomManager/getDesignfa.action?modelID=${Designfa.modelID}',
		    panelHeight:'auto',
		    value:'${Designfa.modelID}',
		    onChange:function(taskID){
		       //提交  
                $.ajax({  
                    type: "POST",  
                    async: false,  
                    url: "/PlanManagement/TargetbomManager/getjson.action?taskID=" + taskID,  
                    data: 'string',  
                    success: function (str) {  
                    json=str;
                    getPrductID();
	         	 $("#products_targetboms").tree({
			   	url: '/PlanManagement/designfaManager/proTree.action?productID='+PrductID,
			   	onLoadSuccess:function(node,data){
				var t = $(this);
				t.tree('expandAll');
				},
				onClick: function(node){
						 	 $("#target_tab").datagrid({  
			            checkOnSelect: false,  
			            fitColumns :true, 
			            checkOnSelect:true,
			            singleSelect: true,
			            selectOnCheck: true,
			            nowrap:true, 
			            rowStyler:function(index,row){
								if (index%2==1){
										return 'background-color:#D2D7D3;';
									}
									 else{
									return 'background-color:#50a3a2;';
								} 
							},
			            url: '/PlanManagement/TargetbomManager/selectProduct_tagert.action?productID=' + node.id, 
			            loadMsg:'加载中...',  
			            fit: true,  
			            columns: [[  
			                { field: 'targetID', title: '编号', width:50},  
			                { field: 'productName', title: '产品名称', width:100},  
			                { field: 'targetName', title: '指标名称', width:100},
			                { field: 'targeUpperLimit', title: '上限值', width:160},  
			                { field: 'targeLowLimit', title: '下限值', width:160},  
			                { field: 'targeFactVal', title: '当前值', width:100},  
			                { field: 'unit', title: '单位', width:100},
			                { field: 'unitName', title: '单位名称', width:100},
			                { field: 'remark', title: '备注', width:150},  
			            ]]
				        });
								}
						}); 
			$("#targetbom_tab").datagrid({  
            checkOnSelect: false,  
            fitColumns :true, 
            checkOnSelect:true,
            singleSelect: false,
            selectOnCheck: true,
                 rowStyler:function(index,row){
			if (index%2==1){
				return 'background-color:#D2D7D3;';
			}
			 else{
			return 'background-color:#50a3a2;';
			} 
		},
            nowrap:true, 
            url: '/PlanManagement/TargetbomManager/selectTargetbom.action',
            queryParams:{TargetbomID:'0'}, 
            loadMsg:'加载中...',  
            fit: true,  
            columns: [[  
                { field: 'targetID', title: '编号', width:50},  
                { field: 'targetName', title: '指标名称', width:100},  
                { field: 'productName', title: '产品名称', width:100},
                { field: 'productID', title: '产品编号', width:100 },  
                { field: 'targeUpperLimit', title: '上限值', width:160},  
                { field: 'targeLowLimit', title: '下限值', width:160},  
                { field: 'targeFactVal', title: '当前值', width:100},  
                { field: 'unit', title: '单位', width:100},
                { field: 'unitName', title: '单位名称', width:100},
                { field: 'remark', title: '备注', width:150},  
                { field: 'director', title: '负责人', width:150},
                { field: 'targetType', title: '指标类型', width:150},
                { field: 'dataType', title: '数据类型', width:150},
                { field: 'directorName', title: '负责人名字', width:150},
            ]]
        });
          $("#targetbom_tab").datagrid("hideColumn", "productID"); // 设置隐藏列   
          $("#targetbom_tab").datagrid("hideColumn", "unit"); // 设置隐藏列  
          $("#targetbom_tab").datagrid("hideColumn", "director"); // 设置隐藏列  
          $("#targetbom_tab").datagrid("hideColumn", "targetType"); // 设置隐藏列  
          $("#targetbom_tab").datagrid("hideColumn", "dataType"); // 设置隐藏列  
           $("#targetbom_tab").datagrid("hideColumn", "directorName"); // 设置隐藏列  
						
						
				                    }
                      
                });  
		    //请求后台传给前台json数据
                  if(json==""){
                  json="[]";
                  enaButton();
                  $("#removeTargetbom").linkbutton("disable");
    			  $("#editTargetbom").linkbutton("disable");
                  $.messager.alert("提示", "当前方案下没有指标树,请创建！", "info");
                  exunll=0;
                  }else
                  { exunll=1;
                  }
                  var obj = eval('(' + json + ')');
               myDiagram.model = new go.TreeModel(obj); 
		    }
		 });
		 
		 
      }) ;
         //置灰按钮
         function initButton(){
      	 $("#addTargetbom").linkbutton("disable");
      	 $("#removeTargetbom").linkbutton("disable");
    	 $("#editTargetbom").linkbutton("disable");
         }
         //启用按钮
          function enaButton(){
      	 $("#addTargetbom").linkbutton("enable");
      	 $("#removeTargetbom").linkbutton("enable");
    	 $("#editTargetbom").linkbutton("enable");
         }
		 function init() {
		    var $ = go.GraphObject.make;  // for conciseness in defining templates
		    myDiagram =
		      $(go.Diagram, "myDiagramDiv",
        {
          isReadOnly: false, 
          
          initialContentAlignment: go.Spot.Left,
          // define the layout for the diagram
          layout: $(go.TreeLayout, { nodeSpacing: 20, layerSpacing: 40 })
        });
    // Define a simple node template consisting of text followed by an expand/collapse button
    myDiagram.nodeTemplate =
      $(go.Node, "Horizontal",
        { selectionChanged: nodeSelectionChanged },  // this event handler is defined below
        $(go.Panel, "Table",{height:28},
        { defaultAlignment: go.Spot.Left},
         $(go.RowColumnDefinition, { column: 0, separatorStroke: "#1F4963" }),
          $(go.RowColumnDefinition, { column: 1, separatorStroke: "white"},new go.Binding("background", "color")),
           $(go.RowColumnDefinition, { row: 0, separatorStroke: "#1F4963" }),
          $(go.Shape, "Rectangle",  
          { fill: "#1F4963", stroke: null ,width:188}
           ,
            new go.Binding("width","index",function(i){
            return parseInt(i)>0 ? 136  : 188;
            })
          ),
          $(go.TextBlock,
            { row: 0, column: 0},
            { font: "bold 13px Helvetica, bold Arial, sans-serif",
              stroke: "white", margin:10 ,width: 85,
              isMultiline:false,textAlign:'left',
               },
            new go.Binding("text", "key")
            ),
              $(go.TextBlock,
            { row: 0, column: 1},
            { font: "bold 13px Helvetica, bold Arial, sans-serif",
              stroke: "white", margin: 1 ,width: 52,
           		isMultiline:false ,visible:false,textAlign:'left',
               },
            new go.Binding("text", "index"),
            new go.Binding("visible","index",function(i){
            return parseInt(i)>0 ? true  : false;
            })
            )
            ),
        $("TreeExpanderButton")
      );
    // Define a trivial link template with no arrowhead.
    myDiagram.linkTemplate =
      $(go.Link,
        { selectable: false,
         /*   reshapable: true,
          routing: go.Link.AvoidsNodes,
          corner: 1,
          curve: go.Link.JumpOver  */
        
        },
        
         $(go.Shape));  //  the link shape

 	 var nodeDataArray = [];
 	  myDiagram.model = new go.TreeModel(nodeDataArray); 
  		}
	  function nodeSelectionChanged(node) {
	  enaButton();
	    if (node.isSelected) {
	    //  names[node.data.name].style.backgroundColor = "lightblue";
	    var targetbomID=node.data.id;
	    thisemp=node.data;
	   $("#targetbom_tab").datagrid('reload',{TargetbomID:targetbomID}); 
	    } else {
	    initButton();
	    //  names[node.data.name].style.backgroundColor = "";
      $('#targetbom_tab').datagrid('loadData',{total:0,rows:[]});
	    }
	  }
		var s='';
		var n='';
  	   /* 添加指标信息 */  
   	 function addIdex(){ 
   	   bs=0; 
       for(var nit=myDiagram.nodes;nit.next();){
       var node=nit.value;
       if(!node.isSelected) continue;
       s= node.data.key;
       n=node.data.id;
       if(n==null){
       }
       }
       if(s!=""){
		//myDiagram.model.addNodeData({ key: "大大儿子111", parent: s,index:"888"});
		$("#enditTab").dialog("open").dialog('setTitle', '添加指标节点');    
        $("#enditTab").panel("move",{top:$(document).scrollTop() + ($(window).height()-250) * 0.5}); 
        selectAll();
        $("#fm").form("clear");
		}else{
		if(json=="[]"){
		$("#enditTab").dialog("open").dialog('setTitle', '添加指标节点');    
        $("#enditTab").panel("move",{top:$(document).scrollTop() + ($(window).height()-250) * 0.5}); 
        selectAll();
        $("#fm").form("clear");
        }else{
		$.messager.alert("提示", "父节点不能为空！", "info");
		}
		}
		
     }  
     /*删除指标*/
      function removeIdex (){
        var selectedNode = '';
      	for(var nit=myDiagram.nodes;nit.next();){
       		var node=nit.value;
       if(!node.isSelected) continue;
      	 s= node.data.key;
      	 n=node.data.id;
       if(node.isSelected)
       	selectedNode=node.data.parent;
		}
		var a=0;
		for(var nit=myDiagram.nodes;nit.next();){
			var node=nit.value;
		if(node.isSelected)continue;
		if(s==node.data.parent){
			a++;
			continue;
		}
		}
		if(a>0){
		$.messager.alert("提示", "当前节点有子节点不能删除！", "info");
		}else{
		 if (myDiagram.commandHandler.canDeleteSelection()) {
		  //myDiagram.commandHandler.deleteSelection();
		  $.messager.confirm('提示框', '你确定要删除吗?',function(r){
		  if(r){
		   $.ajax({  
                    type: "POST",  
                    async: false,  
                    url: "/PlanManagement/TargetbomManager/deleteTargetbom.action?TargetbomID=" + n,  
                    data: 'string',  
                    success: function (str) {  
                    if(str=="true"){
                    $.messager.alert("提示", "删除成功！", "info");
                    myDiagram.commandHandler.deleteSelection();
                    }else{
                     $.messager.alert("提示", "删除失败！", "info");
                    }
                    }  
                }); 
		  }else{
		  
		  }
		  
		  });
		 }
		return;
		}
		}
		
		function selectAll(){
		$("#selectTargetTpye").combobox({editable:false});
          $('#selectTargetTpye').combobox({      
            url:'/PlanManagement/TargetbomManager/getTargetType.action',      
            valueField:'parameterID', 
            panelHeight:'auto',     
            textField:'parameterUnit',
            onLoadSuccess: function (date) {
            if(bs==1){
                }else{
                 $(this).combobox('setText', '--请选择--');
                }
            },
          
            });
            $("#dataType").combobox({editable:false});
         	$('#dataType').combobox({      
          	url:'/PlanManagement/TargetbomManager/selectDateType.action',      
            valueField:'parameterID', 
            panelHeight:'auto',     
            textField:'parameterUnit',
            onLoadSuccess: function (date) {
            if(bs==1){
                }else{
                 $(this).combobox('setText', '--请选择--');
                }
                
            }, onSelect:function(){
            var type= $("#dataType").combobox("getText");
            if(type=="描述型"){
            $('#targeLowLimit').val("");
            $('#targeUpperLimit').val("");
            $('#targeFactVal').val("");
        	$('#targeLowLimit').attr('disabled',true);
        	$('#targeUpperLimit').attr('disabled',true);
        	$('#targeFactVal').attr('disabled',true);
            }else{
            $('#targeLowLimit').attr('disabled',false);
        	$('#targeUpperLimit').attr('disabled',false);
        	$('#targeFactVal').attr('disabled',false);
            }
            
		    }
          
            });
            $("#selectCompany").combobox({editable:false});
          $('#selectCompany').combobox({      
           url:'/PlanManagement/TargetbomManager/selectUnit.action',      
            valueField:'parameterID', 
            panelHeight:'auto',     
            textField:'parameterUnit',
            onLoadSuccess: function (date) {
     		  if(bs==1){
                }else{
                 $(this).combobox('setText', '--请选择--');
                }
            },
          
            });
		
		}
		 function checkProduct(){
		 $("#productTab").dialog("open").dialog('setTitle', '选择产品信息');    
         $("#productTab").panel("move",{top:$(document).scrollTop() + ($(window).height()-250) * 0.5});
         getPrductID();
         initTree(PrductID);
		 }
		 
		 
		 function initTree(productID){
         $("#products").tree({
		    url: '/PlanManagement/designfaManager/proTree.action?productID='+productID,
		    onLoadSuccess:function(node,data){
			var t = $(this);
			t.tree('expandAll');
			},
		    onSelect:function(node){
		    	if($(this).tree("getParent",node.target)){
				parentNodeName = $(this).tree("getParent",node.target).text;
				parentNodeID = $(this).tree("getParent",node.target).id;
		    	}else{
		    	parentNodeName="";
		    	parentNodeID = "";
		    	}
				nodeName = node.text;
				nodeID = node.id;
		    },
		}); 
      }
      function getPrductID(){
          var taskID= $("#select_Project").combobox('getValue');
           $.ajax({  
                    type: "POST",  
                    async: false,  
                    url: "/PlanManagement/TargetbomManager/getPrductID.action?taskID=" + taskID,  
                    data: 'int',  
                    success: function (str) {  
                    PrductID=str;
                    }  
                }); 
      }
      function checkdt(){
      $("#department_tab").datagrid({  
            checkOnSelect: false,  
            fitColumns :true, 
            checkOnSelect:true,
            singleSelect: true,
            selectOnCheck: false,
            nowrap:true, 
            url: '/PlanManagement/deptManager/search.action',  
            loadMsg:'加载中...',  
            fit: true,
            rowStyler:function(index,row){
			if (index%2==1){
				return 'background-color:#D2D7D3;';
			}
			 else{
			return 'background-color:#50a3a2;';
			} 
		},
            //当用户点击当前行触发
            onClickRow:function(rowIndex,rowData){
            deptID = rowData.deptID;
   				$("#departmentUser_tab").datagrid({  
            checkOnSelect: false,  
            fitColumns :true, 
            checkOnSelect:true,
            singleSelect: false,
            selectOnCheck: true,
            nowrap:true, 
            url: '/PlanManagement/userToDeptManager/searchByDept.action?deptID='+deptID,  
            loadMsg:'加载中...',  
            fit: true,  
            rowStyler:function(index,row){
			if (index%2==1){
				return 'background-color:#D2D7D3;';
			}
			 else{
			return 'background-color:#50a3a2;';
			} 
		},
            columns: [[  
                { field: 'userID', checkbox: true},  
                { field: 'userCode', title: '用户帐号', width:100},  
                { field: 'userName', title: '用户姓名', width:100},  
                { field: 'sex', title: '性别', width:100},  
                { field: 'tel', title: '电话', width:160},  
                { field: 'email', title: '电子邮件', width:160},  
                { field: 'jobName', title: '职务', width:100},  
                { field: 'roleID', title: '系统角色', width:100,
                formatter:function(value,row,index){if(value=='1'){return'系统管理员';}if(value=='2'){return'管理者';}if(value=='3'){return'普通用户';}}},  
            ]]            
        });
  					},  
            columns: [[  
                { field: 'deptID', title: '群组编号', width:100},  
                { field: 'deptCode', title: '群组代号', width:100},  
                { field: 'deptShortName', title: '群组简称', width:100},  
                { field: 'deptLongName', title: '群组全称', width:160},  
                { field: 'leaderName', title: '负责人', width:160},  
                { field: 'memberNum', title: '成员数', width:100},  
                { field: 'createDT', title: '创建时间', width:150},  
                { field: 'modifyDT', title: '修改时间', width:150 },  
            ]]   
             });
          	$("#departmentUser_tab").datagrid({  
            checkOnSelect: false,  
            fitColumns :true, 
            checkOnSelect:true,
            singleSelect: false,
            selectOnCheck: true,
            nowrap:true, 
            //url: '/PlanManagement/userManager/search.action',  
            loadMsg:'加载中...',  
            fit: true,
            rowStyler:function(index,row){
			if (index%2==1){
				return 'background-color:#D2D7D3;';
			}
			 else{
			return 'background-color:#50a3a2;';
			} 
		},  
            columns: [[  
                { field: 'userID', checkbox: true},  
                { field: 'userCode', title: '用户帐号', width:100},  
                { field: 'userName', title: '用户姓名', width:100},  
                { field: 'sex', title: '性别', width:100},  
                { field: 'tel', title: '电话', width:160},  
                { field: 'email', title: '电子邮件', width:160},  
                { field: 'jobName', title: '职务', width:100},  
                { field: 'roleID', title: '系统角色', width:100,
                formatter:function(value,row,index){if(value=='1'){return'系统管理员';}if(value=='2'){return'管理者';}if(value=='3'){return'普通用户';}}},  
            ]]            
        });
         
      $("#dtTab").dialog("open").dialog('setTitle', '选择单个用户信息');    
      $("#dtTab").panel("move",{top:$(document).scrollTop() + ($(window).height()-250) * 0.5});
      $('#department_tab').datagrid('clearSelections');
      $('#department_tab').datagrid('load');
      }
      function clickProduct(){
      var node = $("#products").tree("getSelected");
      $('#productTab').dialog('close');
      $("#productName").val(node.text);
       $("#productID").val(node.id);
      }
      function clickdt(){
       var row = $("#departmentUser_tab").datagrid("getSelected");  
         $('#directorName').val(row.userName);
         $('#director').val(row.userID);
         $('#dtTab').dialog('close');
         $("#parentTargetID").val(n);
      }
      /*指标添加*/
      var factval=0;
       var targeUpperLimit=0;
       var targeLowLimit=0;
       function checkTbAdd(){
        factval= $('#targeFactVal').val();
           targeUpperLimit= $('#targeUpperLimit').val();
             targeLowLimit= $('#targeLowLimit').val();
        	var taskID= $("#select_Project").combobox('getValue');
        	      var tID=$("#targetID").val();
        	      var tName=$("#targetName").val();
        	      if(tID==null||tID==""){
                 for(var nit=myDiagram.nodes;nit.next();){
       				var node=nit.value;
       				 if(node.data.key==tName){
       				 $.messager.alert("提示信息", "名称不能相同"); 
       				 return false;
       				 }
       				}
       				}
       				if(parseInt(targeUpperLimit)<parseInt(targeLowLimit)){
       				 $.messager.alert("提示信息", "上限值不能小于下限值");
       				 return false;
       				}
       				var dataType= $("#dataType").combobox('getValue');
       				if(dataType==""){
       				$.messager.alert("提示信息","数据类型不能为空");
       				return false;
       				} 
    	   $("#fm").form("submit", {  
            url: '/PlanManagement/TargetbomManager/addTargetbom.action?exnull='+exunll+'&taskID='+taskID,  
             onsubmit: function () {  
                return $(this).form("validate"); 
            },  
            success: function (result) { 
                if (result == "update") {  
                 //myDiagram.model.addNodeData({ key: $('#targetName').val(), parent:s,index:index,color:"red",id:result});
                	$.messager.alert("提示信息", "操作成功");
                	//修改节点信息
                   var color="";
                   var index=""+$('#targeFactVal').val();
                  
                   var dataType= $('#dataType').combobox("getText");
                   if(parseInt(factval)<parseInt(targeLowLimit)){
                   color="#8F1D21";
                   }
                   if(parseInt(factval)>parseInt(targeUpperLimit)){
                   color="#F7CA18";
                   }
                   if(parseInt(factval)<=parseInt(targeUpperLimit)&&parseInt(factval)>=parseInt(targeLowLimit)){
                   color="#407A52";
                   }if(dataType=="描述型"){
                   color="#1F4963";
                   }
                 myDiagram.model.setDataProperty(thisemp, "key", $('#targetName').val());
                 myDiagram.model.setDataProperty(thisemp, "index", $('#targeFactVal').val());
                 myDiagram.model.setDataProperty(thisemp, "color", color);
                  $("#fm").form("clear"); 
                    $('#enditTab').dialog('close');
                      $('#targetbom_tab').datagrid('load');
                }else if(result != "false"){
                     $.messager.alert("提示信息", "操作成功"); 
                    exunll = 1;
                   var color="";
                   var index=""+$('#targeFactVal').val();
                   var dataType= $('#dataType').combobox("getText");
                  if(parseInt(factval)<parseInt(targeLowLimit)){
                   color="#8F1D21";
                   }
                   if(parseInt(factval)>parseInt(targeUpperLimit)){
                   color="#F7CA18";
                   }
                   if(parseInt(factval)<=parseInt(targeUpperLimit)&&parseInt(factval)>=parseInt(targeLowLimit)){
                   color="#407A52";
                   }if(dataType=="描述型"){
                   color="#1F4963";
                   }
                       myDiagram.model.addNodeData({ key: $('#targetName').val(), parent:s,index:index,color:color,id:result});
                    $("#fm").form("clear"); 
                    $('#enditTab').dialog('close');
                }
                else {  
                    $.messager.alert("提示信息", "保存数据失败");  
                }  
            }  
        });
    	}
    	/*修改指标*/
    	function editIdex(){
    	bs=1;
    	$("#enditTab").dialog("open").dialog('setTitle', '修改指标节点');    
        $("#enditTab").panel("move",{top:$(document).scrollTop() + ($(window).height()-250) * 0.5}); 
        selectAll();
        var row = $("#targetbom_tab").datagrid("getData").rows[0];
        $('#targetID').val(row.targetID);
        $('#targetName').val(row.targetName);
    	$('#productName').val(row.productName);
    	$('#productID').val(row.productID);
    	$('#directorName').val(row.directorName);
    	$('#selectTargetTpye').combobox("select", row.targetType);
    	$('#dataType').combobox("select", row.dataType);
    	if(row.dataType==6){
    	$('#targeLowLimit').attr('disabled',true);
        	$('#targeUpperLimit').attr('disabled',true);
        	$('#targeFactVal').attr('disabled',true);
    	}
    	$('#selectCompany').combobox("select", row.unit);
    	$('#targeUpperLimit').val(row.targeUpperLimit);
    	$('#targeLowLimit').val(row.targeLowLimit);
    	$('#targeFactVal').val(row.targeFactVal);
    	$('#remark').val(row.remark);
    	}
     </script> 
      <body style="min-width:1280px;min-height:1024px;" id="layout" onload="init()">
		<div class="wrap">
			<div class="left-right-middle1">
			<jsp:include page="/WEB-INF/views/main/head.jsp" flush="true" ></jsp:include>
				<div class="easyui-layout"  style="min-width:1280px;min-height:908px;">
				    <div data-options="region:'north',title:'指标信息管理',split:false,collapsible:false"   style="height:78px;background-color: #F1F3F7">
         <table style=" height: 48px;" cellspacing="0" border="0">  
                <tr>  
                    <td> &nbsp 型号</td>  
                    <td><input id="select_Model" name="xh"  style="width: 180px;height: 26px;"></td> 
                    <td> &nbsp 方案</td>  
                    <td><input id="select_Project" name="fa"  style="width: 180px;height: 26px;"></td> 
                    <td> &nbsp 指标树</td>
                    <td><input id="select_indexTree" name="tree"  style="width: 180px;height: 26px;"></td>  
                </tr>  
          </table>  
        </div> 
		<div data-options="region:'center',split:false,collapsible:false"   style="padding:8px; height:300px;  background-color: #F1F3F7">
           <div class="easyui-tabs" style="width: auto; height:800px;" id="channelFeeTab"   >  
        <div  title="产品指标树" style="padding:10px; overflow:hidden;width:auto;height:500px;background-color: #F1F3F7"  >  
         <div class="easyui-layout"  style="width:1280x;height: 700px">
       <div data-options="region:'north'  ,collapsible:false"  style="height:80px ;background-color: #F1F3F7"  >
        <table style="width: auto; height: 50px;;padding:10px" cellspacing="0"   >  
                <tr>  
                    <td>
                    &nbsp;<a href="javascript:void(0)" id="addTargetbom" class="easyui-linkbutton" iconCls="icon-add" onclick="addIdex();">增加指标</a> 
                    &nbsp;<a href="javascript:void(0)" id="removeTargetbom" class="easyui-linkbutton" iconCls="icon-Remove" onclick="removeIdex();">删除指标</a> 
                    &nbsp;<a href="javascript:void(0)" id="editTargetbom"class="easyui-linkbutton" iconCls="icon-edit" onclick="editIdex();">编辑指标</a>
                <!--     &nbsp;<a href="javascript:void(0)" id="bcTargetbom"class="easyui-linkbutton" iconCls="icon-save" onclick="editUser();">保存布局</a> -->
                    </td>             
                </tr> 
                <tr>
                <td>
                <p style="color:#50a3a2;font-family:楷体;font-size:18px;font-weight:bolder"> &nbsp;<img src="STATIC/img/Red_Icon.png" height="16px" width="16px"></img>指标异常 &nbsp <img src="STATIC/img/Blue_Icon.png" height="16px" width="16px"></img>指标正常 &nbsp <img src="STATIC/img/Yellow_Icon.png" height="16px" width="16px"></img>指标有风险</p>
                </td>
                </tr> 
          </table>  
          </div>
       <div data-options="region:'center' ,split:false,collapsible:false" style="height:300px; background-color: #F1F3F7" id="myDiagramDiv"   >
      </div>
  		<div data-options="region:'south' ,split:false,collapsible:false"  style="height:68px;background-color: #F1F3F7"  >
			<table id="targetbom_tab" ></table>
		</div>
        </div>  
        <div id="enditTab" class="easyui-dialog" style="border:1px solid #D2D7D3;width: 680px; height: auto; padding: 10px 20px;" closed="true" buttons="#dlg-buttons" shadow="false" >    
        <form id="fm" method="post" >  
            <table border="0" style="width:98%;">  
                <input type="hidden"  id="targetID" name="targetID"  value=""  style="width: 180px;height: 20px;">  
                <tr style="height: 35px;">  
                    <td>指标编号</td>  
                    <td colspan="2"><input id="" name="targetID" readonly="readonly" style="width: 180px;height: 26px;"></td>  
                    <td>指标名称<span style="color:#ff0000">*</span></td>  
                    <td colspan="2"><input  id="targetName" name="targetName" missingMessage="指标名称不能为空" class="easyui-validatebox" data-options="required:true" value=""  style="width: 180px;height: 26px;"></td>  
                </tr>  
               <tr style="height: 35px;">  
                    <td>所属产品<span style="color:#ff0000">*</span></td>  
                    <td><input id="productName" name="productName" class="easyui-validatebox" missingMessage="所属产品不能为空" data-options="required:true" readonly="readonly" style="width: 180px;height: 26px;"> 
                    <input id= "productID" name="productID" type="hidden" >
                    <input id= "parentTargetID" name="parentTargetID" type="hidden" >
                    </td>
                    <td ><a href="javascript:void(0)" class="easyui-linkbutton" plain="true" iconCls="icon-Search" style="float: left"  onclick="checkProduct();"   ></a></td>
                    <td>负责人<span style="color:#ff0000">*</span></td>  
                    <td ><input  id="directorName"  name="directorName" readonly="readonly" missingMessage="负责人不能为空" value="" class="easyui-validatebox" data-options="required:true" style="width: 180px;height: 26px;">
                    <input id= "director" name="director" type="hidden" >
                   </td>  
                   <td><a href="javascript:void(0)" class="easyui-linkbutton" plain="true" iconCls="icon-Search" style="float: left"  onclick="checkdt();"   ></a></td>
                </tr>
                <tr style="height: 35px;">  
                    <td>指标类型</td>  
                    <td colspan="2"><input  id="selectTargetTpye" name="targetType"  value=""  style="width: 180px;height: 26px;">
					</td>  
                    <td>数据类型<span style="color:#ff0000">*</span></td>  
                    <td colspan="2"><input  id="dataType" name="dataType"  value=""  style="width: 180px;height: 26px;"></td>  
                </tr>
                <tr style="height: 35px;">  
                    <td>目标上限</td>  
                    <td colspan="2"><input id="targeUpperLimit" name="targeUpperLimit" style="width: 180px;height: 26px;"></td>  
                    <td>目标下限</td>  
                    <td colspan="2"><input  id="targeLowLimit" name="targeLowLimit"  value=""  style="width: 180px;height: 26px;"></td>  
                </tr>
                <tr style="height: 35px;">  
                    <td>当前实际</td>  
                 	 <td colspan="2"><input  id=targeFactVal name="targeFactVal"  value=""  style="width: 180px;height: 26px;"></td>   
                    <td>单         位</td>  
                    <td colspan="2"><input  id="selectCompany" name="unit"  value=""  style="width: 180px;height: 26px;"></td>  
                </tr>
                <tr style="height: 35px;">  
                    <td >备注信息</td>  
                    <td colspan="6"> <textarea id="remark" name="remark" value="" style="resize:none; width:485px; height:50px;"></textarea></td>  
                </tr>
                <tr style="height: 50px;">  
                    <td colspan="6" align="center">  
                        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-ok" onclick="checkTbAdd();">确认</a>      
                        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-Cancel" onclick="javascript:$('#enditTab').dialog('close')">取消</a>  
                    </td>  
                </tr>  
            </table>   
        </form>    
    </div> 
       <div id="productTab" class="easyui-dialog" style="border:1px solid #D2D7D3;width: 388px; height: 380px; padding: 10px 20px;" closed="true" buttons="#dlg-buttons" shadow="false" >    
        <form id="fm" method="post" >  
            <table border="0" style="width:98%;">  
                <input type="hidden"  id="targetID" name="targetID"  value=""  style="width: 180px;">  
                <tr style="height: 35px;">  
                    
                    <td ><div region="west" split="true"  style="width:320px ;height:250px;padding:5px;  border:1px solid #ccc; " >
			 <ul id="products"></ul> 
					</div>
			</td>  
                </tr>
                <tr style="height: 50px;">  
                    <td  align="center">  
                        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-ok" onclick="clickProduct();">确认</a>      
                        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-Cancel" onclick="javascript:$('#productTab').dialog('close'); $('#products').tree('loadData',[]);">取消</a>  
                    </td>  
                </tr>  
            </table>   
        </form>    
    </div> 
    <div id="dtTab" class="easyui-dialog"  style="border:1px solid #D2D7D3;width: 800px; height: 500px; padding: 10px 20px;" closed="true" buttons="#dlg-buttons" shadow="false">
    <div data-options="region:'north',split:false,collapsible:false"  style="height:200px; border:1px solid #ccc;">
        <table id="department_tab" border="0"></table >
        </div> 
		<div data-options="region:'center',split:false,collapsible:false"   style="padding:8px; height:20px;">
            <table cellspacing="0" border="0">  
                <tr>  
                    <td><a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" onclick="clickdt();">确定</a> &nbsp
                    <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-Remove" onclick="javascript:$('#dtTab').dialog('close');">取消</a>  &nbsp 
                 	</td>            
                </tr>  
            </table>  
		</div>
		<div data-options="region:'south' ,split:false,collapsible:false"  style="height:200px; border:1px solid #ccc;"   >
			 <table id="departmentUser_tab" border="0"></table>
		</div>
		</div>
		
        </div>
        <div id="cp" style="width:auto;height:98%;padding:10px; background-color: #F1F3F7" title="产品结构树 " closable="false"  >
         <div class="easyui-layout" fit="true" style="width:auto;">
		<div region="west" split="true"  style="width:150px;padding:5px; background-color: #F1F3F7">
			 <ul id="products_targetboms">
			</ul> 
		</div>
		<div id="content" region="center" style="padding:5px; background-color: #F1F3F7">
		<table id="target_tab" ></table>
		</div>
	</div>
       
		</div>
		</div>
		</div>
				</div>
		    </div>
		</div>
	   </body>
</html>
