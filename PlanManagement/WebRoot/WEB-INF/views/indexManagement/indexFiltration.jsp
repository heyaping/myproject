<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"  %>
<%@page import="java.text.SimpleDateFormat"%> 
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"> <!-- HTML5 document type -->
<html>
  <head>
    <base href="<%=basePath%>">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>指标过滤</title>
	<link rel="stylesheet" type="text/css" href="STATIC/css/default.css">
	<link rel="stylesheet" type="text/css" href="STATIC/js/jquery-easyui-1.3.5/themes/gray/easyui.css">
	<link rel="stylesheet" type="text/css" href="STATIC/js/jquery-easyui-1.3.5/themes/icon.css" />
	<link href="STATIC/css/main.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="STATIC/css/mainstyles.css" type="text/css"></link>
	<script type="text/javascript" src="STATIC/js/jquery.min.js"></script>
	<script type="text/javascript" src="STATIC/js/work.js"></script>
	<script type="text/javascript" src="STATIC/js/jquery-easyui-1.3.5/jquery-1.10.2.min.js"></script>
	<script type="text/javascript" src="STATIC/js/jquery-easyui-1.3.5/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="STATIC/js/JQuery-zTree-v3.5.15/jquery.ztree.all-3.5.min.js"></script>
	<script type="text/javascript" src="STATIC/js/index.js"></script>
	<script type="text/javascript" src="STATIC/js/extends.js"></script>
	<script type="text/javascript" src="STATIC/js/common.js"></script>
  </head>
  <script type="text/javascript">  
  	var url = "";
    /* 启动时加载 */  
    $(function(){  
         /*加载型号-方案*/
          $("#slelct_Model").combobox({editable:false});
          $('#slelct_Model').combobox({      
           url:'/PlanManagement/TargetbomManager/getDesignxh.action',      
            valueField:'modelID', 
            panelHeight:'auto',     
            textField:'modelName',
              onLoadSuccess: function () {
                
            },
               onChange:function(modelID){
		    	//$('#city').combobox('clear');
		    	$('#slelct_Project').combobox({
			    valueField:'taskID', //值字段
			    textField:'taskName', //显示的字段
			    url:'/PlanManagement/TargetbomManager/getDesignfa.action?modelID='+modelID,
			    panelHeight:'auto',
		 	});
		    }
            });
          $("#slelct_Project").combobox({editable:false});
          $('#slelct_Project').combobox({      
            //url:'/PlanManagement/TargetbomManager/getTargetType.action',      
            valueField:'taskID', 
            panelHeight:'auto',     
            textField:'taskName',
            onLoadSuccess: function (date) {
            },
          
            });
          $("#slelct_Index_type").combobox({editable:false});
          $('#slelct_Index_type').combobox({      
            url:'/PlanManagement/TargetbomManager/getTargetType.action',      
            valueField:'parameterID', 
            panelHeight:'auto',     
            textField:'parameterUnit',
            onLoadSuccess: function (date) {
            },
          
            });
            
            /* 启动时加载 */  
     $("#targeta_tab").datagrid({  
            checkOnSelect: false,  
            fitColumns :true, 
            checkOnSelect:true,
            singleSelect: false,
            selectOnCheck: true,
            nowrap:true, 
            //url: '/PlanManagement/userManager/search.action',  
            loadMsg:'加载中...',  
            fit: true,
            rowStyler:function(index,row){
			if (index%2==1){
				return 'background-color:#D2D7D3;';
			}
			 else{
			return 'background-color:#50a3a2;';
			}
		},  
            columns: [[  
                { field: 'targetID', title: '编号', width:50},  
                { field: 'productName', title: '产品名称', width:100},  
                { field: 'targetName', title: '指标名称', width:100},  
                { field: 'targeUpperLimit', title: '上限值', width:100},  
                { field: 'targeLowLimit', title: '下限值', width:100},  
                { field: 'targeFactVal', title: '当前值', width:100},  
                { field: 'unitName', title: '单位', width:100},  
                { field: 'targetTypeName', title: '指标类型', width:100 },  
                { field: 'directorName', title: '负责人', width:100 },
                { field: 'remark', title: '备注', width:100 }, 
            ]]            
        });
      }) ;
         function checkdt(){
      $("#department_tab").datagrid({  
            checkOnSelect: false,  
            fitColumns :true, 
            checkOnSelect:true,
            singleSelect: true,
            selectOnCheck: false,
            nowrap:true, 
            url: '/PlanManagement/deptManager/search.action',  
            loadMsg:'加载中...',  
            fit: true,
            rowStyler:function(index,row){
			if (index%2==1){
				return 'background-color:#D2D7D3;';
			}
			 else{
			return 'background-color:#50a3a2;';
			} 
		},
            //当用户点击当前行触发
            onClickRow:function(rowIndex,rowData){
            deptID = rowData.deptID;
   				$("#departmentUser_tab").datagrid({  
            checkOnSelect: false,  
            fitColumns :true, 
            checkOnSelect:true,
            singleSelect: false,
            selectOnCheck: true,
            nowrap:true,
            rowStyler:function(index,row){
			if (index%2==1){
				return 'background-color:#D2D7D3;';
			}
			 else{
			return 'background-color:#50a3a2;';
			} 
		}, 
            url: '/PlanManagement/userToDeptManager/searchByDept.action?deptID='+deptID,  
            loadMsg:'加载中...',  
            fit: true,  
            columns: [[  
                { field: 'userID', checkbox: true},  
                { field: 'userCode', title: '用户帐号', width:100},  
                { field: 'userName', title: '用户姓名', width:100},  
                { field: 'sex', title: '性别', width:100},  
                { field: 'tel', title: '电话', width:160},  
                { field: 'email', title: '电子邮件', width:160},  
                { field: 'jobName', title: '职务', width:100},  
                { field: 'roleID', title: '系统角色', width:100,
                formatter:function(value,row,index){if(value=='1'){return'系统管理员';}if(value=='2'){return'管理者';}if(value=='3'){return'普通用户';}}},  
            ]]            
        });
  					},  
            columns: [[  
                { field: 'deptID', title: '群组编号', width:100},  
                { field: 'deptCode', title: '群组代号', width:100},  
                { field: 'deptShortName', title: '群组简称', width:100},  
                { field: 'deptLongName', title: '群组全称', width:160},  
                { field: 'leaderName', title: '负责人', width:160},  
                { field: 'memberNum', title: '成员数', width:100},  
                { field: 'createDT', title: '创建时间', width:150},  
                { field: 'modifyDT', title: '修改时间', width:150 },  
            ]]   
             });
          	$("#departmentUser_tab").datagrid({  
            checkOnSelect: false,  
            fitColumns :true, 
            checkOnSelect:true,
            singleSelect: false,
            selectOnCheck: true,
            nowrap:true, 
            url: '/PlanManagement/userManager/search.action',  
            loadMsg:'加载中...',  
            fit: true,
            rowStyler:function(index,row){
			if (index%2==1){
				return 'background-color:#D2D7D3;';
			}
			 else{
			return 'background-color:#50a3a2;';
			} 
		},  
            columns: [[  
                { field: 'userID', checkbox: true},  
                { field: 'userCode', title: '用户帐号', width:100},  
                { field: 'userName', title: '用户姓名', width:100},  
                { field: 'sex', title: '性别', width:100},  
                { field: 'tel', title: '电话', width:160},  
                { field: 'email', title: '电子邮件', width:160},  
                { field: 'jobName', title: '职务', width:100},  
                { field: 'roleID', title: '系统角色', width:100,
                formatter:function(value,row,index){if(value=='1'){return'系统管理员';}if(value=='2'){return'管理者';}if(value=='3'){return'普通用户';}}},  
            ]]            
        });
         
      $("#dtTab").dialog("open").dialog('setTitle', '选择单个用户信息');    
      $("#dtTab").panel("move",{top:$(document).scrollTop() + ($(window).height()-450) * 0.5});
      $('#department_tab').datagrid('clearSelections');
      $('#department_tab').datagrid('load');
      } 
       function clickdt(){
       var row = $("#departmentUser_tab").datagrid("getSelected");  
         $('#director').val(row.userName);
         $('#userid').val(row.userID);
         $('#dtTab').dialog('close');
      }
      function Search(){
        var xh = $('#slelct_Model').combobox('getValue');
        var fa = $('#slelct_Project').combobox('getValue');
        var zbType = $('#slelct_Index_type').combobox('getValue');  
         var user=$('#userid').val(); 
         var url = '/PlanManagement/TargetbomManager/Search.action?modelID='+xh+'&taskID='+fa+'&targetType='+zbType+'&director='+user;
        $('#targeta_tab').datagrid('options').url='/PlanManagement/TargetbomManager/Search.action?modelID='+xh+'&taskID='+fa+'&targetType='+zbType+'&director='+user;
       $('#targeta_tab').datagrid('load');       
      }
     </script> 
     <body style="min-width:1280px;min-height:1024px;" id="layout" onload="init()">
		<div class="wrap">
			<div class="left-right-middle1">
			<jsp:include page="/WEB-INF/views/main/head.jsp" flush="true" ></jsp:include>
				<div class="easyui-layout"  style="min-width:1280px;min-height:908px;">
				 <div data-options="region:'north',title:'指标过滤',split:false,collapsible:false"   style="height:80px;background-color: #F1F3F7">
         <table style=" height: 48px;" cellspacing="0" border="0">  
                <tr style=" height: 48px;">  
                    <td> &nbsp 型号</td>  
                    <td><input id="slelct_Model" name="modelID"  style="width: 180px;height: 26px;"></td> 
                    <td> &nbsp 方案</td>  
                    <td><input id="slelct_Project" name="taskID"  style="width: 180px;height: 26px;"></td> 
                    <td> &nbsp 指标类型</td>  
                    <td><input id="slelct_Index_type" name="zbType"  style="width: 180px;height: 26px;"></td> 
                    <td> &nbsp 负责人</td>  
                    <td><input id="director" name="targetType" readonly="readonly" style="width: 180px;height: 26px;">
                    </td>
                    <td> <input type="hidden"  id="userid" name="userid"  value=""  style="width: 180px;">  </td>
                    <td><a href="javascript:void(0)" class="easyui-linkbutton" plain="true"  iconCls="icon-Search" onclick="checkdt();"   ></a></td>
                    <td>
                    <a href="javascript:void(0)" class="easyui-linkbutton"   iconCls="icon-Search" onclick="Search();"  >查询</a>
                    </td>
                </tr>  
          </table>  
        </div>
        <div data-options="region:'center',split:false,collapsible:false"   style="padding:8px; height:500px;background-color: #F1F3F7; overflow:hidden;">
          <table id="targeta_tab" ></table>
	   </div>
	    <div id="dtTab" class="easyui-dialog"  style="border:1px solid #CDB38B;width: 800px; height: 500px; padding: 10px 20px;" closed="true" buttons="#dlg-buttons" shadow="false">
    	<div data-options="region:'north',split:false,collapsible:false"  style="height:200px; border:1px solid #ccc;">
        <table id="department_tab" border="0"></table >
        </div> 
		<div data-options="region:'center',split:false,collapsible:false"   style="padding:8px; height:20px;">
            <table cellspacing="0" border="0">  
                <tr>  
                    <td><a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" onclick="clickdt();">确定</a> &nbsp
                    <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-Remove" onclick="javascript:$('#dtTab').dialog('close');">取消</a>  &nbsp 
                 	</td>            
                </tr>  
            </table>  
		</div>
		<div data-options="region:'south' ,split:false,collapsible:false"  style="height:200px; "   >
			 <table id="departmentUser_tab" border="0"></table>
		</div>
		</div>
			    </div>
			</div>
		</div>
	</body>
</html>
